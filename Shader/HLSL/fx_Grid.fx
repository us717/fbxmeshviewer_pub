//+****************************************************************
//| HLSL:グリッド線
//+****************************************************************

// ========================================================
// グローバル変数
// ========================================================

float4x4    World;
float4x4    View;
float4x4    Proj;
float4      Diffuse;

// ========================================================
// 定義
// ========================================================

// ------------------------------------------
// 頂点シェーダー
// ------------------------------------------
struct Def_VS
{
    float4 Pos  :POSITION;
    float4 Diff :COLOR0;
};

// ------------------------------------------
// ピクセルシェーダー
// ------------------------------------------
struct Def_PS
{
    float4 Color :COLOR0;
};

// ========================================================
// メイン
// ========================================================

// ------------------------------------------
// グリッド描画用
// ------------------------------------------
Def_VS VS_Grid( Def_VS In )
{
    Def_VS Out  = (Def_VS)0;
    Out.Pos     = mul( In.Pos, World );
    Out.Pos     = mul( Out.Pos, View );
    Out.Pos     = mul( Out.Pos, Proj );
    Out.Diff    = Diffuse;
    return Out;
}

Def_PS PS_Grid(Def_VS In)
{
    Def_PS Out = (Def_PS)0;
    Out.Color = In.Diff;
    return Out;
}

// ------------------------------------------
// Axis描画用
// ------------------------------------------
// ピクセルはグリッドと共通
// ------------------------------------------
Def_VS VS_Axis( Def_VS In )
{
    Def_VS Out  = (Def_VS)0;
    Out.Pos     = mul( In.Pos, World );
    Out.Pos     = mul( Out.Pos, View );
    Out.Pos     = mul( Out.Pos, Proj );
    Out.Diff    = In.Diff;
    return Out;
}

// ========================================================
// テクニック
// ========================================================

technique GridShader
{
    //GRID用
    pass P0
    {
        //!レンダーステート
        AlphaBlendEnable    = TRUE;
        Zenable             = TRUE;
        ZWriteEnable        = TRUE;
        CullMode            = 3;

        //!テクニック
        VertexShader = compile vs_2_0 VS_Grid();
        PixelShader  = compile ps_2_0 PS_Grid();
    }

    //Axis用
    pass P1
    {
        //!レンダーステート
        AlphaBlendEnable    = FALSE;
        Zenable             = TRUE;
        ZWriteEnable        = FALSE;
        CullMode            = 1;

        //!テクニック
        VertexShader = compile vs_2_0 VS_Axis();
        PixelShader  = compile ps_2_0 PS_Grid();
    }
}