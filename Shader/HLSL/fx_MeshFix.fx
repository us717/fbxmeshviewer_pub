//+****************************************************************
//| HLSL:メッシュ固定
//+****************************************************************

// ========================================================
// グローバル変数
// ========================================================
float4x4 World;
float4x4 View;
float4x4 Proj;
float4   Diffuse;
texture  Tex;

// =========================================================
// テクスチャサンプラ
// =========================================================
sampler tex0 = sampler_state
{
    Texture      = <Tex>;
    MinFilter    = LINEAR;
    MagFilter    = LINEAR;
    MipFilter    = NONE;
};

// -------------------------------------------------
// メッシュ(テクスチャあり)
// -------------------------------------------------

//====定義<VS>=================================
struct VS_IN_Mesh
{
    float4 Pos          :POSITION;
    float2 Texture      :TEXCOORD0;
};

struct VS_OUT_Mesh
{
    float4 Pos          :POSITION;
    float4 Diff         :COLOR0;
    float2 Texture      :TEXCOORD0;
};

//====定義<PS>=================================
struct PS_IN_Mesh
{
    float4 Diff        :COLOR0;
    float2 Texture     :TEXCOORD0;
};

struct PS_OUT_Mesh
{
    float4 Color       :COLOR0;
};

//====シェーダ=================================
VS_OUT_Mesh VSMesh( VS_IN_Mesh In )
{
    VS_OUT_Mesh Out = (VS_OUT_Mesh)0;
    Out.Pos     = mul( In.Pos, World );
    Out.Pos     = mul( Out.Pos, View );
    Out.Pos     = mul( Out.Pos, Proj );
    Out.Diff    = Diffuse;
    Out.Texture = In.Texture;
    return Out;
}

PS_OUT_Mesh PSMesh( PS_IN_Mesh In )
{
    PS_OUT_Mesh Out = (PS_OUT_Mesh)0;
    Out.Color = In.Diff * tex2D( tex0, In.Texture );
    return Out;
}


// ------------------------------------------
// テクスチャなし
// ------------------------------------------

//====定義<VS>=================================
struct VS_IN_MeshNoTex
{
    float4 Pos          :POSITION;
};

struct VS_OUT_MeshNoTex
{
    float4 Pos          :POSITION;
    float4 Diff         :COLOR0;
};

//====定義<PS>=================================
struct PS_IN_MeshNoTex
{
    float4 Diff         :COLOR0;
};

struct PS_OUT_MeshNoTex
{
    float4 Color        :COLOR0;
};

//====シェーダ=================================
VS_OUT_MeshNoTex VSMeshNoTex( VS_IN_MeshNoTex In )
{
    VS_OUT_MeshNoTex Out = (VS_OUT_MeshNoTex)0;
    Out.Pos     = mul( In.Pos, World );
    Out.Pos     = mul( Out.Pos, View );
    Out.Pos     = mul( Out.Pos, Proj );
    Out.Diff    = Diffuse;
    return Out;
}

PS_OUT_MeshNoTex PSMeshNoTex( PS_IN_MeshNoTex In )
{
    PS_OUT_MeshNoTex Out = (PS_OUT_MeshNoTex)0;
    Out.Color = In.Diff;
    return Out;
}



// ========================================================
// テクニック
// ========================================================
technique TShader
{
    //!メッシュ(テクスチャあり)
    pass P0
    {
        CullMode = 2;
        AlphaBlendEnable    = TRUE;
        Zenable             = TRUE;
        ZWriteEnable        = TRUE;
        Wrap0               = U;
        VertexShader = compile vs_2_0 VSMesh();
        PixelShader  = compile ps_2_0 PSMesh();
    }

    //!メッシュ(テクスチャなし)
    pass P1
    {
        CullMode = 2;
        AlphaBlendEnable    = TRUE;
        Zenable             = TRUE;
        ZWriteEnable        = TRUE;
        Wrap0               = U;
        VertexShader = compile vs_2_0 VSMeshNoTex();
        PixelShader  = compile ps_2_0 PSMeshNoTex();
    }
}