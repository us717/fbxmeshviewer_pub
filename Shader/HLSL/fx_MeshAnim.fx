//+****************************************************************
//| HLSL:アニメ
//+****************************************************************

// =================================================================
// グローバル変数
// =================================================================
float4x4    Mat;
float4x4    View;
float4x4    Proj;
float4      Diffuse;
float4x4    World[12];
texture     Tex;


// =================================================================
// テクスチャサンプラ
// =================================================================
sampler tex0 = sampler_state
{
    Texture = <Tex>;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    MipFilter = LINEAR;
};

// =================================================================
// 定義
// =================================================================

// ---------------------------------------------------
// メッシュ(テクスチャあり)
// ---------------------------------------------------

//====定義<VS>==================================
//!VS:IN
struct VS_IN_Mesh
{
    float3 Pos     :POSITION;
    float4 blend   :BLENDWEIGHT;
    int4   idx     :BLENDINDICES;
    float2 Tex     :TEXCOORD0;
};

//!VS:OUT
struct VS_OUT_Mesh
{
    float4 Pos     :POSITION;
    float4 Diff    :COLOR0;
    float2 Tex     :TEXCOORD0;
};

//====定義<PS>==================================
//!PS-IN
struct PS_IN_Mesh
{
    float4 Diff     :COLOR0;
    float2 Tex      :TEXCOORD0;
};

//!PS-OUT
struct PS_OUT_Mesh
{
    float4 Color    :COLOR0;
};

//====シェーダー===============================

//!頂点シェーダ
VS_OUT_Mesh VSMesh( VS_IN_Mesh In )
{
    VS_OUT_Mesh Out = ( VS_OUT_Mesh )0;
    float w[4]      = (float[4])In.blend;
    float4x4 comb   = (float4x4)0;
        
    for( int i=0 ; i<4 ; i++ )
    {
        comb += World[ In.idx[i] ] * w[i];
    }

    Out.Pos     = mul( float4(In.Pos, 1.0f), comb);
    Out.Pos     = mul( Out.Pos, Mat );
    Out.Pos     = mul( Out.Pos, View );
    Out.Pos     = mul( Out.Pos, Proj );
    Out.Diff    = Diffuse;
    Out.Tex     = In.Tex;
    return Out;
}

//!ピクセルシェーダ
PS_OUT_Mesh PSMesh(PS_IN_Mesh In)
{
    PS_OUT_Mesh Out = (PS_OUT_Mesh)0;
    Out.Color = (In.Diff * tex2D(tex0, In.Tex) );

    return Out;
}


// ---------------------------------------------------
// メッシュ(テクスチャなし)
// ---------------------------------------------------

//====定義<VS>==============================
//!VS:IN
struct VS_IN_MeshNoTex
{
    float3  Pos     :POSITION;
    float4  blend   :BLENDWEIGHT;
    int4    idx     :BLENDINDICES;
};

//!VS:OUT
struct VS_OUT_MeshNoTex
{
    float4  Pos     :POSITION;
    float4  Diff    :COLOR0;
};

//====定義<PS>==============================
//!PS-IN
struct PS_IN_MeshNoTex
{
    float4  Diff    :COLOR0;
};

//!PS-OUT
struct PS_OUT_MeshNoTex
{
    float4 Color    :COLOR0;
};

//====シェーダー==========================

//!頂点シェーダ
VS_OUT_MeshNoTex VSMeshNoTex ( VS_IN_MeshNoTex In )
{
    VS_OUT_MeshNoTex Out = ( VS_OUT_MeshNoTex )0;
    float w[4]      = (float[4])In.blend;
    float4x4 comb   = (float4x4)0;
        
    for( int i=0 ; i<4 ; i++ )
    {
        comb += World[ In.idx[i] ] * w[i];
    }

    Out.Pos     = mul( float4(In.Pos, 1.0f), comb);
    Out.Pos     = mul( Out.Pos, Mat );;
    Out.Pos     = mul( Out.Pos, View );
    Out.Pos     = mul( Out.Pos, Proj );
    Out.Diff    = Diffuse;
    return Out;
}

//!ピクセルシェーダ
PS_OUT_MeshNoTex PSMeshNoTex(PS_IN_MeshNoTex In)
{
    PS_OUT_MeshNoTex Out = (PS_OUT_MeshNoTex)0;
    Out.Color = In.Diff;
    return Out;
}

// =================================================
// テクニック
// =================================================
technique TShader
{
    //!メッシュ(テクスチャあり)
    pass P0
    {
        CullMode = 2;
        AlphaBlendEnable    = TRUE;
        Zenable             = TRUE;
        ZWriteEnable        = TRUE;
        Wrap0               = U;
        VertexShader = compile vs_2_0 VSMesh();
        PixelShader  = compile ps_2_0 PSMesh();
    }

    //!メッシュ(テクスチャなし)
    pass P1
    {
        CullMode = 2;
        AlphaBlendEnable    = TRUE;
        Zenable             = TRUE;
        ZWriteEnable        = TRUE;
        Wrap0               = U;
        VertexShader = compile vs_2_0 VSMeshNoTex();
        PixelShader  = compile ps_2_0 PSMeshNoTex();
    }
}