//|*************************************************************
//|ファイルパッキング機能
//|*************************************************************

//====インクルード===================
#include <string>
#include <fstream>
#include <iostream>
#include <Windows.h>
#include <list>

using namespace std;

//====関数プロトタイプ================
void CreateFileList	( list<string> &FileNameList, const string &DirectryName );
void CreateArchive	( list<string> &FileNameList, const char *pFileName );
void Write( ofstream *o, int a );

//--------------------------------------------------
// メイン
//--------------------------------------------------
int main()
{
	//!変数
	list<string> FileName;	//ファイル名格納用リスト
	
	//!アーカイブ化
	CreateFileList( FileName, "data/bin");
	CreateArchive( FileName, "data/sys.bin" );
	
	//!ログ出力
	cout << "======ログ=======================" << endl;
	cout << endl;
	cout << "<リスト化ファイル名>" << endl;
	list<string>::iterator it = FileName.begin();
	while( it != FileName.end() )
	{
		cout << *it << endl;
		++it;
	}

	cout <<"---------------------------------" << endl;
	bool flag = true;
	while(flag)
	{
		int num;
		cout << "1で終了" << endl;
		cin >> num;
		if( num == 1)
		{
			flag = false;
		}
	}
	return 0;
}

//--------------------------------------------------
// ファイルリストの作成
//--------------------------------------------------
//	(in) list<string>	:ファイル名書き込み先リスト
//	(in) string			:アーカイブ化対象親フォルダ
//--------------------------------------------------
void CreateFileList( list<string> &FileNameList, const string &DirectryName )
{
	HANDLE iterater;					//イテレーター
	WIN32_FIND_DATAA date;				//ファイル名が入る

	string FirstFilePath = DirectryName;
	FirstFilePath += "/*.*";

	iterater = FindFirstFileA( FirstFilePath.c_str(), &date );
	while(true)
	{
		const char *name = date.cFileName;
		if( !( date.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
		{
			//ファイル名結合
			string FileName = DirectryName;
			FileName += '/';
			FileName += name;
			
			//リストに追加
			FileNameList.push_back(FileName);
		}
		
		if( !FindNextFileA( iterater, &date ) )//次に移動
		{
			break;
		}
	}
	
	//!ディレクトリの時
	iterater = FindFirstFileA( FirstFilePath.c_str(), &date );
	while(true)
	{
		string Name = date.cFileName;
		if( Name == "." || Name == ".." )
		{
			;//無視
		}
		else if( date.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			//ディレクトリ名追加
			string newDirectryName = DirectryName;
			newDirectryName += '/';
			newDirectryName += Name;

			//再帰処理
			CreateFileList( FileNameList, newDirectryName.c_str() );
		}
		
		//ディレクトリがなければ何もしない
		if( !FindNextFileA( iterater , &date ))
		{
			break;
		}
	}	
}

//-------------------------------------------------
// アーカイブの作成
//-------------------------------------------------
// (in)list<string>	:	ファイル名を格納したリスト
// (in)const char	:	アーカイブファイル保存先
//-------------------------------------------------
//<内部データ>
//	・データ	
//		[データ..
//		[ファイル情報の開始位置
//	・ファイル数
//	・ファイル情報
//		[場所..
//		[ファイルサイズ..
//		[名前のサイズ..
//		[ファイル名..
//	・ファイルデータ(表開始位置)
//-------------------------------------------------
void CreateArchive( list<string> &FileNameList, const char *pFileName )
{
	//!ローカル変数
	int *FileSize = new int[FileNameList.size()];

	//!アーカイブ書き込みファイルの指定
	ofstream OutPut( pFileName, ofstream::trunc| ofstream::binary );
	//!データコピー
	int i=0;
	list<string>::iterator it = FileNameList.begin();
	while( it != FileNameList.end() )
	{
		ifstream In( (*it).c_str() , ifstream::binary );
		//サイズ取得
		In.seekg( 0, ifstream::end );
		FileSize[i] = static_cast<int>(In.tellg());
		
		//データ書き込み
		In.seekg( 0, ifstream::beg );
		char *date = new char[ FileSize[i] ];
		In.read( date, FileSize[i] );
		OutPut.write( date, FileSize[i] );
		In.close();
		++it;
		++i;
	}

	//!末尾にファイル情報記述
	int dateEnd = static_cast<int>(OutPut.tellp());
	Write( &OutPut, FileNameList.size() );			//ファイル数

	//場所・サイズ・名前の文字数・名前記述
	int pos = 0;
	it = FileNameList.begin();
	i = 0;
	while( it != FileNameList.end() )
	{
		Write( &OutPut, pos );						//場所
		Write( &OutPut, FileSize[i] );				//ファイルサイズ
		int NameLength = static_cast< int >((*it).size());
		string Name = (*it).c_str();
		Write( &OutPut, NameLength );				//名前のサイズ
		OutPut.write( Name.c_str(), NameLength );	//ファイル名
		pos += FileSize[i];
		++it;
		++i;
	}
	
	Write( &OutPut, dateEnd );	//ファイル情報の開始位置
	
	OutPut.close();
	delete[] FileSize;
	FileSize = 0;
}

//---------------------------------------------
// 書き込み(リトルエンディアンに変換)
//---------------------------------------------
// (in)	ofstream:	書き込み用ストリーム
// (in)	int		:	変換する要素
//---------------------------------------------
void Write( ofstream *o, int a )
{
	char str[4];
	str[0] = static_cast< char >( ( a & 0x000000ff ) >> 0);
	str[1] = static_cast< char >( ( a & 0x0000ff00 ) >> 8 );
	str[2] = static_cast< char >( ( a & 0x00ff0000 ) >> 16 );
	str[3] = static_cast< char >( ( a & 0xff000000 ) >> 24 );
	o->write( str, 4);
}

