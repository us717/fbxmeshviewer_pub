# FbxMeshViewer #
　* Blenderで作成したFBXファイルをDirectX9.0c開発環境下で使用できるか確認するビューア。

　* 1マテリアル、1テクスチャ、1メッシュ、12ボーン、キーフレームアニメーションに対応しています。

　* 自分用ツールのため実用性はありませんが、情報の少ないFBXSDKの参考にはなると思います。(src/Fileloder/FBXLoder)

# ビルド方法 #

■使用ライブラリ

    Microsoft DirextX SDK (June2010)
    Lua5.1
    FBXSDK 2014.1

■VisualStudioの設定

    <VCディレクトリ>
       #インクルードディレクトリ
         -(絶対パス)\Microsoft DirectX SDK (June 2010)Include x86
       #ライブラリディレクトリ
         -(絶対パス)\Microsoft DirectX SDK (June 2010)Lib x86
    <C/C++>
        #追加のインクルードディレクトリ
         -(絶対パス)\src以下のディレクトリ(Object,Camera,Sprite,Mesh,Font,Sound)
         -(絶対パス)\FBXSDK 2014.1 include
         -(絶対パス)\Lua include
    <リンカ>
        #追加のライブラリディレクトリ
         -(絶対パス)\Lua
         -(絶対パス)\FBX SDK 2014.1 lib
    <入力>
        #(Debug)
         -d3d9.lib
         -d3dx9d.lib
         -dxerr.lib
         -dinput8.lib
         -dxguid.lib
         -dsound.lib
         -winmm.lib
         -wininet.lib
         -lua5.1.lib
         -(絶対パス)\Autodesk\FBX\FBX SDK\2014.1\lib\vs2010\x86\debug\libfbxsdk-mt.lib
         -shlwapi.lib
        #(Release)
         -d3d9.lib
         -d3dx9.lib
         -dxerr.lib
         -dxguid.lib
         -dinput8.lib
         -dsound.lib
         -winmm.lib
         -lua5.1.lib
         (絶対パス)Autodesk\FBX\FBX SDK\2014.1\lib\vs2010\x86\release\libfbxsdk-mt.lib
         -shlwapi.lib

# その他 #

■シェーダ

 * DirectXSDK付属のfxc.exe(HLSLコンパイラ)を使用しています。

 * コンパイルしたものを、Shaderディレクトリに入れることによってシェーダを管理しています。
 
■FilePacking.exe

* dataディレクトリ内のファイルをアーカイブファイルにします
* 作成したアーカイブ(sys.bin)はdataディレクトリ内に作成されます。
* 作成したアーカイブ、sys.binを使用する場合はプロジェクト直下に移動して下さい。
* FilePacking.cppはFilePacking.exeのソースファイルとなります。