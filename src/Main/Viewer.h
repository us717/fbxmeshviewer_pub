//=======================================================
// メッシュビューア
//=======================================================
#ifndef Include_Viewer_H
#define Include_Viewer_H

//====<インクルード>=================================
#include "SequenceBase.h"

//====<前方宣言>=====================================
namespace Camera{ class Projection; }
namespace Camera{ class ViewCamera; }
namespace Object{ class GridLines; }
namespace Object{ class Axis; }
namespace Object{ class Mesh; }

// -------------------------------------------------
// クラス: ビューア
// -------------------------------------------------
class Viewer: public SequenceBase{

//=====構築子：消去子=================
public:
    Viewer();
    ~Viewer();

//=====変数===========================
private:
    //!オブジェクト
    Camera::ViewCamera      *pCamera;       //カメラ(ビューカメラ)
    Object::GridLines       *pGridLines;    //グリッド線
    Object::Axis            *pAxis;         //軸
    Object::Mesh            *pMesh;         //メッシュ

    //!フラグ
    u8                      DrawFlag;       //フラグ:オブジェ描画


    //!入力情報
    f32                     Roll;

    //!マウス動作
    POINT                   TmpPos;
    POINT                   PrePos;
    f32                     MoveSide;
    f32                     MoveHeight;
    f32                     MoveAtX;
    f32                     MoveAtY;
    f32                     Depth;

//=====関数===========================
public:
    //!基底
    void Input();       //入力
    void Update();      //更新
    void Draw();        //描画
    SequenceBase* CheckSequence( SequenceBase* pTempSequence);  //シーケンス確認
};

#endif