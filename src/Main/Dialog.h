//=======================================================
// ダイアログ
//=======================================================
#ifndef Include_Dialog_H
#define	Include_Dialog_H

//------------------------------------------
// プロトタイプ宣言
//------------------------------------------
BOOL CALLBACK DialogProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lp );



#endif