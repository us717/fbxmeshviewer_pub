//=======================================================
// Window・DirectX制御
//-------------------------------------------------------
// DirecX初期化処理
//=======================================================
#ifndef Include_Dx_H
#define Include_Dx_H

// ------------------------------------------
// Window・DirectX制御
// ------------------------------------------
// ・シングルトンクラス
// ------------------------------------------
class Dx{

//=====定義==========================
public:
    //!デフォルト画面サイズ
    enum{   DEFALT_WIDTH = 800,
            DEFALT_HEIGHT = 600,
        };

//=====構築子：消去子=================
public:
    //!クラスへのアクセス手段
    static Dx *Get()
    {
        static Dx instance;
        return &instance;
    }
private:
    //!コンストラクタ<使用不可>
    Dx();
    Dx( const Dx &src ){}
    Dx &operator=( const Dx &src ){}

    //!デストラクタ
    ~Dx(){};

//=====変数===========================
private:
    //システム
    HWND                    hWnd;
    HWND                    hDlg;
    u32                     DlgUserMsg;
    HINSTANCE               hInst;
    LPDIRECT3D9             pD3DIF;
    LPDIRECT3DDEVICE9       pD3DDevice;
    D3DPRESENT_PARAMETERS   d3dpp;
    LPD3DXSPRITE            pSprite;
    f32                     WindowWidth;    //現在の画面サイズ(横)
    f32                     WindowHeight;   //現在の画面サイズ(縦)
    f32                     CurWidthRate;   //画面横、デフォルト値と現在の設定値の比率
    f32                     CurHeightRate;  //画面縦、デフォルト値と現在の設定値の比率
    bool                    ErrorHandle;

//=====関数===========================
public:
    //!アクセサ
    HINSTANCE               GetHinstance()      const { return hInst; }         //インスタンスハンドルの取得
    HWND                    GetMainHwnd()       const { return hWnd; }          //メインハンドルの取得
    HWND                    GetDialogHwnd()     const { return hDlg;}           //ダイアログハンドルの取得
    LPDIRECT3D9             GetD3DIF()          const { return pD3DIF; }        //Directxインターフェイスの取得
    LPDIRECT3DDEVICE9       GetD3DDevice()      const { return pD3DDevice; }    //Dirextxデバイスの取得
    D3DPRESENT_PARAMETERS   GetD3DParam()       const { return d3dpp; }         //PresntParametersの取得
    LPD3DXSPRITE            GetSprite()         const { return pSprite; }       //spriteインターフェイスの取得
    f32                     GetWindowWidth()    const { return WindowWidth; }   //画面横サイズ取得
    f32                     GetWindowHeight()   const { return WindowHeight; }  //画面縦サイズ取得
    f32                     GetCurWidthRate()   const { return CurWidthRate; }  //現在の画面横比率取得
    f32                     GetCurHeightRate()  const { return CurHeightRate; } //現在の画面縦比率取得

    //DirectX
private:
    bool CreateD3DIF();                                             //Directxインターフェイスの作成
    bool CreateD3DDevice();                                         //Directxデバイスの作成
    bool CreateSprite();                                            //Spriteインターフェイス作成
    bool CreateViewPort();                                          //ビューポートの作成
public:
    bool InitDirectX();                                             //DirectXの初期化
    void CleanUpDirectXVar();                                       //DirectX変数解放
    bool ChangeWindowSize( const u32 &Width, const u32 &Height );   //画面サイズ変更
    void SystemError( bool Handle );                                //キルスイッチ(false強制終了)

    //Window
    void SetHinstance( HINSTANCE hInst ){ this->hInst = hInst; }    //インスタンスハンドルを登録する
    void SetDialogHandle( HWND hDlg ){ this->hDlg = hDlg; }         //ダイアログハンドルを登録する
    void SetMainWindHandle( HWND hWnd ){ this->hWnd = hWnd; }       //ウィンドハンドルを登録する
};


#endif