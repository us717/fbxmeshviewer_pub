//+**************************************************************************************
//| ダイアログ
//+**************************************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <WindowsX.h>
#include <string.h>
#include "resource.h"
#include "Mesh.h"

BOOL CALLBACK DialogProc( HWND hDlg, UINT msg, WPARAM wp, LPARAM lp )
{
    HWND RADIO1 = GetDlgItem( hDlg, IDC_RADIO1 ); //プリミティブタイプ:ポリゴン
    HWND RADIO2 = GetDlgItem( hDlg, IDC_RADIO2 ); //プリミティブタイプ:頂点
    HWND RADIO4 = GetDlgItem( hDlg, IDC_RADIO4 ); //アニメ:再生
    HWND RADIO5 = GetDlgItem( hDlg, IDC_RADIO5 ); //アニメ:停止
    HWND LIST1  = GetDlgItem( hDlg, IDC_LIST1 );  //リスト
    HWND CHECK1 = GetDlgItem( hDlg, IDC_CHECK1 ); //アニメ描画

    int id = 0;
    TCHAR pBuf[16]={0};

    switch(msg)
    {
        //====初期化処理(ボタン初期配置)========
        case WM_INITDIALOG:
            //ラジオボタン
            Button_SetCheck( RADIO1, BST_CHECKED );
            Object::Mesh::SetPrimitiveType( Object::Mesh::POLYGON );

            //チェックボタン
            Button_SetCheck( CHECK1 , BST_CHECKED );

            //!ボックス
            Button_SetCheck( RADIO4, BST_CHECKED );
            for( u32 i=0; i<Object::Mesh::GetMaxTake(); ++i )
            {
                memset( pBuf, 0, sizeof(pBuf) );
                _stprintf_s( pBuf, _T("テイク%d"), i );
                ListBox_AddString( LIST1, pBuf );
            }
        return TRUE;

        //=====再初期化========================
        case UM_MESSAGE01:
            ListBox_ResetContent(LIST1);
            for( u32 i=0; i<Object::Mesh::GetMaxTake(); ++i )
            {
                memset( pBuf, 0, sizeof(pBuf) );
                _stprintf_s( pBuf, _T("テイク%d"), i );
                ListBox_AddString( LIST1, pBuf );
            }
        return TRUE;

        //====コマンド=========================
        case WM_COMMAND:
            switch( LOWORD(wp) )
            {
                //====プリミティブタイプ:ポリゴン====
                case IDC_RADIO1:
                    Button_SetCheck( RADIO2, BST_UNCHECKED );
                    Object::Mesh::SetPrimitiveType( Object::Mesh::POLYGON );
                return TRUE;

                //=====プリミティブタイプ:頂点========
                case IDC_RADIO2:
                    Button_SetCheck( RADIO1, BST_UNCHECKED );
                    Object::Mesh::SetPrimitiveType( Object::Mesh::POINT );
                return TRUE;

                //====アニメ:再生============
                case IDC_RADIO4:
                    Button_SetCheck( RADIO5, BST_UNCHECKED );
                    Object::Mesh::SetAnimPlayFlag( Object::Mesh::PLAY );
                return TRUE;

                //====アニメ:停止============
                case IDC_RADIO5:
                    Button_SetCheck( RADIO4, BST_UNCHECKED );
                    Object::Mesh::SetAnimPlayFlag( Object::Mesh::STOP );
                return TRUE;

                //====アニメ:リセット========
                case IDC_BUTTON2:
                    Object::Mesh::AnimReset();
                return TRUE;

                //====リスト=================
                case IDC_LIST1:
                    if( HIWORD(wp)== LBN_DBLCLK )
                    {
                        id = ListBox_GetCurSel(LIST1);
                        //テイク番号を入れる
                        Object::Mesh::SetAnimTake(id);
                    }

                //====アニメ描画=============
                case IDC_CHECK1:
                    if( Button_GetCheck(CHECK1) == BST_CHECKED )
                    {
                        Object::Mesh::SetAnimFlag(true);
                    }
                    else
                    {
                        Object::Mesh::SetAnimFlag(false);
                    }
                return TRUE;
            }
        break;

        //====閉じる==================
        case WM_CLOSE:
            SendMessage( Dx::Get()->GetMainHwnd(), WM_DESTROY, wp, lp );
        break;
    }

    return FALSE;
}