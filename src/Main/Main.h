//=======================================================
// SysMainの関数プロトタイプ宣言
//=======================================================
#ifndef Include_SysMain_H
#define	Include_SysMain_H

//------------------------------------------
// SysMainの関数プロトタイプ宣言
//------------------------------------------

//!Windowsメッセージプロシージャ
LRESULT WINAPI MsgProc( HWND hWnd , UINT msg, WPARAM wParam, LPARAM lParam );

//!メインループ
VOID WinMainRoop(HWND hWnd);

//!メインデバイスの解放処理
void CleanUpMainDevice();

//!デバッグ用ログファイル初期化
void InitDebugLog();


#endif

