//=======================================================
// メインシーケンス(メインループ)
//=======================================================
#ifndef Include_MainSequence_H
#define Include_MainSequence_H

//====<前方宣言>==============================
class SequenceBase;
namespace System{ class LostDevice;}

// ------------------------------------------
// クラス:メインシーケンス
// ------------------------------------------
class MainSequence{

//=====構築子：消去子=================
private:
    //!コピー禁止
    void operator = ( const MainSequence &src ){}
    MainSequence( const MainSequence&src ){}
public:
    //!コンストラクタ・デストラクタ
    MainSequence();
    ~MainSequence();

//=====変数===========================
private:
    SequenceBase            *pCurSequence;      //現在のシーケンス
    SequenceBase            *pNextSequence;     //次に移動するシーケンス
    System::LostDevice      *pDev;              //ロストデバイスインターフェイスへのポインタ
    bool                    LostFlag;           //フラグ:デバイスロスト判断
    u8                      Red, Green, Blue;   //背景色

//=====関数===========================
public:
    //シーケンス制御
    void Input();                               //入力
    void Update();                              //更新
    void Draw();                                //描画
    void UpdateSequence();                      //シーケンスの更新

    //背景色変更
    void SetBackColor( const u8 &Red, const u8 &Green, const u8 &Blue );
};

#endif