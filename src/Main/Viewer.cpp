//+****************************************************************
//| メッシュビューア
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include "Viewer.h"
#include "GridLines.h"
#include "ViewCamera.h"
#include "Axis.h"
#include "Mesh.h"
#include "FileLoader.h"
#include "Projection.h"

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
Viewer::Viewer()
    :DrawFlag(false),Roll(0.0f),Depth(0.0f)
{
    //!オブジェクト領域確保
    pCamera     = new Camera::ViewCamera;
    pGridLines  = new Object::GridLines;
    pAxis       = new Object::Axis;

    //!グリッド作成
    pGridLines->SetGridNum(51);
    pGridLines->Update();
    pAxis->CreateBuf();

    //!マウス動作関係初期化
    POINT init  = {0};
    TmpPos      = init;
    PrePos      = init;
    MoveSide    = 0.0f;
    MoveHeight  = 0.0f;
    MoveAtX     = 0.0f;
    MoveAtY     = 0.0f;

    //!メッシュ作成
    pMesh = new Object::Mesh;

    //!リソースのロード
    FileLoader::FileLoader::Get()->LoadALL();
}

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
Viewer::~Viewer()
{
    //オブジェクト解放
    SAFE_DELETE(pCamera);
    SAFE_DELETE(pGridLines);
    SAFE_DELETE(pAxis);
    SAFE_DELETE(pMesh);
}

// -------------------------------------------------
// 入力
// -------------------------------------------------
void Viewer::Input()
{
//====要素初期化======================
    Roll= 0.0f;
    DInput::Get()->GetKeyState();   //キーの状態確認
    DInput::Get()->GetMouseState(); //マウスの状態確認
    MoveSide    = 0.0f;
    MoveHeight  = 0.0f;
    MoveAtX     = 0.0f;
    MoveAtY     = 0.0f;

//====メッシュ描画形態=================-
    if( DInput::Get()->Key()[DIK_1]&0x80 )
        DrawFlag = 0;
    if( DInput::Get()->Key()[DIK_2]&0x80 )
        DrawFlag = 1;
    if( DInput::Get()->Key()[DIK_3]&0x80 )
        DrawFlag = 2;

//====カメラ動作======================
    TmpPos = DInput::Get()->Mouse()->MousePos; //現在のマウス入力情報

    //!カメラ移動
    if( DInput::Get()->Mouse()->MainInfo.rgbButtons[1]&0x80 )
    {
        //マウスの移動量計算
        MoveAtX = (f32)(PrePos.x-TmpPos.x);
        MoveAtY = (f32)(TmpPos.y-PrePos.y);
    }

    //!注視点移動
    if( DInput::Get()->Mouse()->MainInfo.rgbButtons[0]&0x80 )
    {
        MoveSide    = (f32)(PrePos.x-TmpPos.x);
        MoveHeight  = (f32)(TmpPos.y -PrePos.y);
    }

    //!キー:
    if( DInput::Get()->Key()[DIK_A]&0x80 )  Roll = -1.0f; //Z軸回転
    if( DInput::Get()->Key()[DIK_D]&0x80 )  Roll = 1.0f;

    //!マウス:スクロール(注視点からの距離)
    Depth = (f32)( DInput::Get()->Mouse()->MainInfo.lZ) * 0.018f;
    if( DInput::Get()->Key()[DIK_W]&0x80 )  Depth = 0.1f;
    if( DInput::Get()->Key()[DIK_S]&0x80 )  Depth = -0.1f;

    //!1フレーム分のマウス位置記憶
    PrePos = TmpPos;

//====<押されていない>============
    DInput::Get()->ResetFixFlag();
}

// -------------------------------------------------
// 更新
// -------------------------------------------------
void Viewer::Update()
{
    //!カメラ位置
    pCamera->MoveLookAt( MoveAtX, MoveAtY );
    pCamera->MoveCamera( MoveSide, MoveHeight, Depth );
    pCamera->SetRotateZ( Roll );
    pCamera->Update();

    //!軸
    pAxis->Update( pCamera->GetViewMat(0) );

    //!メッシュ更新
    pMesh->Update();
    pGridLines->GridLines::Update();
}

// -------------------------------------------------
// 描画
// -------------------------------------------------
void Viewer::Draw()
{
    Dx::Get()->GetSprite()->Begin(D3DXSPRITE_ALPHABLEND );

    pGridLines->Draw( *Camera::Projection::Get()->GetProj(), *pCamera->GetViewMat(0) );
    pAxis->Draw( Camera::Projection::Get()->GetProj() );

    //!メッシュ描画
    pMesh->Draw( Camera::Projection::Get()->GetProj(), pCamera->GetViewMat(0) );

    Dx::Get()->GetSprite()->End();
}

// -------------------------------------------------
// シーケンスの更新
// -------------------------------------------------
// (i/o)    pTempSequence   : 現在のシーケンス
// (out)    MainSequence    : 次のシーケンス
// -------------------------------------------------
SequenceBase*  Viewer::CheckSequence( SequenceBase* pTempSequence )
{
    pNextSequence = pTempSequence;
    return pNextSequence;
}
