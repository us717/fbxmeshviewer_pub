//=======================================================
//ライブラリ・ヘッダ(Directx,Windows,固定機能)
//-------------------------------------------------------
// よく使うヘッダまとめ
//=======================================================
#ifndef Include_Lib_H
#define Include_Lib_H

//------------------------------------------
// マクロ
//------------------------------------------
#define WIN32_LEAN_AND_MEAN         //ヘッダーからあまり使われない関数を省く
#define DIRECTINPUT_VERSION	0x0800  //DirectInputバージョン警告回避マクロ
#define DIRECTSOUND_VERSION 0x1000  //DirectSoundバージョン警告回避マクロ

//------------------------------------------
// ヘッダ
//------------------------------------------

//!外部ライブラリ(DirectX)
#include <MMSystem.h>
#include <tchar.h>
#include <stdlib.h>
#include <d3dx9.h>
#include <dsound.h>
#include <dinput.h>

//!エラー関係
#ifdef _DEBUG
#include <dxerr.h>
#include <assert.h>
#endif

//!独自ヘッダ(常時使用するクラス)
#include "LibDef.h"
#include "Dx.h"
#include "Com_ptr.h"
#include "Smart_ptr.h"
#include "LostResource.h"
#include "LostDevice.h"

#endif