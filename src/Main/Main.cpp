//+**************************************************************************************
//| メインプログラム
//|--------------------------------------------------------------------------------------
//| プログラムの開始
//| システムの開始・ウィンドの初期設定
//+**************************************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <stdio.h>
#include <fstream>
#include <shlwapi.h>
#include "resource.h"
#include "Dialog.h"
#include "Main.h"
#include "DirectInput.h"
#include "ControlFPS.h"
#include "MainSequence.h"
#include "FileLoader.h"
#include "FBXLoader.h"
#include "FBX.h"
#include "Mesh.h"
#include "Projection.h"

// -------------------------------------------------
// Windowの作成
// -------------------------------------------------
INT WINAPI _tWinMain( HINSTANCE hInst, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, s32 nCmdShow )
{
    //!メモリリーク検知
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

    //!COMの初期化
    CoInitialize(NULL);

    //!windowの登録
    WNDCLASSEX wc =
    {
        sizeof( WNDCLASSEXW ), CS_CLASSDC|CS_HREDRAW | CS_VREDRAW , MsgProc, 0L, 0L,
        GetModuleHandle( NULL ), LoadIcon( hInst, MAKEINTRESOURCE(IDI_ICON1)), NULL, NULL, NULL,
        _T("MeshViewer"), NULL
    };

    RegisterClassEx( &wc );

    HWND hWnd = CreateWindow( _T("MeshViewer"), _T("MeshViewer ver1.2"),
                        WS_OVERLAPPEDWINDOW , 240, 70, static_cast<s32>(Dx::Get()->GetWindowWidth() ),
                        static_cast<s32>(Dx::Get()->GetWindowHeight()), NULL, NULL, wc.hInstance, NULL );

    Dx::Get()->SetHinstance(hInst);
    Dx::Get()->SetMainWindHandle(hWnd);     //ウィンドハンドルの登録

    //!DirectX関連・メインループ
    if( Dx::Get()->InitDirectX() )          //DirectXの初期化
    {
        ShowWindow( hWnd, nCmdShow );       //Window表示
        UpdateWindow( hWnd );
#ifdef _DEBUG
        InitDebugLog();                     //デバッグログ初期化
#endif
        
        //DirectInputの初期化
        if( FAILED( Input::DirectInput::Get()->SetDirectInput( hWnd, hInst, Input::DirectInput::Get()->ModeKeyMouse ) ) )
        {
            MessageBox( Dx::Get()->GetMainHwnd(), _T("DirectInput初期化エラー"),_T("DirectInput") , MB_OK );
            Dx::Get()->SystemError(false);//進行不能なので強制終了
        }

        //メインループ
        WinMainRoop(hWnd);
    }
    else
    {
        MessageBox( Dx::Get()->GetMainHwnd(), _T("DirectX初期化エラー") , _T("DirectX"), MB_OK );
    }

    //!終了処理
    CleanUpMainDevice(); //メインデバイスの解放処理
    UnregisterClass( _T("MeshViewer"), wc.hInstance ); //Windowの解放
    CoUninitialize();

    return 0;
}

// -------------------------------------------------
// Windowsメッセージプロシージャ 
// -------------------------------------------------
LRESULT WINAPI MsgProc( HWND hWnd , UINT msg, WPARAM wParam, LPARAM lParam )
{
    TCHAR T_Chara[512]={0};
    u32 Count = 0;
    int CommandNum = 0;
    LPTSTR *lplpszArgs = 0;

    switch(msg)
    {
        //====Window作成=======================
        case WM_CREATE:
            DragAcceptFiles( hWnd, TRUE );  //ドラッグ&ドロップ受付
            Dx::Get()->CleanUpDirectXVar(); //Directx用変数解放
        break;

        //====アクティブ切り替え================
        case WM_ACTIVATE:
            if( wParam != WA_INACTIVE)
            {
                Input::DirectInput::Get()->ReAcquire(); //キーの受付再開
            }
        break;

        //====閉じる============================
        case WM_DESTROY:
            PostQuitMessage(0);
        break;

        //====キー入力==========================
        case WM_KEYDOWN:
            if( wParam == VK_ESCAPE )   //強制終了
                PostQuitMessage(0);
        break;

        //====ドラッグ&ドロップ==================
        case WM_DROPFILES:
            //!ファイル名を調べる
            Count = DragQueryFile( (HDROP)wParam, 0, 0, 0 );
            if( Count > 512 )
            {
                MessageBox( Dx::Get()->GetMainHwnd(), _T("ファイルパスが長すぎます"), _T("エラー"), MB_OK );
                break;
            }
            DragQueryFile( (HDROP)wParam, 0, T_Chara, sizeof(T_Chara) );
            DragFinish((HDROP)wParam);

            //!メッシュバッファ再作成
            Object::Mesh::Release();
            Object::Mesh::LoadResource(T_Chara);
            Object::Mesh::CreateBuf();

            //!ダイアログ再作成
            SendMessage( Dx::Get()->GetDialogHwnd(), UM_MESSAGE01, wParam, lParam );
        break;

        //====画面サイズ変更======================
        case WM_SIZE:
            Dx::Get()->ChangeWindowSize( LOWORD(lParam), HIWORD(lParam) );
            Camera::Projection::Get()->CreateProjMat( LOWORD(lParam), HIWORD(lParam) );
        break;

        //====デフォルト==========================
        default:
            return DefWindowProc( hWnd, msg, wParam, lParam );
    }
    return (0L);
}

// -------------------------------------------------
// メインループ
// -------------------------------------------------
void WinMainRoop(HWND hWnd)
{
    //!コマンドラインチェック
    int CommandNum = 0;
    LPTSTR *lplpszArgs = CommandLineToArgvW( GetCommandLine(), &CommandNum );
    if( CommandNum > 1 )    //関連付けられたファイルから起動する場合
    {
        PathRemoveFileSpec(lplpszArgs[0]);
        SetCurrentDirectory( lplpszArgs[0] );//カレントディレクトリをexeの場所に戻す
    }
    LocalFree(lplpszArgs);

    //!アーカイブの情報を読み込む
    FileLoader::FileLoader::Get()->SetLoadSystem(true);
    if(!FileLoader::FileLoader::Get()->CreateFileTable())
    return;

    //!システムメモリ領域確保
    MSG msg;
    BOOL Ret;
    SecureZeroMemory( &msg, sizeof( msg ) );
    MainSequence *pSequenceMain = new MainSequence; //シーケンスメイン

    //!初回の設定
    System::ControlFPS::Get()->SetFPS(60);
    Camera::Projection::Get()->CreateProjMat( Dx::Get()->GetWindowWidth(), Dx::Get()->GetWindowHeight() );

    //!/関連付けられたファイルから起動する場合ファイル読み込む
    CommandNum = 0;
    lplpszArgs = CommandLineToArgvW( GetCommandLine(), &CommandNum );
    if( CommandNum > 1 )
    {
        Object::Mesh::Release();
        Object::Mesh::LoadResource( lplpszArgs[1] );
        Object::Mesh::CreateBuf();
    }
    LocalFree(lplpszArgs);

    //!ダイアログ作成
    HWND hDlg = CreateDialog( (HINSTANCE)GetWindowLong( hWnd, GWL_HINSTANCE), MAKEINTRESOURCE(ViewerController), hWnd, DialogProc );
    Dx::Get()->SetDialogHandle(hDlg);
    ShowWindow( hDlg, SW_NORMAL );

    //メインループ
    while(TRUE)
    {
        if( PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) )
        {
            Ret = GetMessage( &msg, NULL, 0, 0);
            if( !Ret || msg.message == WM_QUIT )
            {
                break;
            }
            if( !hDlg || !IsDialogMessage( hDlg, &msg ) )
            {
                TranslateMessage( &msg );
                DispatchMessage( &msg );
            }
        }else
        {
        //======================================================
        // メイン処理
        //======================================================
            System::ControlFPS::Get()->ControlMain();       //FPSの制御

            pSequenceMain->Input();                         //入力
            pSequenceMain->Update();                        //更新
            if( !System::ControlFPS::Get()->CheckFrameSkip() )
                pSequenceMain->Draw();                      //描画
            pSequenceMain->UpdateSequence();                //シーケンス更新
        }

        System::LostDevice::Get()->CleanupResourse();       //COMリソースの定期整理

        #ifdef _DEBUG
            System::ControlFPS::Get()->ShowFPS(hWnd);       //FPSの表示
        #endif
    }

    //解放処理
    DestroyWindow(hDlg);
    Object::Mesh::Release();
    SAFE_DELETE(pSequenceMain);
}

// -------------------------------------------------
// メインデバイスの解放処理
// -------------------------------------------------
void CleanUpMainDevice()
{
    Dx::Get()->CleanUpDirectXVar(); //DirectX解放処理
}

// -------------------------------------------------
// デバッグ用ログファイル初期化
// -------------------------------------------------
void InitDebugLog()
{
}
