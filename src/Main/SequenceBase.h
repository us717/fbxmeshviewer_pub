//=======================================================
//シーケンス用基底クラス
//-------------------------------------------------------
// 継承先で用意
// →Input(),Update(),Draw(),CheckSequence()
//=======================================================
#ifndef Include_SequenceBase_H
#define Include_SequenceBase_H

//====<インクルード>=============================
#include "DirectInput.h"

// ------------------------------------------
// クラス:シーケンス基底
// ------------------------------------------
class SequenceBase{
private:
//=====名前空間=======================
    typedef Input::DirectInput DInput;

//=====構築子：消去子=================
public:
    SequenceBase() { pNextSequence = 0; }
    virtual ~SequenceBase(){ pNextSequence = 0; }

//=====変数===========================
protected:
    SequenceBase    *pNextSequence; //次に移動するシーケンス

//=====関数===========================

//!公開:基底
public:
    virtual void Input()    = 0;
    virtual void Update()   = 0;
    virtual void Draw()     = 0;
    virtual SequenceBase* CheckSequence( SequenceBase* pTempSequence ) = 0;
};

#endif