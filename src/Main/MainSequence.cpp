//+****************************************************************
//| メインシーケンス(メインループ)
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include "SequenceBase.h"
#include "MainSequence.h"
#include "DirectInput.h"
#include "Viewer.h"

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
MainSequence::MainSequence():pDev( System::LostDevice::Get() ),LostFlag(false)
{
    //!初期シーケンス
    pCurSequence = new Viewer;

    //!初期背景色
    Red     = 57;
    Green   = 57;
    Blue    = 57;
}

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
MainSequence::~MainSequence()
{
    pNextSequence = 0;
    SAFE_DELETE(pCurSequence);
}

// -------------------------------------------------
// 入力
// -------------------------------------------------
void MainSequence::Input()
{
    pCurSequence->Input();
}

// -------------------------------------------------
// 更新
// -------------------------------------------------
void MainSequence::Update()
{
    if(!LostFlag)
    pCurSequence->Update();
}

// -------------------------------------------------
// 描画
// -------------------------------------------------
void MainSequence::Draw()
{
    //!バックバッファの初期化
    Dx::Get()->GetD3DDevice()->Clear( 0, 0, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(Red, Green, Blue), 1.0f, 0 );

    //!シーンの開始
    Dx::Get()->GetD3DDevice()->BeginScene();

    //!描画処理
    pCurSequence->Draw();

    //!シーンの終了
    Dx::Get()->GetD3DDevice()->EndScene();

    //!バッグバッファの転送・デバイスロスト検出
    if( Dx::Get()->GetD3DDevice()->Present( NULL, NULL, NULL, NULL) == D3DERR_DEVICELOST )
    {
        LostFlag = true;
        LostFlag = pDev->RecoverLostDevice(); //デバイスロスト時の復帰処理
    }
}

// -------------------------------------------------
// シーケンスの更新
// -------------------------------------------------
void MainSequence::UpdateSequence()
{
    pNextSequence = pCurSequence->CheckSequence(pCurSequence);
    //シーケンス遷移
    if( pNextSequence != pCurSequence )
    {
        SAFE_DELETE(pCurSequence);
        pCurSequence = pNextSequence;
    }
}

// -------------------------------------------------
// 背景色変更
// -------------------------------------------------
// (in) Red     :背景色(赤)
// (in) Green   :背景色(緑)
// (in) Blue    :背景色(青)
// -------------------------------------------------
void MainSequence::SetBackColor( const u8 &Red, const u8 &Green, const u8 &Blue )
{
    this->Red       = Red;
    this->Green     = Green;
    this->Blue      = Blue;
}