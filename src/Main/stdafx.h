//=======================================================
// プリコンパイル済みヘッダー
//=======================================================
#ifndef Include_stdafx__H
#define Include_stdafx__H

//====<インクルード>===============================
#define STRICT      //型チェックを厳密に行う

//!システム関連
#include <windows.h>

//!プリコンパイル済みHLSL
#include<fx_Grid.h>
#include<fx_MeshAnim.h>
#include<fx_MeshFix.h>

#endif