//+**************************************************************************************
//| Window・DirectX制御
//+**************************************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include "LostDevice.h"

// ------------------------------------------
// コンストラクタ
// ------------------------------------------
Dx::Dx():
    hWnd(0),
    pD3DIF(0),
    pD3DDevice(0),
    pSprite(0),
    WindowWidth(800.0f),
    WindowHeight(600.0f),
    ErrorHandle(true)
{
    CurWidthRate    = WindowWidth / DEFALT_WIDTH;
    CurHeightRate   = WindowHeight / DEFALT_HEIGHT;
}

// ------------------------------------------
// private:Directxインターフェイスセット
// ------------------------------------------
// (out)    bool    :フラグ(失敗false)
// ------------------------------------------
bool Dx::CreateD3DIF()
{
#ifdef _DEBUG
    assert( pD3DIF == 0);
#endif
    pD3DIF = Direct3DCreate9(D3D_SDK_VERSION);

    if( pD3DIF == 0)
        return false;
    return true;
}

// ------------------------------------------
// private:Directxデバイスの作成
// ------------------------------------------
// (out)    bool    :フラグ(失敗false)
// ------------------------------------------
bool Dx::CreateD3DDevice()
{
#ifdef _DEBUG
    assert( pD3DDevice == 0 );
#endif
    SecureZeroMemory( &d3dpp, sizeof(d3dpp) );
    d3dpp.BackBufferCount           = 1;
    d3dpp.MultiSampleType           = D3DMULTISAMPLE_NONE;
    d3dpp.MultiSampleQuality        = 0;
    d3dpp.SwapEffect                = D3DSWAPEFFECT_DISCARD;
    d3dpp.BackBufferFormat          = D3DFMT_UNKNOWN;
    d3dpp.hDeviceWindow             = 0;
    d3dpp.Windowed                  = TRUE;
    d3dpp.EnableAutoDepthStencil    = TRUE;
    d3dpp.AutoDepthStencilFormat	= D3DFMT_D16;
    d3dpp.FullScreen_RefreshRateInHz= 0;
    d3dpp.PresentationInterval      = D3DPRESENT_INTERVAL_IMMEDIATE; //ループ内でV-synk同期 DEFAULT IMMEDIATE

    if( FAILED( GetD3DIF()->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
        D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pD3DDevice ) ) )
    {
        return false;
    }

    return true;
}

// ------------------------------------------
// private:Spriteインターフェイス作成
// ------------------------------------------
bool Dx::CreateSprite()
{
    if( FAILED(D3DXCreateSprite( pD3DDevice, &pSprite ) ) )
        return false;
    return true;
}

// ------------------------------------------
// private:ビューポートの作成
// ------------------------------------------
// (out)    bool    :フラグ(失敗false)
// ------------------------------------------
bool Dx::CreateViewPort()
{
    D3DVIEWPORT9 vp;
    vp.X        = 0;
    vp.Y        = 0;
    vp.Width    = d3dpp.BackBufferWidth;
    vp.Height   = d3dpp.BackBufferHeight;
    vp.MinZ     = 0.0f;
    vp.MaxZ     = 1.0f;
    if( FAILED( Dx::Get()->GetD3DDevice()->SetViewport(&vp)) )
    {
        return false;
    }
    return true;
}

// ------------------------------------------
// DirectXの初期化
// ------------------------------------------
bool Dx::InitDirectX()
{
    if(!CreateD3DIF())                  //IDirect3D9インターフェイスの取得
        return false;
    if(!CreateD3DDevice())              //D3DDeviceの作成
        return false;
    if(!CreateSprite())                 //Spriteインターフェイスの取得
        return false;
    if(!Dx::Get()->CreateViewPort())    //ビューポートの作成
        return false;
    return true;
}

// ------------------------------------------
// DirectX変数解放
// ------------------------------------------
void Dx::CleanUpDirectXVar()
{
    if( pSprite != 0 ){
        pSprite->Release();
        pSprite = 0;
    }
    if( pD3DDevice != 0 ){
        pD3DDevice->Release();
        pD3DDevice = 0;
    }
    if( pD3DIF != 0 ){
        pD3DIF->Release();
        pD3DIF = 0;
    }
}

// ------------------------------------------
// 画面サイズ変更
// ------------------------------------------
bool Dx::ChangeWindowSize( const u32 &Width, const u32 &Height )
{
    d3dpp.BackBufferWidth=Width;
    d3dpp.BackBufferHeight=Height;
    CurWidthRate    = static_cast<f32>(Width) / DEFALT_WIDTH;
    CurHeightRate   = static_cast<f32>(Height) / DEFALT_HEIGHT;

    System::LostDevice::Get()->ReleaseD3DObject(); //D3Dリソース解放

    HRESULT hr = pD3DDevice->Reset(&d3dpp);
    if( hr == D3DERR_DEVICELOST )
    {
        return true;
    }
    if( FAILED(hr) )
    {
        return false;
    }

    System::LostDevice::Get()->RecoverD3DObject();

    D3DVIEWPORT9 vp;
    vp.X        = 0;
    vp.Y        = 0;
    vp.Width    = d3dpp.BackBufferWidth;
    vp.Height   = d3dpp.BackBufferHeight;
    vp.MinZ     = 0.0f;
    vp.MaxZ     = 1.0f;
    if( FAILED( Dx::Get()->GetD3DDevice()->SetViewport(&vp)) )
    {
        return false;
    }

    return true;
}

// ------------------------------------------
// キルスイッチ
// ------------------------------------------
// 深刻なエラー、非常時の強制終了
// ------------------------------------------
// (in) bool    : falseで強制終了
// ------------------------------------------
void Dx::SystemError( bool Handle )
{
    ErrorHandle = Handle;
    if(!ErrorHandle)
    {
        MessageBox( Dx::Get()->GetMainHwnd(), _T("予期せぬエラーが発生しました。\nプログラムを終了します。"),
                    _T("システムエラー") , MB_OK );
        PostQuitMessage(0);
    }
}