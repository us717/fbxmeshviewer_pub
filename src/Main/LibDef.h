//=======================================================
// ライブラリ定義
//-------------------------------------------------------
// プログラム全体に影響を及ぼす定義・機能
//=======================================================
#ifndef Include_LibDef_H
#define Include_LibDef_H

//------------------------------------------
// 型名typedef
//------------------------------------------
typedef     char                    s8;
typedef     unsigned char           u8;
typedef     short int               s16;
typedef     unsigned short int      u16;
typedef     int                     s32;
typedef     unsigned int            u32;
typedef     float                   f32;
typedef     double                  f64;
typedef     D3DXVECTOR2             VEC2;
typedef     D3DXVECTOR3             VEC3;
typedef     D3DXVECTOR4             VEC4;
typedef     D3DXMATRIX              MATRIX;

//------------------------------------------
// 独自定義
//------------------------------------------
#define UM_MESSAGE01    (WM_APP+1)

//------------------------------------------
// メモリ解放用マクロ
//------------------------------------------

//!====<COM>=====
#define SAFE_RELEASE(x) do{ if(x) {(x) ->Release(); (x) = 0;} }while(0)     //解放(配列は個別解放すること)

//!====<通常>=====
#define SAFE_DELETE(x) do{ if(x) { delete(x); (x) = 0;} }while(0)           //解放

#define SAFE_DELETE_ARRAY(x) do{if(x) { delete[](x); (x) = 0;} }while(0)    //配列解放

//------------------------------------------
// Unicode対応関係
//------------------------------------------
#ifdef UNICODE
#define _tWinmain wWinmain
#else
#define _tWinmain WinMain
#endif

//------------------------------------------
// デバッグ出力
//------------------------------------------
// 出力に変数を利用できる (Cのように利用)
//------------------------------------------
#ifdef _DEBUG
#define ExOutputDebugString( str, ... ) \
        do{ \
            TCHAR c[256]; \
            _stprintf_s( c, str, __VA_ARGS__ ); \
            OutputDebugString( c ); \
        }while(0)
#else
#define ExOutputDebugString( str, ... ) // 空実装
#endif

//------------------------------------------
// float誤差修正
//------------------------------------------

//! 許容誤差
#define FL_EPS      FLT_EPSILON //要求精度によって調節してください

// ==
inline bool FL_EQUAL( const f32 &a, const f32 &b )
{
    return fabs(a-b) < FL_EPS ? true : false ;
}

// >
inline bool FL_LARGE( const f32 &a, const f32 &b )
{ 
    return (a-b) >= (FL_EPS * (a+b) ) ? true : false ;
}

// <
inline bool FL_SMALL( const f32 &a, const f32 &b )
{
    return (a-b) <= (FL_EPS * (a+b)) ? true : false ;
}

#endif