//+****************************************************************
//| オブジェクト:グリッド線
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include "ShaderLoader.h"
#include "GridLines.h"

namespace Object{

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
GridLines::GridLines():
    pOffsetBuf(0),pGridDef(0),VertexCount(0),PolygonCount(0),UpdateFlag(false),Height(0.0f),X(0),Z(0)
{
    //!変数初期値
    Width = 1.0f;                                   //初期幅
    Color = VEC4( 0.29f, 0.29f, 0.29f, 1.0f );      //初期色
    D3DXMatrixIdentity(&WorldMat);

    //!頂点要素の定義
    D3DVERTEXELEMENT9 mdecl[]=
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        D3DDECL_END()
    };
    for( s32 i=0 ; i<2 ; i++ )
    {
        decl[i] = mdecl[i];
    }
    Dx::Get()->GetD3DDevice()->CreateVertexDeclaration( decl, &pVertexDecl );

    //!シェーダー登録
    bool h = false;
#ifdef SH_OBJ
    h = FileLoader::ShaderLoader::Get().LoadFromObject( _T("Shader/HLSLObj/fx_Grid.cfx"), &Effect );
#else
    size_t Size = sizeof(g_fx_Grid);
    h = FileLoader::ShaderLoader::Get().LoadFromHeader( g_fx_Grid, Size, &Effect );
#endif
#ifdef _DEBUG
    assert(h == true);
#endif
}

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
GridLines::~GridLines()
{
    SAFE_RELEASE(pOffsetBuf);
    SAFE_DELETE_ARRAY(pGridDef);
    SAFE_RELEASE(pVertexDecl);
}

// -------------------------------------------------
// private: バッファの作成
// -------------------------------------------------
void GridLines::CreateBuf()
{
    //!バッファ初期化処理
    pGridDef = new VEC3[VertexCount];
    if(pOffsetBuf)
    {
        SAFE_RELEASE(pOffsetBuf);
    }
    //!X軸グリッドのオフセット指定
    f32 AbsoluteX = Width * (f32)(Z-1) / 2; //0.0を基準としたXオフセット絶対値
    f32 TmpZ = Width * (f32)(X-1) / 2;      //Zオフセット

    for( u32 i=0 ; i<X*2 ; ++i )
    {
        if( i % 2 == 0 )
        {
            pGridDef[i].x = AbsoluteX; pGridDef[i].y = Height; pGridDef[i].z = TmpZ;
        }
        else
        {
            pGridDef[i].x = -1.0f * AbsoluteX; pGridDef[i].y = Height; pGridDef[i].z = TmpZ;
            TmpZ -= Width;
        }
    }

    //!Z軸グリッドのオフセット指定
    f32 AbsoluteZ = Width * (f32)(Z-1) / 2; //0.0を基準としたZオフセット絶対値
    f32 TmpX = Width * (f32)(X-1) / 2;      //Xオフセット

    for( u32 i=X*2 ; i<VertexCount ; ++i )
    {
        if( i % 2 == 0 )
        {
            pGridDef[i].x = TmpX; pGridDef[i].y = Height; pGridDef[i].z = AbsoluteZ;
        }
        else
        {
            pGridDef[i].x = TmpX; pGridDef[i].y = Height; pGridDef[i].z = -1.0f * AbsoluteZ;
            TmpX -= Width; //幅
        }
    }

    //!バッファの作成
    Buf::CreateOffsetBuf( VertexCount, pGridDef, &pOffsetBuf ); //頂点

    //!要素解放
    SAFE_DELETE_ARRAY(pGridDef);
}

// -------------------------------------------------
// 線の数指定
// -------------------------------------------------
// ・奇数を指定(原点一本 + 両サイドに広がっていくため)
// ・両軸とも同じ本数になる
// -------------------------------------------------
// (in) GridNum     :一つの軸上でのグリッド数
// -------------------------------------------------
void GridLines::SetGridNum( const unsigned &GridNum )
{
#ifdef _DEBUG
    assert( GridNum % 2 == 1 && _T("奇数を指定してください") );
#endif
    X = GridNum;
    Z = GridNum;
    VertexCount = GridNum * 4;
    PolygonCount = VertexCount / 2;
    UpdateFlag = true;
}

// -------------------------------------------------
// 高さ指定
// -------------------------------------------------
// (in) Height  :高さ
// -------------------------------------------------
void GridLines::SetHeight( const f32 &Height )
{
    this->Height = Height;
    UpdateFlag = true;
}

// -------------------------------------------------
// 色指定
// -------------------------------------------------
//  (in)    Color   :色情報(RGBA)(float)
// -------------------------------------------------
void GridLines::SetColor( const VEC4 &Color )
{
    this->Color = Color;
}

// -------------------------------------------------
// 更新
// -------------------------------------------------
void GridLines::Update()
{
    //!バッファ情報に変更があったらバッファを再作成
    if(UpdateFlag)
    {
        CreateBuf();
    }

    UpdateFlag = false;
}

// -------------------------------------------------
// 描画
// -------------------------------------------------
// (in) ProjMat     :プロジェクション行列
// (in) ViewMat     :ビュー行列
// -------------------------------------------------
void GridLines::Draw( const MATRIX &ProjMat, const MATRIX &ViewMat )
{
    //!ストリームのセット
    Dx::Get()->GetD3DDevice()->SetVertexDeclaration(pVertexDecl);
    Dx::Get()->GetD3DDevice()->SetStreamSource( 0, pOffsetBuf, 0, sizeof(VEC3) );

    //!テクニック
    UINT numPass;
    Effect.GetPtr()->SetTechnique("GridShader");
    Effect.GetPtr()->Begin( &numPass, 0 );
    Effect.GetPtr()->BeginPass(0);
    Effect.GetPtr()->SetMatrix( "World", &WorldMat );
    Effect.GetPtr()->SetMatrix( "View", &ViewMat );
    Effect.GetPtr()->SetVector( "Diffuse", &Color );
    Effect.GetPtr()->SetMatrix( "Proj", &ProjMat );
    Effect.GetPtr()->CommitChanges();
    Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_LINELIST, 0, VertexCount );
    Effect.GetPtr()->EndPass(); 
    Effect.GetPtr()->End();
}
}