//=======================================================
// オブジェクト:軸(XYZ)
// ------------------------------------------------------
// カメラは左上、オブジェクトは重ねて表示する仕様
// ------------------------------------------------------
// <カメラが対象>            :Updateでワールド行列作成→描画
// <オブジェクトが対象>      :描画時に、オブジェクトのワールド行列、
//                           ビュー行列を渡してやる
//=======================================================
#ifndef Include_Axis_H
#define Include_Axis_H

//====<インクルード>=================================
#include "BufferFormat.h"

//====<前方宣言>=====================================
namespace Object{

// -------------------------------------------------
// クラス: 軸
// -------------------------------------------------
// ・オブジェクトごとにクラスを作成すること
// ・頂点バッファの共有は対応していない
// -------------------------------------------------
class Axis{

//=====構築子：消去子=================
private:
    //!コピー禁止
    void operator = ( const Axis &src ){}
    Axis( const Axis &src ){}
public:
    //!コンストラクタ・デストラクタ
    Axis();
    ~Axis();

//====名前空間========================
private:
    typedef Draw::Mesh::BufferFormat    Buf;
//=====変数===========================
private:
    //!バッファ要素
    LPDIRECT3DVERTEXDECLARATION9        pVertexDecl;            //頂点宣言オブジェクト
    D3DVERTEXELEMENT9                   decl[3];                //バッファ要素格納
    LPDIRECT3DVERTEXBUFFER9             pOffsetBuf;             //オフセットバッファ
    LPDIRECT3DVERTEXBUFFER9             pColorbuf;              //カラーバッファ
    System::Com_ptr<ID3DXEffect>        Effect;                 //HLSLハンドル

    //!Axis要素
    VEC3                                *pAxisDef;              //グリッド座標格納
    D3DCOLOR                            *pColorDef;             //色格納
    MATRIX                              WorldMat;               //変換行列
    u32	                                VertexNum;              //頂点数

    //!フラグ
    bool                                DrawFlag;               //フラグ:描画対象はカメラかどうか(カメラ == true)

    //!ビュー行列
    MATRIX                              *pPrivateView;          //常時描画用ビュー行列

//=====関数===========================
public:
    void CreateBuf( bool TargetCam = true );                                //バッファの作成
    void Update( const MATRIX *pTargetViewMat = 0 );                        //更新
    void Draw( const MATRIX *pProjMat = 0, const MATRIX *pObjWorldMat = 0,  //描画
                const MATRIX *pViewMat	= 0 );
};

}
#endif