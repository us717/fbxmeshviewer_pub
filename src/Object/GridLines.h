//=======================================================
// オブジェクト:グリッド線(マス目)
// ------------------------------------------------------
// 基本は各種設定→Update→draw
// 変更があればUpdateでバッファ再作成している
//=======================================================
#ifndef Include_GridLines_H
#define Include_GridLines_H

//====<インクルード>=================================
#include "BufferFormat.h"

//====<前方宣言>=====================================

namespace Object{

// -------------------------------------------------
// クラス: グリッド線
// -------------------------------------------------
// ・オブジェクトごとにクラスを作成すること
// ・頂点バッファの共有は対応していない
// -------------------------------------------------
class GridLines {

//=====構築子：消去子=================
private:
    //!コピー禁止
    void operator = ( const GridLines &src ){}
    GridLines( const GridLines &src ){}
public:
    //!コンストラクタ・デストラクタ
    GridLines();
    ~GridLines();

//====名前空間========================
private:
    typedef Draw::Mesh::BufferFormat    Buf;

//=====変数===========================
private:
    //!バッファ要素
    LPDIRECT3DVERTEXDECLARATION9        pVertexDecl;    //頂点宣言オブジェクト
    D3DVERTEXELEMENT9                   decl[2];        //バッファ要素格納
    LPDIRECT3DVERTEXBUFFER9             pOffsetBuf;     //オフセットバッファ
    System::Com_ptr<ID3DXEffect>        Effect;         //HLSLハンドル

    //!グリッド要素
    VEC3                                *pGridDef;      //グリッド座標格納
    u32                                 VertexCount;    //頂点数
    u32                                 PolygonCount;   //ポリゴン数
    VEC4                                Color;          //色(RGBA)
    MATRIX                              WorldMat;       //変換行列
    bool                                UpdateFlag;     //フラグ：更新

    //!座標
    f32                                 Height;         //高さ
    f32                                 Width;          //幅
    u32                                 X;              //X軸の本数
    u32                                 Z;              //Y軸の本数

//=====関数===========================
private:
    void CreateBuf();                                   //バッファの作成

public:
    void SetGridNum( const unsigned &GridNum );         //線の数指定
    void SetHeight( const f32 &Height );                //高さ指定
    void SetColor( const VEC4 &Color );                 //色指定
    void Update();                                      //更新(変更反映・バッファの作成)
    void Draw( const MATRIX &ProjMat, const MATRIX &ViewMat );//描画
};


}


#endif