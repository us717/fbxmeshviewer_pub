//+****************************************************************
//| オブジェクト:軸
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include "ShaderLoader.h"
#include "Axis.h"
namespace Object{

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
Axis::Axis()
    :pOffsetBuf(0),pColorbuf(0),pAxisDef(0),pColorDef(0),VertexNum(6),pPrivateView(0),DrawFlag(false)
{
    //!変数初期化
    D3DXMatrixIdentity(&WorldMat);

    //!頂点要素の定義
    D3DVERTEXELEMENT9 mdecl[]=
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        { 1, 0, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
        D3DDECL_END()
    };
    for( s32 i=0 ; i<3 ; i++ )
    {
        decl[i] = mdecl[i];
    }
    Dx::Get()->GetD3DDevice()->CreateVertexDeclaration( decl, &pVertexDecl );

    //!シェーダー登録
    bool h = false;
#ifdef SH_OBJ
    h = FileLoader::ShaderLoader::Get().LoadFromObject( _T("Shader/HLSLObj/fx_Grid.cfx"), &Effect );
#else
    size_t Size = sizeof(g_fx_Grid);
    h = FileLoader::ShaderLoader::Get().LoadFromHeader( g_fx_Grid, Size, &Effect );
#endif
#ifdef _DEBUG
    assert(h == true);
#endif
}

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
Axis::~Axis()
{
    SAFE_RELEASE(pOffsetBuf);
    SAFE_RELEASE(pColorbuf);
    SAFE_RELEASE(pVertexDecl);
    SAFE_DELETE_ARRAY(pAxisDef);
    SAFE_DELETE_ARRAY(pColorDef);
    SAFE_DELETE(pPrivateView);
}

// -------------------------------------------------
// private: バッファの作成
// -------------------------------------------------
// (in) TargetCam   :Axis対象はカメラかどうか(trueでカメラ)
// -------------------------------------------------
void Axis::CreateBuf( bool TargetCam )
{
    DrawFlag = TargetCam;

//====領域確保・初期化処理====
    pAxisDef    = new VEC3[VertexNum];
    pColorDef   = new D3DCOLOR[VertexNum];
    if(pOffsetBuf)
        SAFE_RELEASE(pOffsetBuf);
    if(pColorbuf)
        SAFE_RELEASE(pColorbuf);

//====バッファ定義============
    //!頂点
    pAxisDef[0].x = 0.0f;   pAxisDef[0].y = 0.0f;   pAxisDef[0].z = 0.0f;   //X
    pAxisDef[1].x = 0.15f;  pAxisDef[1].y = 0.0f;   pAxisDef[1].z = 0.0f;
    pAxisDef[2].x = 0.0f;   pAxisDef[2].y = 0.0f;   pAxisDef[2].z = 0.0f;   //Y
    pAxisDef[3].x = 0.0f;   pAxisDef[3].y = 0.15f;  pAxisDef[3].z = 0.0f;
    pAxisDef[4].x = 0.0f;   pAxisDef[4].y = 0.0f;   pAxisDef[4].z = 0.0f;   //Z
    pAxisDef[5].x = 0.0f;   pAxisDef[5].y = 0.0f;   pAxisDef[5].z = 0.15f;

    //!色
    pColorDef[0] = D3DCOLOR_XRGB( 220,   0,   0 );  //X
    pColorDef[1] = D3DCOLOR_XRGB( 220,   0,   0 );
    pColorDef[2] = D3DCOLOR_XRGB(   0, 220,   0 );  //Y
    pColorDef[3] = D3DCOLOR_XRGB(   0, 220,   0 );
    pColorDef[4] = D3DCOLOR_XRGB(   0,   0, 220 );  //Z
    pColorDef[5] = D3DCOLOR_XRGB(   0,   0, 220 );

//====バッファの作成==========
    Buf::CreateOffsetBuf( VertexNum, pAxisDef, &pOffsetBuf );   //頂点
    Buf::CreateColorBuf( VertexNum, pColorDef, &pColorbuf );    //色

//====要素解放================
    SAFE_DELETE_ARRAY(pAxisDef);
    SAFE_DELETE_ARRAY(pColorDef);

//====タイプ:ワールドの処理====
    if(DrawFlag)
    {
        //Axis常時描画用カメラの作成
        pPrivateView = new MATRIX;
        VEC3 Eye;       Eye.x = 0.0f, Eye.y = 0.0f, Eye.z = -3.0f;
        VEC3 At;        At.x = 0.0f, At.y = 0.0f, At.z = 0.0f;
        VEC3 Up;        Up.x = 0.0f, Up.y = 1.0f, Up.z = 0.0f;
        D3DXMatrixLookAtLH( pPrivateView, &Eye, &At, &Up);
    }
}

// -------------------------------------------------
// 更新
// -------------------------------------------------
// カメラのAxisを表示する場合に使用
// カメラ用の変換行列を作成する
// -------------------------------------------------
// (in) pTargetViewMat  :対象となるカメラのビュー行列
// -------------------------------------------------
void Axis::Update( const MATRIX *pTargetViewMat )
{
    if(DrawFlag)
    {
        memcpy( WorldMat.m[0], pTargetViewMat->m[0], sizeof( VEC3 ) );
        memcpy( WorldMat.m[1], pTargetViewMat->m[1], sizeof( VEC3 ) );
        memcpy( WorldMat.m[2], pTargetViewMat->m[2], sizeof( VEC3 ) );

        //!Axis位置指定
        WorldMat._41 = -1.5f;
        WorldMat._42 = 1.05f;
        WorldMat._43 = 0.0f;
    }
}

// -------------------------------------------------
// 描画
// -------------------------------------------------
// ↓対象がオブジェクトの場合入力
// -------------------------------------------------
// (in) pProjMat*       :プロジェクション行列
// (in) pObjWorldMat*   :対象のワールド行列
// (in) pViewMat*       :適応されているビュー行列
// -------------------------------------------------
void Axis::Draw( const MATRIX *pProjMat, const MATRIX *pObjWorldMat, const MATRIX *pViewMat )
{
    //!ストリームのセット
    Dx::Get()->GetD3DDevice()->SetVertexDeclaration(pVertexDecl);
    Dx::Get()->GetD3DDevice()->SetStreamSource( 0, pOffsetBuf, 0, sizeof(VEC3) );
    Dx::Get()->GetD3DDevice()->SetStreamSource( 1, pColorbuf, 0, sizeof(D3DCOLOR) );

    //!テクニック
    UINT numPass;
    Effect.GetPtr()->SetTechnique("GridShader");
    Effect.GetPtr()->Begin( &numPass, 0 );
    Effect.GetPtr()->BeginPass(1);

    if(DrawFlag)//対象によって切り替え
    {
        //カメラの場合
        Effect.GetPtr()->SetMatrix( "View", pPrivateView );
        Effect.GetPtr()->SetMatrix( "World", &WorldMat );
    }
    else
    {
        //オブジェクトの場合
#ifdef _DEBUG
        assert( pObjWorldMat != 0 && pViewMat != 0 && _T("引数の値が0です") );
#endif
        Effect.GetPtr()->SetMatrix( "View", pViewMat );
        Effect.GetPtr()->SetMatrix( "World", pObjWorldMat );
    }

    Effect.GetPtr()->SetMatrix( "Proj", pProjMat );
    Effect.GetPtr()->CommitChanges();
    Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_LINELIST, 0, 3 );
    Effect.GetPtr()->EndPass(); 
    Effect.GetPtr()->End();
}

}