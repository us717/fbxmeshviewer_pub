//+****************************************************************
//| ビューア表示用メッシュ
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <Shlwapi.h>
#include "BufferFormat.h"
#include "FileLoader.h"
#include "ShaderLoader.h"
#include "FBX.h"
#include "FBXData.h"
#include "AnimController.h"
#include "Mesh.h"

namespace Object{

// -------------------------------------------------
// static:定義
// -------------------------------------------------
    //!ファイル管理
    bool                                Mesh::TexState;                 //テクスチャの状態(読み込めているか)

    //!リソース
    FileLoader::FBXLoader::FBX          *Mesh::pFBX;                    //FBXデータ

    //!バッファ
    LPDIRECT3DTEXTURE9                  Mesh::pTexture;                 //テクスチャ
    D3DVERTEXELEMENT9                   Mesh::decl[5];                  //バッファ要素格納
    LPDIRECT3DVERTEXBUFFER9             Mesh::pOffsetBuf;               //オフセットバッファ
    LPDIRECT3DVERTEXBUFFER9             Mesh::pNormalBuf;               //法線バッファ
    LPDIRECT3DVERTEXBUFFER9             Mesh::pUVBuf;                   //UV情報
    LPDIRECT3DVERTEXBUFFER9             Mesh::pWeight;                  //ウェイトバッファ
    LPDIRECT3DVERTEXBUFFER9             Mesh::pIndexWeight;             //ウェイトインデックス
    MATRIX                              *Mesh::pInvMat;                 //アニメーション:逆行列
    Draw::Mesh::AnimController          *Mesh::pAnimController;         //アニメーションコントローラー

    //!描画フラグ
    Mesh::PrimitiveType_t               Mesh::PrimitiveType(POLYGON);   //プリミティブのタイプ
    Mesh::AnimPlayFlag_t                Mesh::AnimPlayFlag(PLAY);       //アニメ再生フラグ
    u32                                 Mesh::TakeNum(0);               //アニメテイク番号
    bool                                Mesh::AnimeFlag(true);          //アニメフラグ

// -------------------------------------------------
// static:リソース読み込み
// -------------------------------------------------
// (in)     pFBXFullPath    :テクスチャファイルパス
// (out)    bool            :フラグ
// -------------------------------------------------
bool Mesh::LoadResource( LPCTSTR pFBXFullPath )
{
#ifdef _DEBUG
    assert( pFBX==0 && pTexture==0 );
#endif

//====FBX読み込み=======================================
#ifdef _UNICODE
    char tmpFbxFilePath[512] ={0};
    WideCharToMultiByte( CP_ACP, 0, pFBXFullPath, -1, tmpFbxFilePath, (s32)((wcslen(pFBXFullPath)+1) * 2), NULL, NULL );
    FileLoader::FileLoader::Get()->CreateFBX( &pFBX, tmpFbxFilePath, _T("メッシュデータ"), true );
#else
    FileLoader::FileLoader::Get()->CreateFBX( &pFBX, pFBXFullPath, _T("メッシュデータ"), true );
#endif
    FileLoader::FileLoader::Get()->LoadFBX();
    FileLoader::FileLoader::Get()->LoadTex();

//====テクスチャ読み込み=================================
    //(注)1マテリアル対応なので、現状1つだけテクスチャバッファを作っている
    //!エラーチェック
    if( pFBX->GetData()->DataState == FBXData::NONE )
    {
        TexState = false;
        return false;
    }

    //!ディレクトリパスの作成
    u32 DirectSize = 0;
    TCHAR pFilePath[BUF_SIZE];
    for( u32 i=0 ; pFBXFullPath[i] !='\0' ; ++i ) //文字列コピー
    {
        pFilePath[i] = pFBXFullPath[i];
        ++DirectSize;
    }
    PathRemoveFileSpec( pFilePath ); //ディレクトリパスだけ抽出
    std::list<std::string>::iterator ite = pFBX->GetData()->pUVSet[0].TEXName.begin();

    //!エラーチェック
    size_t length = (*ite).length(); //stringサイズ
    if( length+1 > BUF_SIZE || (length+1)+DirectSize > BUF_SIZE )
    {
        MessageBox( Dx::Get()->GetMainHwnd(), _T("テクスチャ名が長すぎます"), _T("エラー"), MB_OK );
        TexState = false;
        return false;
    }

    //!パス名変換(tchar型へ)
    char *cTexName = new char[length+1];            //char*メモリ確保
    memcpy( cTexName, (*ite).c_str(), length+1 );   //string→char
    TCHAR tTexName[BUF_SIZE] = {0};                 //TChar初期化

#ifdef _UNICODE
    //!結合
    MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, cTexName, -1, tTexName, (sizeof(cTexName)+1) * 2 ); //char→TChar
    PathCombine( tTexName, pFilePath, tTexName );
#else
    PathCombine( tTexName, pFilePath, cTexName );
#endif
    SAFE_DELETE_ARRAY(cTexName);

    //!テクスチャ読み込み
    if( FAILED( D3DXCreateTextureFromFile( Dx::Get()->GetD3DDevice(), tTexName, &pTexture ) ) )
    {
        TexState = false;
        return false;
    }

    TexState = true;
    return true;
}

// -------------------------------------------------
// static:バッファ作成
// -------------------------------------------------
// (out)    bool    :フラグ
// -------------------------------------------------
bool Mesh::CreateBuf()
{
#ifdef _DEBUG
    assert( pFBX != 0 );
#endif

    //!頂点バッファ作成
    switch( pFBX->GetData()->DataState )
    {
        //====固定====================
        case FBXData::FIX:
            Buf::CreateOffsetBuf( pFBX->GetData()->VertexCount, pFBX->GetData()->pVertex, &pOffsetBuf );
            Buf::CreateUVBuf( pFBX->GetData()->VertexCount, &pFBX->GetData()->pUVSet[0], &pUVBuf );
        break;

        //====アニメ==================
        case FBXData::ANIME:
            //バッファ作成
            Buf::CreateOffsetBuf( pFBX->GetData()->VertexCount, pFBX->GetData()->pVertex, &pOffsetBuf );
            Buf::CreateUVBuf( pFBX->GetData()->VertexCount, &pFBX->GetData()->pUVSet[0], &pUVBuf );
            Buf::CreateWeightBuf( pFBX->GetData()->VertexCount, pFBX->GetData()->pBoneWeight, &pWeight );
            Buf::CreateWeightIndexBuf( pFBX->GetData()->VertexCount, pFBX->GetData()->pBoneWeight, &pIndexWeight );

            //アニメーション作成
            pAnimController = new AnimController;
            pInvMat = new MATRIX[ pFBX->GetData()->BoneCount ];
            pAnimController->CreateInvMat( pFBX->GetData(), pInvMat );
            pAnimController->CreateAnimMemory( pFBX->GetData()->BoneCount );
        break;

        //====データなし==============
        case FBXData::NONE:
        break;
    }

    //!メッシュ情報表示バッファ作成
    pFBX->GetData()->CreateInfo();
    return true;
}

// -------------------------------------------------
// static:リソース・バッファの解放
// -------------------------------------------------
// (out)    bool    :フラグ
// -------------------------------------------------
bool Mesh::Release()
{
    //!FBX解放
    FileLoader::FileLoader::Get()->DeleteFBX(&pFBX);

    //!テクスチャ解放
    SAFE_RELEASE(pTexture);
    TexState = false;

    //!バッファ解放
    SAFE_RELEASE(pOffsetBuf);
    SAFE_RELEASE(pNormalBuf);
    SAFE_RELEASE(pUVBuf);
    SAFE_RELEASE(pWeight);
    SAFE_RELEASE(pIndexWeight);
    SAFE_DELETE_ARRAY(pInvMat);
    SAFE_DELETE(pAnimController);

    return true;
}

// -------------------------------------------------
// static:プリミティブの指定
// -------------------------------------------------
// (in) Type    :プリミティブの種類(描画タイプ)
// -------------------------------------------------
void Mesh::SetPrimitiveType( const PrimitiveType_t &Type )
{
    PrimitiveType = Type;
}

// -------------------------------------------------
// static:アニメ再生フラグの指定
// -------------------------------------------------
// (in) Type    :プリミティブの種類(描画タイプ)
// -------------------------------------------------
void Mesh::SetAnimPlayFlag( const AnimPlayFlag_t &Flag )
{
    AnimPlayFlag = Flag;
}

// -------------------------------------------------
// static:アニメ再生のリセット
// -------------------------------------------------
// (out)    bool    :フラグ
// -------------------------------------------------
bool Mesh::AnimReset()
{
    //!エラーチェック
    if( !pFBX || !pAnimController ) return false;
    if( pFBX->GetData()->DataState != FBXData::ANIME )
    return false;

    //!リセット指定
    pAnimController->AnimReset();

    return true;
}

// -------------------------------------------------
// static:アニメテイク番号のセット
// -------------------------------------------------
// (in)     TakeNum :テイク番号
// (out)    bool    :フラグ
// -------------------------------------------------
bool Mesh::SetAnimTake( const u32 &TakeNum )
{
    //!エラーチェック
    if( !pFBX || !pAnimController )                     return false;
    if( pFBX->GetData()->DataState != FBXData::ANIME )  return false;
    if( TakeNum > pFBX->GetData()->TakeNum )            return false;

    //!テイク番号の設定
    pAnimController->SetAnimTake( TakeNum, 0.03f );

    return true;
}

// -------------------------------------------------
// static:保持しているテイク数取得
// -------------------------------------------------
// (out)    u32 :テイク数
// -------------------------------------------------
u32 Mesh::GetMaxTake()
{
    if( !pFBX || !pAnimController )
    {
        return 0;
    }
    return pFBX->GetData()->TakeNum;
}

// -------------------------------------------------
// static:アニメテイク番号のセット
// -------------------------------------------------
// (in)      Flag    :フラグ
// -------------------------------------------------
void Mesh::SetAnimFlag( const bool &Flag )
{
    AnimeFlag = Flag;
}

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
Mesh::Mesh()
{
    //!頂点要素の定義
    D3DVERTEXELEMENT9 mdecl[]=
    {
        { 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
        { 1, 0, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
        { 2, 0, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0 },
        { 3, 0, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },
        D3DDECL_END()
    };
    for( u32 i=0 ; i<5 ; i++ )
    {
        decl[i] = mdecl[i];
    }
    Dx::Get()->GetD3DDevice()->CreateVertexDeclaration( decl, &pVertexDecl );
    
    //!シェーダー登録
    bool h = false;
#ifdef SH_OBJ
    h = Shader::Get().LoadFromObject( _T("Shader/HLSLObj/fx_MeshFix.cfx"), &FIXShader );
    h = Shader::Get().LoadFromObject( _T("Shader/HLSLObj/fx_MeshAnim.cfx"), &AnimShader );
#else
    size_t Size = sizeof(g_fx_MeshFix);
    h = Shader::Get().LoadFromHeader( g_fx_MeshFix, Size, &FIXShader );
    Size = sizeof(g_fx_MeshAnim);
    h = Shader::Get().LoadFromHeader( g_fx_MeshAnim, Size, &AnimShader );
#endif
    if(!h)
    {
        Dx::Get()->SystemError(false);
    }

    //!メモリ確保
    pMat = new MATRIX;
    D3DXMatrixIdentity(pMat);
    Diffuse.x = 256.0f; Diffuse.y = 256.0f; Diffuse.z = 256.0f, Diffuse.w = 256.0f;
}


// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
Mesh::~Mesh()
{
    SAFE_RELEASE(pVertexDecl);
    SAFE_DELETE(pMat);
}


// -------------------------------------------------
// 更新
// -------------------------------------------------
//  (out)   bool    :フラグ
// -------------------------------------------------
bool Mesh::Update()
{
    //!エラーチェック
    if( !pFBX ) return false;
    //!更新
    switch( pFBX->GetData()->DataState )
    {
        //====固定====================
        case FBXData::FIX:

        break;

        //====アニメ==================
        case FBXData::ANIME:
            switch(AnimeFlag)
            {
                //====アニメ描画======
                case true:
                    if( AnimPlayFlag == PLAY )
                    {
                        pAnimController->AnimPlay();
                    }
                    else if( AnimPlayFlag == STOP )
                    {
                        pAnimController->AnimStop();
                    }
                    pAnimController->AnimUpdate( pFBX->GetData(), pInvMat );    //アニメーション更新
                break;
                
                //====固定描画========
                case false:

                break;
            }
        break;
        //====データなし==============
        case FBXData::NONE:
        break;
    }

    return true;
}

// -------------------------------------------------
// 描画
// -------------------------------------------------
// (in)     pProjMat    :プロジェクション行列
// (in)     pViwMat     :ビュー行列
// (out)    bool        :フラグ
// -------------------------------------------------
bool Mesh::Draw( const MATRIX *pProjMat, const MATRIX *pViwMat )
{
    //!エラーチェック
    if( !pFBX ) return false;

    switch( pFBX->GetData()->DataState )
    {
        //====固定====================
        case FBXData::FIX:
            if( TexState == true )
            {
                DrawFixTex( pProjMat, pViwMat );
            }
            else if(TexState == false)
            {
                DrawFix( pProjMat, pViwMat );
            }
        break;

        //====アニメ==================
        case FBXData::ANIME:
            switch(AnimeFlag)
            {
                //====アニメ描画======
                case true:
                    if( TexState == true )
                    {
                        DrawAnimTex( pProjMat, pViwMat );
                    }
                    else if(TexState == false)
                    {
                        DrawAnim( pProjMat, pViwMat );
                    }
                break;
                //====固定描画========
                case false:
                    if( TexState == true )
                    {
                        DrawFixTex( pProjMat, pViwMat );
                    }
                    else if(TexState == false)
                    {
                        DrawFix( pProjMat, pViwMat );
                    }
                break;
            }
        break;

        //====データなし==============
        case FBXData::NONE:
        break;
    }
    
    pFBX->GetData()->DrawInfo( 550, 20, 700, 20 );

    return true;
}

// -------------------------------------------------
// private:描画-固定テクスチャあり
// -------------------------------------------------
// (in) pProjMat    :プロジェクション行列
// (in) pViewMat    :ビュー行列
// -------------------------------------------------
void Mesh::DrawFixTex( const MATRIX *pProjMat, const MATRIX *pViewMat )
{
    UINT numPass;

    //!ストリームのセット
    Dx::Get()->GetD3DDevice()->SetVertexDeclaration(pVertexDecl);
    Dx::Get()->GetD3DDevice()->SetStreamSource(0, pOffsetBuf, 0, sizeof(VEC3));
    Dx::Get()->GetD3DDevice()->SetStreamSource(1, pUVBuf, 0, sizeof(VEC2));
    Dx::Get()->GetD3DDevice()->SetStreamSource(2, NULL, 0, 0 );
    Dx::Get()->GetD3DDevice()->SetStreamSource(3, NULL, 0, 0);

    //!テクニック
    FIXShader.GetPtr()->SetTechnique("TShader");
    FIXShader.GetPtr()->Begin( &numPass, 0 );
    FIXShader.GetPtr()->BeginPass(0);
    FIXShader.GetPtr()->SetMatrix( "World", pMat );
    FIXShader.GetPtr()->SetMatrix( "View", pViewMat );
    FIXShader.GetPtr()->SetMatrix( "Proj", pProjMat );
    FIXShader.GetPtr()->SetVector( "Diffuse", &Diffuse );
    FIXShader.GetPtr()->SetTexture( "Tex", pTexture );
    FIXShader.GetPtr()->CommitChanges();
    switch(PrimitiveType)
    {
        case POLYGON:
            Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_TRIANGLELIST, 0, pFBX->GetData()->PolygonCount );
        break;
        case POINT:
            Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_POINTLIST, 0, pFBX->GetData()->VertexCount );
        break;
    }

    FIXShader.GetPtr()->EndPass();
    FIXShader.GetPtr()->End();
}

// -------------------------------------------------
// private:描画-固定テクスチャなし
// -------------------------------------------------
// (in)    pProjMat    :プロジェクション行列
// (in)    pViewMat    :ビュー行列
// -------------------------------------------------
void Mesh::DrawFix( const MATRIX *pProjMat, const MATRIX *pViewMat )
{
    UINT numPass;

    //!ストリームのセット
    Dx::Get()->GetD3DDevice()->SetVertexDeclaration(pVertexDecl);
    Dx::Get()->GetD3DDevice()->SetStreamSource( 0, pOffsetBuf, 0, sizeof(VEC3) );
    Dx::Get()->GetD3DDevice()->SetStreamSource( 1, pUVBuf, 0, sizeof(VEC2) );
    Dx::Get()->GetD3DDevice()->SetStreamSource( 2, NULL, 0, 0 );
    Dx::Get()->GetD3DDevice()->SetStreamSource( 3, NULL, 0, 0 );

    //!テクニック
    FIXShader.GetPtr()->SetTechnique("TShader");
    FIXShader.GetPtr()->Begin( &numPass, 0 );
    FIXShader.GetPtr()->BeginPass(1);
    FIXShader.GetPtr()->SetMatrix( "World", pMat );
    FIXShader.GetPtr()->SetMatrix( "View", pViewMat );
    FIXShader.GetPtr()->SetMatrix( "Proj", pProjMat );
    FIXShader.GetPtr()->SetVector( "Diffuse", &Diffuse );
    FIXShader.GetPtr()->CommitChanges();
    switch(PrimitiveType)
    {
        case POLYGON:
            Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_TRIANGLELIST, 0, pFBX->GetData()->PolygonCount );
        break;
        case POINT:
            Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_POINTLIST, 0, pFBX->GetData()->VertexCount );
        break;
    }
    FIXShader.GetPtr()->EndPass();
    FIXShader.GetPtr()->End();
}

// -------------------------------------------------
// private:描画-アニメ・テクスチャ
// -------------------------------------------------
//  (in)    pProjMat    :プロジェクション行列
//  (in)    pViewMat    :ビュー行列
// -------------------------------------------------
void Mesh::DrawAnimTex( const MATRIX *pProjMat, const MATRIX *pViewMat )
{
    UINT numPass;

    //!ストリームのセット
    Dx::Get()->GetD3DDevice()->SetVertexDeclaration(pVertexDecl);
    Dx::Get()->GetD3DDevice()->SetStreamSource(0, pOffsetBuf, 0, sizeof(VEC3));
    Dx::Get()->GetD3DDevice()->SetStreamSource(1, pUVBuf, 0, sizeof(VEC2));
    Dx::Get()->GetD3DDevice()->SetStreamSource(2, pWeight, 0, sizeof(VEC4));
    Dx::Get()->GetD3DDevice()->SetStreamSource(3, pIndexWeight, 0, sizeof(Buf::IndexWeightDef_t));

    //!テクニック
    AnimShader.GetPtr()->SetTechnique("TShader");
    AnimShader.GetPtr()->Begin( &numPass, 0 );
    AnimShader.GetPtr()->BeginPass(0);
    AnimShader.GetPtr()->SetMatrix( "Mat", pMat );
    AnimShader.GetPtr()->SetMatrix( "View", pViewMat );
    AnimShader.GetPtr()->SetMatrix( "Proj", pProjMat );
    AnimShader.GetPtr()->SetVector( "Diffuse", &Diffuse );
    AnimShader.GetPtr()->SetTexture( "Tex", pTexture );
    AnimShader.GetPtr()->SetMatrixArray( "World", pAnimController->GetAnimMat(), pFBX->GetData()->BoneCount );
    AnimShader.GetPtr()->CommitChanges();
    switch(PrimitiveType)
    {
        case POLYGON:
            Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_TRIANGLELIST, 0, pFBX->GetData()->PolygonCount );
        break;
        case POINT:
            Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_POINTLIST, 0, pFBX->GetData()->VertexCount );
        break;
    }
    AnimShader.GetPtr()->EndPass();
    AnimShader.GetPtr()->End();
}

// -------------------------------------------------
// private:描画-アニメ・テクスチャなし
// -------------------------------------------------
// (in)    pProjMat    :プロジェクション行列
// (in)    pViewMat    :ビュー行列
// -------------------------------------------------
void Mesh::DrawAnim( const MATRIX *pProjMat, const MATRIX *pViewMat )
{
    UINT numPass;

    //!ストリームのセット
    Dx::Get()->GetD3DDevice()->SetVertexDeclaration(pVertexDecl);
    Dx::Get()->GetD3DDevice()->SetStreamSource( 0, pOffsetBuf, 0, sizeof(VEC3) );
    Dx::Get()->GetD3DDevice()->SetStreamSource( 1, pUVBuf, 0, sizeof(VEC2) );
    Dx::Get()->GetD3DDevice()->SetStreamSource( 2, pWeight, 0, sizeof(VEC4) );
    Dx::Get()->GetD3DDevice()->SetStreamSource( 3, pIndexWeight, 0, sizeof(Buf::IndexWeightDef_t) );

    //!テクニック
    AnimShader.GetPtr()->SetTechnique("TShader");
    AnimShader.GetPtr()->Begin( &numPass, 0 );
    AnimShader.GetPtr()->BeginPass(1);
    AnimShader.GetPtr()->SetMatrix( "Mat", pMat );
    AnimShader.GetPtr()->SetMatrix( "View", pViewMat );
    AnimShader.GetPtr()->SetMatrix( "Proj", pProjMat );
    AnimShader.GetPtr()->SetVector( "Diffuse", &Diffuse );
    AnimShader.GetPtr()->SetMatrixArray( "World", pAnimController->GetAnimMat(), pFBX->GetData()->BoneCount );
    AnimShader.GetPtr()->CommitChanges();
    switch(PrimitiveType)
    {
    case POLYGON:
            Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_TRIANGLELIST, 0, pFBX->GetData()->PolygonCount );
        break;
        case POINT:
            Dx::Get()->GetD3DDevice()->DrawPrimitive( D3DPT_POINTLIST, 0, pFBX->GetData()->VertexCount );
        break;
    }
    AnimShader.GetPtr()->EndPass();
    AnimShader.GetPtr()->End();
}

}