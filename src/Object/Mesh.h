//=======================================================
// ビューア表示用メッシュ
//=======================================================
#ifndef Include_Mesh_H
#define Include_Mesh_H

//====<インクルード>=================================
//====<前方宣言>=====================================
namespace FileLoader{ class FileLoader; }
namespace FileLoader{ class ShaderLoader; }
namespace FileLoader{ namespace FBXLoader{ class FBX; }}
namespace FileLoader{ namespace FBXLoader{ class FBXData; }}
namespace Draw{ namespace Mesh{ class BufferFormat; }}
namespace Draw{ namespace Mesh{ class AnimController; }}

namespace Object{
// -------------------------------------------------
// クラス:モデルデータ
// -------------------------------------------------
class Mesh{
//=====定義==========================
private:
    enum{ BUF_SIZE = 512, };

public:
    //描画プリミティブタイプ
    typedef enum PrimitiveType_t{
        POLYGON = 0,
        POINT,
    }PrimitiveType_t;

    //アニメ再生フラグ
    typedef enum AnimPlayFlag_t{
        PLAY = 0,
        STOP
    }AnimPlayFlag_t;

//=====名前空間======================
private:
    typedef FileLoader::ShaderLoader            Shader;
    typedef FileLoader::FBXLoader::FBX          FBX;
    typedef FileLoader::FBXLoader::FBXData      FBXData;
    typedef Draw::Mesh::BufferFormat            Buf;
    typedef Draw::Mesh::AnimController          AnimController;

//=====構築子：消去子=================
private:
    //!コピー禁止
    void operator = ( const Mesh &src ){}
    Mesh( const Mesh &src ){}
public:
    //!コンストラクタ・デストラクタ
    Mesh();
    ~Mesh();

//=====変数<static>==================
private:
    //!ファイル管理
    static bool	                                TexState;               //テクスチャの状態(読み込めているか)

    //!リソース
    static FileLoader::FBXLoader::FBX           *pFBX;                  //FBXデータ

    //!バッファ
    static LPDIRECT3DTEXTURE9                   pTexture;               //テクスチャ
    static D3DVERTEXELEMENT9                    decl[];                 //バッファ要素格納
    static LPDIRECT3DVERTEXBUFFER9              pOffsetBuf;             //オフセットバッファ
    static LPDIRECT3DVERTEXBUFFER9              pNormalBuf;             //法線バッファ
    static LPDIRECT3DVERTEXBUFFER9              pUVBuf;                 //UV情報
    static LPDIRECT3DVERTEXBUFFER9              pWeight;                //ウェイトバッファ
    static LPDIRECT3DVERTEXBUFFER9              pIndexWeight;           //ウェイトインデックス
    static MATRIX                               *pInvMat;               //アニメーション:逆行列
    static AnimController                       *pAnimController;       //アニメーションコントローラー

    //!描画フラグ
    static PrimitiveType_t                      PrimitiveType;          //プリミティブの種類
    static AnimPlayFlag_t                       AnimPlayFlag;           //アニメ再生フラグ
    static u32                                  TakeNum;                //アニメテイク番号
    static bool                                 AnimeFlag;              //アニメフラグ

//=====関数<static>==================
public:
    static bool LoadResource( LPCTSTR pFBXFullPath );                   //リソース読み込み
    static bool CreateBuf();                                            //バッファ作成
    static bool Release();                                              //リソースバッファの解放
    static void SetPrimitiveType( const PrimitiveType_t &Type );        //プリミティブの指定
    static void SetAnimPlayFlag( const AnimPlayFlag_t &Flag );          //アニメ再生のフラグ指定
    static bool AnimReset();                                            //アニメ再生のリセット
    static bool SetAnimTake( const u32 &TakeNum );                      //アニメテイク番号のセット
    static u32 GetMaxTake();                                            //保持しているテイク数
    static void SetAnimFlag( const bool &Flag );                        //アニメフラグの設定

//=====変数==========================
private:
    //!メッシュコントロール
    LPDIRECT3DVERTEXDECLARATION9        pVertexDecl;                    //頂点宣言オブジェクト
    System::Com_ptr<ID3DXEffect>        FIXShader;                      //シェーダー：固定
    System::Com_ptr<ID3DXEffect>        AnimShader;                     //シェーダー：アニメ
    MATRIX                              *pMat;                          //変換行列
    VEC4                                Diffuse;                        //描画色

//=====関数===========================
private:
    //!描画個別機能
    void DrawFix    ( const MATRIX *pProjMat, const MATRIX *pViewMat ); //固定
    void DrawFixTex ( const MATRIX *pProjMat, const MATRIX *pViewMat ); //固定・テクスチャ
    void DrawAnim   ( const MATRIX *pProjMat, const MATRIX *pViewMat ); //アニメ
    void DrawAnimTex( const MATRIX *pProjMat, const MATRIX *pViewMat ); //アニメ・テクスチャ

public:
    bool Update();                                                      //更新
    bool Draw( const MATRIX *pProjMat, const MATRIX *pViewMat );        //描画
};

}
#endif