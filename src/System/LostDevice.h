//=======================================================
// デバイスロスト時の処理
//=======================================================
#ifndef Include_LostDevice
#define Include_LostDevice

//====<インクルード>=================================
#include <list>
#include "LostResource.h"

namespace System{

// ------------------------------------------
// クラス:ロストからの復帰
// ------------------------------------------
// ・シングルトンクラス
// ------------------------------------------
class LostDevice{

//=====構築子：消去子=================
public:
    //!クラスへのアクセス手段
    static LostDevice *Get()
    {
        static LostDevice instance;
        return &instance;
    }

private:
    //!コンストラクタ<使用不可>
    LostDevice(){}
    LostDevice( const LostDevice &src ){}
    LostDevice &operator=( const LostDevice &src ){}

//=====変数==========================
private:
    std::list<Smart_ptr<LostResourceBase> > Resourcelist;           //COMリソース登録用

//=====関数===========================
public:
    //ロストデバイス
    void RegisterResourse( Smart_ptr<LostResourceBase> spResource );//COMリソースをロスト管理に登録する
    bool RecoverLostDevice();                                       //デバイスロストからの復帰処理
    void CleanupResourse();                                         //リストの使用しない領域を削除(定期的に呼び出すこと)

    //リソース個別処理
    bool ReleaseD3DObject();                                        //リソースの解放
    bool RecoverD3DObject();                                        //リソースの復元
};

}
#endif
