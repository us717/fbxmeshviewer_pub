//+*******************************************************
//| デバイスロスト時におけるCOMインターフェイスの個別処理
//+*******************************************************

//====<インクルード>==========================
#include "Lib.h"
#include "LostResource.h"

namespace System{

//------------------------------------------
// HLSLロスト時の処理
//------------------------------------------
    //!登録
    void LostHLSL_File::RegisterResource( Com_ptr<ID3DXEffect> Effect )
    {
        //if( cpHLSL.GetPtr() )
        cpHLSL = Effect;
    }

    //!解放
    void LostHLSL_File::Backup()
    {
        cpHLSL->OnLostDevice();
    }
    
    //!復旧
    void LostHLSL_File::Recover()
    {
        cpHLSL->OnResetDevice();
    }

    //!参照カウンタ取得
    size_t LostHLSL_File::GetCount()
    {
        return cpHLSL.GetRefCnt();
    }
    
    //!メンバのインターフェイスCOMポインタ取得
    Com_ptr<ID3DXEffect>& LostHLSL_File::GetMemInterface()
    {
        return cpHLSL;
    }
}