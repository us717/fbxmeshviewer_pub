//+****************************************************************
//| FPS制御
//+****************************************************************

#include "lib.h"
#include "ControlFPS.h"
namespace System{

// ------------------------------------------
// コンストラクタ
// ------------------------------------------
ControlFPS::ControlFPS():
    FrameCount(0),FrameRate(0),FrameTime(0),InitFrameTime(0)
    ,InitFlag(false),ScheduleTime(0),SkipFlag(false)
{
    //!FPS表示初回文字化け防止
    DWORD Init = 0;
    wsprintf( buf, TEXT("%d"), Init );
    
    //!最少タイマ分解精度指定
    timeBeginPeriod(1);
}

// ------------------------------------------
// デストラクタ
// ------------------------------------------
ControlFPS::~ControlFPS()
{
    timeEndPeriod(1);
}

// ------------------------------------------
// フレーム処理メイン
// ------------------------------------------
void ControlFPS::ControlMain()
{
    if(!InitFlag)
    {
        InitFlag = true;
        InitFrameTime = timeGetTime();
    }

    //!次回のフレーム予定時刻
    ScheduleTime =	(FrameCount + 1) * (DWORD)FrameTime + InitFrameTime;

    FrameCount++;
    //!フレーム制御
    if( timeGetTime() > ScheduleTime )          //!予定時刻オーバーならフレームスキップ
    {
        SkipFlag = true;
    }
    else
    {
        while( timeGetTime() <= ScheduleTime )  //!予定時刻より早く処理が終わったいたら待機する
        {
            Sleep(1);
        }
        SkipFlag = false;
    }

    //!FPS取得時の処理(1秒経過時)
    if( timeGetTime() - InitFrameTime >= 1000 )
    {
        FrameRate	= FrameCount;
        FrameCount	= 0;
#ifdef _DEBUG
        wsprintf( buf, TEXT("%d"),FrameRate );
#endif
        InitFrameTime	= timeGetTime();
    }
}

//------------------------------------------
// FPSの設定
// ------------------------------------------
// (in)    Fps     :指定するFPSの値
// ------------------------------------------
void ControlFPS::SetFPS( DWORD Fps )
{
    FrameTime = 1000.0f / Fps;
}

// ------------------------------------------
// FPSの表示
// ------------------------------------------
// (in)    HWND    :ウィンドハンドル
// ------------------------------------------
void ControlFPS::ShowFPS(HWND hWnd)
{
    SetWindowText( hWnd, buf );
}


// ------------------------------------------
// フレームスキップの確認
// ------------------------------------------
//  (out)   bool    :true-フレームをスキップする
// ------------------------------------------
bool ControlFPS::CheckFrameSkip()
{
    return SkipFlag;
}

}