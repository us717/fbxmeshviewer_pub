//  製作者      : IKD様
//  HomePage    : http://marupeke296.com/
//  公開されているソースを改変使用

//=======================================================
// Com_ptr:COMテンプレート
//      →COM使用時はこれを使用すること!
//-------------------------------------------------------
// ・COMインターフェイスのコピー問題解決
// ・自動解放処理、スワップ対応
//=======================================================
#ifndef Include_Com_ptr_H
#define Include_Com_ptr_H

namespace System{

// -------------------------------------------------
// クラス：COMテンプレート
// -------------------------------------------------
template <class T>
class Com_ptr{

//====<メンバ変数>=================================
private:
    T           **m_ppInterface;    //インターフェイスへのダブルポインタ
    ULONG       *m_pRef;            // COMポインタの参照カウンタ
    static T    *m_NullPtr;         // NULLポインタ

//====<関数:カウンタ処理>===========================
protected:
    //!インターフェイスの参照カウンタ増化
    void AddRef(T *pInterface)
    {
        pInterface->AddRef();
    }

    //!インターフェイスのカウンタ減少
    void Release( T *pInterface )
    {	
        pInterface->Release();
    }

    //!内部参照カウンタ減少
    void ReleaseComRef()
    {
        if( --(*m_pRef) == 0 )
        {
            delete m_ppInterface;
            delete m_pRef;
        }
    }

//====<コンストラクタ>==============================
public:
    //!デフォルトコンストラクタ
    explicit Com_ptr( T *pInterface = 0, bool add = false )
    {
        if( pInterface && add )
            pInterface->AddRef();
        m_ppInterface = new T*;     //ポインタ領域新規確保
        m_pRef = new ULONG(1);      //内部カウンタ新規確保
        
        if(pInterface)
            *m_ppInterface = pInterface;
        else
            *m_ppInterface = m_NullPtr;
    }

    //!暗黙型キャストコピーコンストラクタ
    template<class T2> Com_ptr( Com_ptr<T2>& src )
    {
        m_pRef = src.GetMyRefPtr();             // COMポインタ参照カウンタ共有
        *m_pRef += 1;                           // COMポインタ参照カウンタをインクリメント

        m_ppInterface = (T**)src.GetPtrPtr();   // 共有
        *m_ppInterface = src.GetPtr();          // 型チェック用

        // 参照カウンタ増加
        if(*m_ppInterface)
            AddRef( *m_ppInterface );
    }


    //!同型コピーコンストラクタ
    Com_ptr( const Com_ptr<T> &src)
    {
        m_pRef = src.GetMyRefPtr();         // COMポインタ参照カウンタ共有
        *m_pRef += 1;                       // COMポインタ参照カウンタをインクリメント

        m_ppInterface = src.GetPtrPtr();    // 共有

        //参照カウンタ増加
        if(*m_ppInterface)
            AddRef( *m_ppInterface );
    }

    //!コピーコンストラクタ(NULL代入用)
    Com_ptr(const s32 nullval)
    {
        m_ppInterface = new T*; //ポインタ格納領域を新規確保
        *m_ppInterface = m_NullPtr;
        m_pRef = new ULONG(1);
    }

//====<デストラクタ>============
    virtual ~Com_ptr()
    {
        if(*m_ppInterface)
            Release(*m_ppInterface);
        ReleaseComRef();
    }

//====<演算子>==================

    //!同型代入演算子（明示的コピー）
    Com_ptr<T>& operator =(const Com_ptr<T>& src)
    {
        //COMポインタ参照グループである場合は何もしない
        if( m_pRef == src.GetMyRefPtr() )
            return *this;

        //自分のインターフェイスの参照カウンタを1つ減らす
        if(*m_ppInterface)
            Release(*m_ppInterface);

        //他人になるので参照カウンタをデクリメント
        ReleaseComRef();

        //相手のポインタをコピー
        m_ppInterface = src.m_ppInterface;
        m_pRef = src.m_pRef;

        // カウンタをインクリメントして共有
        if(*m_ppInterface)
            AddRef( *m_ppInterface );
        *m_pRef += 1;

        return *this;
    }

    //!暗黙型変換代入演算子（明示的コピー）
    template<class T2> Com_ptr<T>& operator =(Com_ptr<T2>& src)
    {
        //同じCOMポインタ参照グループである場合は何もしない
        if( m_pRef == src.GetMyRefPtr() )
            return *this;

        //自分のインターフェイスの参照カウンタを1つ減らす
        if(*m_ppInterface)
            Release(*m_ppInterface);

        //他人になるので参照カウンタをデクリメント
        ReleaseComRef();

        //相手のポインタをコピー
        m_ppInterface = (T**)src.GetPtrPtr();
        *m_ppInterface = src.GetPtr();		//チェック用代入
        m_pRef = src.GetMyRefPtr();

        //カウンタをインクリメントして共有
        if(*m_ppInterface)
            AddRef( *m_ppInterface );
        *m_pRef += 1;

        return *this;
    }

    //!NULL代入用演算子（明示的コピー）
    Com_ptr<T>& operator =(const s32 nullval)
    {
        //自分のインターフェイスの参照カウンタを1つ減らす
        if(*m_ppInterface)
            Release(*m_ppInterface);

        //自身の参照カウンタをデクリメント
        ReleaseComRef();

        //ポインタを初期化
        m_ppInterface = new T*;
        *m_ppInterface = m_NullPtr;
        m_pRef = new ULONG(1);

        return *this;
    }

    //!インターフェイス代入演算子（新規インターフェイス登録）
    template<class T2> void operator =(T2* pInterface)
    {
        //明示的にインターフェイスを新規登録する場合に用いる

        //自分のンターフェイスの参照カウンタを1つ減らす
        if(*m_ppInterface)
            Release(*m_ppInterface);

        //他人になるので参照カウンタをデクリメント
        ReleaseComRef();

        //自分は新しい人になるので、
        //新しいダブルポインタを作成
        m_ppInterface = new T*;
        m_pRef = new ULONG(1);

        //新規代入
        if(pInterface)
            *m_ppInterface = pInterface;
        else
            *m_ppInterface = m_NullPtr;
    }

    //!比較演算子(!=)
    bool operator !=(T* ptr)
    {
        if(ptr != *m_ppInterface)
            return true;
        return false;
    }

    //!比較演算子(!=)
    bool operator !=(Com_ptr<T> &src)
    {
        if(*m_ppInterface != *src.m_ppInterface)
            return true;
        return false;
    }

    //!比較演算子(!=)
    bool operator !=(const s32 nullval)
    {
        if(*m_ppInterface != NULL)
            return true;
        return false;
    }

    //!比較演算子(==)
    bool operator ==(T* ptr)
    {
        if(ptr == *m_ppInterface)
            return true;
        return false;
    }

    //!比較演算子(==)
    bool operator ==(Com_ptr<T> &src)
    {
        if(*m_ppInterface == *src.m_ppInterface)
            return true;
        return false;
    }

    //!単項演算子(!)
    bool operator !()
    {
        if( *m_ppInterface == NULL )
            return true;
        return false;
    }

    //=メンバ選択演算子(->)
    T* operator ->(){ return *m_ppInterface; }


//====<関数>====================
    //!COMポインタの参照カウンタポインタを取得
    ULONG *GetMyRefPtr() const { return m_pRef;}

    //!インターフェイスの参照カウンタ数を取得
    ULONG GetRefCnt()
    {
        if(*m_ppInterface){
            ULONG c = (*m_ppInterface)->AddRef()-1;
            (*m_ppInterface)->Release();
            return c;
        }
        return 0;
    }

    //!アップキャストコピー
    template<class T1> void UpcastCopy(Com_ptr<T1> & src ){
        //自分のインターフェイスの参照カウンタを1つ減らす
        if(*m_ppInterface)
            Release(*m_ppInterface);

        //自分は他人になってしまうので自身の参照カウンタをデクリメント
        ReleaseComRef();

        //相手をコピー
        m_ppInterface = src.GetPtrPtr();    //共有
        *m_ppInterface = src.GetPtr();      //チェック用代入
        m_pRef = src.GetMyRefPtr();         //COMポインタ参照カウンタ共有

        //参照カウンタをインクリメント
        if(*m_ppInterface)
            AddRef( *m_ppInterface );
        *m_pRef += 1;
    }

    //!ポインタ取得
    T* GetPtr(){return *m_ppInterface;}

    //!ダブルポインタを取得
    T** GetPtrPtr() const {return m_ppInterface;}

    //!インターフェイス生成関数へ渡す専用関数
    T** ToCreator(){
        //自分のインターフェイスの参照カウンタを1つ減らす
        if(*m_ppInterface)
            Release(*m_ppInterface);

        //自分は他人になってしまうので自身の参照カウンタをデクリメント
        ReleaseComRef();

        //ポインタを初期化
        m_ppInterface = new T*;
        *m_ppInterface = m_NullPtr; //一応代入
        m_pRef = new ULONG(1);

        return m_ppInterface;
    }

    //!COMスワップ
    bool Swap( Com_ptr<T> &src )
    {
        //引数のCOMポインタが保持するインターフェイスと自身のとを入れ替える

        //双方のCOMポインタ参照カウンタ数のチェック
        ULONG SrcComRef = *src.GetMyRefPtr();
        ULONG MyComRef = *m_pRef;

        //双方のCOMポインタ参照カウンタ数の差を算出
        bool SrcDecriment = false;  //引数の参照カウンタを減少させる場合trueになる
        ULONG DefComRef = MyComRef - SrcComRef;
        if( SrcComRef > MyComRef )
        {
            //引数の参照数の方が多いのでSrcDecrimentをtrueに
            DefComRef = SrcComRef - MyComRef;
            SrcDecriment = true;
        }

        //参照カウンタの増加側と減少側を確定
        T *pReleaseObj, *pAddObj;
        if(SrcDecriment){
            pReleaseObj = src.GetPtr(); //引数のを減少
            pAddObj = *m_ppInterface;
        }
        else{
            pReleaseObj = *m_ppInterface;
            pAddObj = src.GetPtr();     //引数のを増加
        }

        //互いの参照カウント数を交換
        ULONG i;
        if(pReleaseObj && pAddObj)      //双方が有効なインターフェイス
        {
            for(i=0; i<DefComRef; i++){
                pReleaseObj->Release();
                pAddObj->AddRef();
            }
        }
        else if(pReleaseObj && (pAddObj==NULL)) //減少側だけが有効
        {
            for(i=0; i<DefComRef; i++)
                pReleaseObj->Release();
        }
        else if((pReleaseObj==NULL) && pAddObj) // 増加側だけが有効
        {
            for(i=0; i<DefComRef; i++)
                pAddObj->AddRef();
        }

        //COMポインタ内のインターフェイスポインタを交換
        T* pTmp = *src.m_ppInterface;
        *src.m_ppInterface = *m_ppInterface;
        *m_ppInterface = pTmp;

        return true;
    }
};

template <class T>
T* Com_ptr<T>::m_NullPtr = NULL;

}
#endif
