//=======================================================
// デバイスロスト時におけるCOMインターフェイスの個別処理
//=======================================================
#ifndef Include_LostResource_H
#define Include_LostResource_H

//====<インクルード>==========================
#include "Com_ptr.h"

namespace System{

//------------------------------------------
// 基底クラス:リソース復旧用
//------------------------------------------
// 各機能別クラスに継承して使用
//------------------------------------------
class LostResourceBase{

//=====構築子：消去子=========
public:
    virtual ~LostResourceBase(){};

//=====関数==================
public:
    //!継承して定義
    virtual void Backup()       = 0; //バックアップ(解放処理)
    virtual void Recover()      = 0; //復旧
    virtual size_t GetCount()   = 0; //参照カウンタ取得
};

//------------------------------------------
// クラス：HLSLロスト時の処理
//------------------------------------------
class LostHLSL_File: public LostResourceBase{

//=====変数==================
private:
    Com_ptr<ID3DXEffect> cpHLSL;                            //インターフェイスCOMポインタ

//=====関数==================
public:
    LostHLSL_File(){};
    void RegisterResource( Com_ptr<ID3DXEffect> Effect );   //登録
    void Backup();                                          //バックアップ
    void Recover();                                         //復旧
    size_t GetCount();                                      //参照カウンタ取得
    Com_ptr<ID3DXEffect> &GetMemInterface();                //メンバのインターフェイスCOMポインタ取得
};

}

#endif
