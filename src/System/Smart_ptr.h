// 製作者   : IKD様
// HomePage : http://marupeke296.com/
// 公開されているソースを改変使用

//=======================================================
// Smart_ptr:スマートポインタテンプレート
//-------------------------------------------------------
// ・自動解放処理、スワップ対応
//=======================================================
#ifndef Include_Smart_ptr_H
#define Include_Smart_ptr_H

namespace System{
// -------------------------------------------------
// クラス：スマートポインタテンプレート
// -------------------------------------------------
template <class T>
class Smart_ptr{
//====<メンバ変数>=================================
private:
    u32 *m_pRefCnt;             //参照カウンタへのポインタ
    T** m_ppPtr;                //T型のオブジェクトのダブルポインタ
    static T* m_NullPtr;        //NULLポインタ値

//====<関数:カウンタ処理>===========================
private:
    //!参照カウンタ増加
    void AddRef(){(*m_pRefCnt)++;}

    //!参照カウンタ減少
    void Release()
    {
        if(--(*m_pRefCnt) == 0)
        {
            delete *m_ppPtr;
            delete m_ppPtr;
            delete m_pRefCnt;
        }
    }

//====<コンストラクタ>==============================
public:
    //!デフォルトコンストラクタ
    explicit Smart_ptr(T* src=NULL, s32 add=0)
    {
        m_pRefCnt = new u32;
        *m_pRefCnt = add;
        m_ppPtr = new T*;
        m_NullPtr = NULL;
        if(src)
            *m_ppPtr = src;
        else
            *m_ppPtr = m_NullPtr;       //NULLポインタを入れる
        AddRef();                       //参照カウンタ増加
    }

    //!コピーコンストラクタ（同型コピー）
    Smart_ptr(const Smart_ptr<T> &src)
    {
        //相手のポインタをすべてコピー
        m_pRefCnt = src.m_pRefCnt;      //参照カウンタポインタ
        m_ppPtr = src.m_ppPtr;          //T型ダブルポインタ
        AddRef();                       //参照カウンタを増加
    }

    //!コピーコンストラクタ（暗黙的アップキャスト）
    template<class T2> Smart_ptr(Smart_ptr<T2> &src)
    {
        //相手のダブルポインタをコピー
        m_pRefCnt = src.GetRefPtr();
        m_ppPtr = (T**)src.GetPtrPtr();
        *m_ppPtr = src.GetPtr();        //型チェックコピー
        AddRef();                       //カウンタを増加
    }

    //!コピーコンストラクタ（NULL代入代用）
    Smart_ptr(const s32 nullval)
    {
        m_pRefCnt = new UINT;
        *m_pRefCnt = 0;
        m_ppPtr = new T*;
        *m_ppPtr = m_NullPtr;
        AddRef();                       //参照カウンタ増加
    }

//====<デストラクタ>==============================
    virtual ~Smart_ptr()
    {
        Release();
    }

//====<演算子のオーバーロード>=====================

    //代入演算子（=明示的コピー）
    Smart_ptr<T>& operator =(const Smart_ptr<T> &src)
    {
        //自分自身への代入は不正でなので行わない。
        if(*src.m_ppPtr == *m_ppPtr)
            return (*this);
        Release();                      //参照カウンタを1つ減少

        //相手のポインタをコピー
        m_pRefCnt = src.m_pRefCnt;
        m_ppPtr = src.m_ppPtr;
        AddRef();                       //参照カウンタを増加

        return (*this);
    }

    //!代入演算子（=明示的アップキャスト）
    template<class T2> Smart_ptr<T>& operator =(Smart_ptr<T2> &src)
    {
        //自分自身への代入は不正でなので行わない。
        if(src.GetPtr() == *m_ppPtr)
        return (*this);
        Release();                      //参照カウンタを1つ減少

        //相手のポインタをコピー
        m_pRefCnt = src.GetRefPtr();
        m_ppPtr = (T**)src.GetPtrPtr();

        //型チェックコピー
        *m_ppPtr = src.GetPtr();

        //新しい自分自身の参照カウンタを増加
        AddRef();
        return (*this);
    }

    //!代入演算子(=NULL代入によるリセット）
    Smart_ptr<T>& operator =(const s32 nullval)
    {
        Release();//参照カウンタを1つ減少

        //新規に自分自身を作る
        m_pRefCnt = new u32(1);
        m_ppPtr = new T*;
        m_*ppPtr = m_NullPtr;
        return (*this);
    }

    //!間接演算子(*)
    T& operator *(){ return **m_ppPtr;}

    //メンバ選択演算子(->)
    T* operator ->(){ return *m_ppPtr;}
    
    //比較演算子(==)
    bool operator ==(T *val)
    {
        if(*m_ppPtr == val)
            return true;
        return false;
    }

    //比較演算子(!=)
    bool operator !=(T *val)
    {
        if(*m_ppPtr != val)
            return true;
        return false;
    }
//====<メンバ関数>============================
public:
    //!ポインタの明示的な登録
    void SetPtr(T* src = NULL, s32 add=0)
    {
        //参照カウンタを減らした後に再初期化
        Release();
        m_pRefCnt = new u32;
        *m_pRefCnt = add;
        m_ppPtr = new T*;
            if(src)
                *m_ppPtr = src;
            else
                *m_ppPtr = m_NullPtr;
            AddRef();
    }

    //!ポインタの貸し出し
    T* GetPtr(){return *m_ppPtr;}
    T** GetPtrPtr(){ return m_ppPtr;}

    //!参照カウンタへのポインタを取得
    u32* GetRefPtr(){return m_pRefCnt;}

//====<ダウンキャストコピー>==================
    template <class T2> bool DownCast(Smart_ptr<T2> &src)
    {
        //引数のスマートポインタの持つポインタが、
        //自分の登録しているポインタに
        //ダウンキャスト可能な場合はダウンキャストコピーを実行
        T* castPtr = dynamic_cast<T*>(src.GetPtr());

        if(castPtr)//ダウンキャスト成功
        {
            Release();                      //既存の参照カウンタを1つ減少
            m_ppPtr = (T**)src.GetPtrPtr(); //新しいポインタと参照カウンタを共有
            *m_ppPtr = castPtr;
            m_pRefCnt = src.GetRefPtr();
            
            AddRef();                       //参照カウンタ増加
            return true;
        }
        return false;
    }

//====<ポインタスワップ（交換）>=================
    void SwapPtr( Smart_ptr<T> &src )
    {
        T* pTmp = src.GetPtr();
        *src.m_ppPtr = *m_ppPtr;            // ポインタの交換
        *m_ppPtr = pTmp;
    }
};

template <class T>
T* Smart_ptr<T>::m_NullPtr = NULL;

}
#endif
