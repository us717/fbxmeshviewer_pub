//=======================================================
// FPS制御
//=======================================================
#ifndef Include_FPS2_H
#define Include_FPS2_H

namespace System{

// ------------------------------------------
// クラス:FPS制御
// ------------------------------------------
// ・シングルトンクラス
// ・固定フレーム採用
// ------------------------------------------
class ControlFPS{
//=====構築子：消去子=================
public:
    //!クラスへのアクセス手段
    static ControlFPS *Get()
    {
        static ControlFPS instance;
        return &instance;
    }

private:
    //!コンストラクタ<使用不可>
    ControlFPS();
    ControlFPS( const ControlFPS &src ){}
    ControlFPS &operator=( const ControlFPS &src ){}
    ~ControlFPS();

//=====変数===========================
private:
    //!フレーム制御
    f32     FrameTime;              //FPSの設定
    bool    InitFlag;               //フラグ:初回の処理
    DWORD   FrameCount;             //フレームカウンタ
    DWORD   InitFrameTime;          //フレームを開始した時刻
    DWORD   ScheduleTime;           //フレーム毎の予定時刻
    bool    SkipFlag;               //フラグ:フレームスキップ

    //!FPS表示
    DWORD   FrameRate;              //FPS
    TCHAR   buf[8];                 //文字用

//=====関数===========================
public:
    void ControlMain();             //フレーム処理メイン
    void SetFPS( DWORD Fps );       //FPSセット
    void ShowFPS( HWND hWnd );      //FPSの表示
    bool CheckFrameSkip();          //フレームスキップの確認
};

}
#endif