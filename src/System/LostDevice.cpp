//+*******************************************************
//| デバイスロスト時の処理
//+*******************************************************

//====<インクルード>==========================
#include "Lib.h"
#include "LostDevice.h"
#include "LostResource.h"
#include <list>

namespace System{

// ------------------------------------------
// COMリソースをロスト管理に登録
// ------------------------------------------
void LostDevice::RegisterResourse(Smart_ptr<LostResourceBase> spResource)
{
    Resourcelist.push_back(spResource);
}

// ------------------------------------------
// デバイスロスト復帰処理
// ------------------------------------------
// (out)    bool    :フラグ
// ------------------------------------------
bool LostDevice::RecoverLostDevice()
{
    //!デバイス状態の確認
    HRESULT hr = Dx::Get()->GetD3DDevice()->TestCooperativeLevel();
    if(FAILED(hr))
    {
        if( hr == D3DERR_DEVICELOST )           //まだ失われている状態
            return  true;
        if( hr != D3DERR_DEVICENOTRESET )       //予期せぬ状態(→終了)
        {
            MessageBox( Dx::Get()->GetMainHwnd(), _T("デバイス復元に失敗しました"),_T("システムエラー") , MB_OK );
            PostQuitMessage(0);
        }

    //!リソースの解放
        //Sprite用ロスト時の処理
        Dx::Get()->GetSprite()->OnLostDevice();

        //他登録されてるリソースの解放
        std::list<Smart_ptr<LostResourceBase> >::iterator it;
        for( it=Resourcelist.begin() ; it!=Resourcelist.end(); it++ )
        {
            (*it)->Backup(); //リソースの解放
        }

    //!デバイスの復元
        hr = Dx::Get()->GetD3DDevice()->Reset( &Dx::Get()->GetD3DParam() );
        if(FAILED(hr)) //失敗(→終了)
        {
            if( hr == D3DERR_DEVICELOST)
                return true;
            MessageBox( Dx::Get()->GetMainHwnd(), _T("デバイス復元に失敗しました"),_T("システムエラー") , MB_OK );
            PostQuitMessage(0);
        }
    //!解放されたリソースの復元
        Dx::Get()->GetSprite()->OnResetDevice();    //Sprite用ロスト時からの復帰

        for( it=Resourcelist.begin() ; it!=Resourcelist.end(); it++ )
        {
            (*it)->Recover(); //リソースの解放
        }
    
    //!レンダーステートの再設定
        //必要なら記述
    }

    return false;
}

//----------------------------------------------
// リストの使用しない領域を削除
//----------------------------------------------
// リソースは登録すると自動で削除できないので、
// 定期的に削除してやること
//----------------------------------------------
void LostDevice::CleanupResourse()
{
    std::list<Smart_ptr<LostResourceBase> >::iterator it;
    for( it=Resourcelist.begin() ; it != Resourcelist.end() ; )
    {
        //参照カウンタが1つの場合、削除(登録リストにしかデータがないため)
        if( (*it)->GetCount() == 1)
        {
            it = Resourcelist.erase(it);
            continue;
        }
        it++;
    }
}

//----------------------------------------------
// リソースの解放
//----------------------------------------------
// (out)    bool    :フラグ
//----------------------------------------------
bool LostDevice::ReleaseD3DObject()
{
    //Sprite用ロスト時の処理
    Dx::Get()->GetSprite()->OnLostDevice();

    //他登録されてるリソースの解放
    std::list<Smart_ptr<LostResourceBase> >::iterator it;
    for( it=Resourcelist.begin() ; it!=Resourcelist.end(); it++ )
    {
        (*it)->Backup(); //リソースの解放
    }

    return true;
}

//----------------------------------------------
// リソースの復元
//----------------------------------------------
// (out)    bool    :フラグ
//----------------------------------------------
bool LostDevice::RecoverD3DObject()
{
    Dx::Get()->GetSprite()->OnResetDevice(); //Sprite用ロスト時からの復帰
    std::list<Smart_ptr<LostResourceBase> >::iterator it;
    for( it=Resourcelist.begin() ; it!=Resourcelist.end(); it++ )
    {
        (*it)->Recover(); //リソースの解放
    }

    return true;
}

}