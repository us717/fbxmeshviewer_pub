//+******************************************************************************
//|DirectInput:入力の管理
//+******************************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <fstream>
#include "DirectInput.h"

namespace Input{

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
DirectInput::DirectInput():
    pDInput(0), pKeyDevice(0), pMouseDevice(0), InputFlag(0){}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
DirectInput::~DirectInput()
{
    if(pKeyDevice)
    pKeyDevice->Unacquire();	//入力終了

    SAFE_RELEASE(pKeyDevice);
    SAFE_RELEASE(pMouseDevice);
    SAFE_RELEASE(pDInput);
}

//-----------------------------------------
// private: <キー>デバイス作成
//-----------------------------------------
//	(out)	bool	:フラグ
//-----------------------------------------
bool DirectInput::CreateKeyDevice()
{
    //!デバイスの取得
    if( FAILED( pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, NULL) ) )
    {
    #ifdef _DEBUG	
        MessageBox( Dx::Get()->GetMainHwnd(), _T("キー:デバイス作成失敗"),_T("Input") , MB_OK );	
    #endif	
        return false;
    }
    
    //!デバイスのフォーマットの設定
    if( FAILED( pKeyDevice->SetDataFormat(&c_dfDIKeyboard) ) )
    {
    #ifdef _DEBUG	
        MessageBox( Dx::Get()->GetMainHwnd(), _T("キー:フォーマット設定失敗"),_T("Input") , MB_OK );	
    #endif
        SAFE_RELEASE(pKeyDevice);
        return false;
    }

    //!キーボード動作の設定(協調モード)
    pKeyDevice->SetCooperativeLevel( hWnd, DISCL_FOREGROUND|DISCL_NONEXCLUSIVE);	//バックグラウンド時デバイス解放・非排他モード

    //!入力の開始
    pKeyDevice->Acquire();

    return true;
}

//-----------------------------------------
// private: <マウス>デバイス作成
//-----------------------------------------
//	(out)	bool	:フラグ
//-----------------------------------------
bool DirectInput::CreateMouseDevice()
{
    //!デバイスの取得
    if( FAILED( pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, NULL) ) )
    {
    #ifdef _DEBUG	
        MessageBox( Dx::Get()->GetMainHwnd(), _T("マウス:デバイス作成失敗"),_T("Input") , MB_OK );	
    #endif	
        return false;
    }
    
    //!デバイスのフォーマットの設定
    if( FAILED( pMouseDevice->SetDataFormat( &c_dfDIMouse ) ) )
    {
    #ifdef _DEBUG	
        MessageBox( Dx::Get()->GetMainHwnd(), _T("マウス:フォーマット設定失敗"),_T("Input") , MB_OK );	
    #endif
        SAFE_RELEASE(pMouseDevice);
        return false;
    }

    //!マウス動作の設定(協調モード)
    pMouseDevice->SetCooperativeLevel( hWnd, DISCL_FOREGROUND|DISCL_NONEXCLUSIVE ); //バックグラウンド時デバイス解放・非排他モード

    //!入力の開始
    pKeyDevice->Acquire();

    return true;
}

//-----------------------------------------------------
// 初期設定
//-----------------------------------------------------
// ・インターフェイスとデバイスの作成を行う
// ----------------------------------------------------
// (in) HWND        :ウィンドハンドル
// (in) HINSTANCE   :インスタンスハンドル
// (in) HRESULT     :フラグ
// (in) InputMode_t :使用デバイスの選択
//-----------------------------------------------------
HRESULT DirectInput::SetDirectInput( HWND tmpWnd, HINSTANCE tmpInst, InputMode_t tmpMode )
{
#ifdef _DEBUG
    assert( pDInput == 0 && _T("すでに初期化されています") );
#endif

    hWnd        = tmpWnd;
    hInst       = tmpInst;
    this->Mode  = tmpMode;

    //!インターフェイスの取得
    if( FAILED( DirectInput8Create( hInst, DIRECTINPUT_VERSION, IID_IDirectInput8,
                                    (void**)&pDInput, NULL) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("ンターフェイス作成失敗"),_T("Input") , MB_OK );
    #endif
        return E_FAIL;
    }

    //!デバイスの取得
    switch(Mode)
    {
        //====<キー>==============
        case ModeKey:
            if( !CreateKeyDevice() )
                return E_FAIL;
        break;
        //====<マウス>============
        case ModeMouse:
            if( !CreateMouseDevice() )
                return E_FAIL;

        break;
        //====<両方>==============
        case ModeKeyMouse:
            if( !CreateKeyDevice() )
                return E_FAIL;
            if( !CreateMouseDevice() )
                return E_FAIL;
        break;
    }

    return S_OK;
}

//-----------------------------------------
// キーボードの状態を取得(直接データ)
//-----------------------------------------
void DirectInput::GetKeyState()
{
#ifdef _DEBUG
    assert( pKeyDevice != 0 && _T("キーのデバイスが取得されていません") );
#endif

    if( FAILED (pKeyDevice->GetDeviceState(256, KeyState) ) )
    {
        //!入力の開始
        pKeyDevice->Acquire();
        return;
    }
}

//-----------------------------------------
// マウスの状態を取得(直接データ)
//-----------------------------------------
void DirectInput::GetMouseState()
{
#ifdef _DEBUG
    assert( pMouseDevice != 0 && _T("マウスのデバイスが取得されていません") );
#endif
    if( FAILED ( pMouseDevice->GetDeviceState( sizeof(DIMOUSESTATE), &MouseState.MainInfo ) ) )
    {
        //!入力の開始
        pMouseDevice->Acquire();
        return;
    }
    
    //座標系は相対座標なので別途絶対座標を取得する
    GetCursorPos( &MouseState.MousePos );
    ScreenToClient( hWnd, &MouseState.MousePos );
}

//-----------------------------------------
// キー情報取得
//-----------------------------------------
// (out) BYTE*  :キー状態格納変数
//-----------------------------------------
const BYTE* DirectInput::Key() const
{
    return KeyState;
}

//-----------------------------------------
// マウス情報取得
//-----------------------------------------
// (out) BYTE*  :マウス状態格納変数
//-----------------------------------------
const DirectInput::MouseState_t* DirectInput::Mouse() const
{
    return &MouseState;
}

//-----------------------------------------
// 中断された入力を再開させる
//-----------------------------------------
// 非アクティブ時入力が中断されるので、入力を
// 再度受け付ける
//-----------------------------------------
// (out)    bool    :フラグ
//-----------------------------------------
bool DirectInput::ReAcquire()
{
    if(pKeyDevice)
        pKeyDevice->Acquire();
    if(pMouseDevice)
        pMouseDevice->Acquire();
    return true;
}

//--------------------------------------------------
// システム：固定入力確認
//--------------------------------------------------
// 一度ボタンが離れるまで入力は受付停止される
//--------------------------------------------------
// (out) bool   :入力されていたらtrue
//--------------------------------------------------

//====<上>==========================
bool DirectInput::Check_FixUP()
{
    if( ((Key()[SystemKey.UP]&0x80) != 0) && ((InputFlag &= 0x1) != 0) )
    {
        InputFlag &= ~0x1; //false
        return true;
    }
    return false;
}

//====<下>==========================
bool DirectInput::Check_FixDOWN()
{
    if( ( (Key()[SystemKey.DOWN]&0x80) != 0) && ((InputFlag &= 0x1) != 0) )
    {
        InputFlag &= ~0x1; //false
        return true;
    }
    return false;
}

//====<右>==========================
bool DirectInput::Check_FixRIGHT()
{
    if( ((Key()[SystemKey.RIGHT]&0x80) != 0) && ((InputFlag &= 0x2) != 0) )
    {
        InputFlag &= ~0x2; //false
        return true;
    }
    return false;
}

//====<左>=========================
bool DirectInput::Check_FixLEFT()
{
    if( ((Key()[SystemKey.LEFT]&0x80) != 0) && ((InputFlag &= 0x2) != 0) )
    {
        InputFlag &= ~0x2; //false
        return true;
    }
    return false;
}
//====<決定>=======================
bool DirectInput::Check_FixDECIDE()
{
    if( ((Key()[SystemKey.DECIDE]&0x80) != 0) && ((InputFlag &= 0x4) != 0) )
    {
        InputFlag &= ~0x4; //false
        return true;
    }
    return false;
}

//====<左シフト>====================
bool DirectInput::Check_FixLSHIFT()
{
    if( ((Key()[SystemKey.LSHIFT]&0x80) != 0) && ((InputFlag &= 0x8) != 0) )
    {
        InputFlag &= ~0x8; //false
        return true;
    }
    return false;
}


//====受付再開するか判断する=======
bool DirectInput::ResetFixFlag()
{
    //!上下
    if( ((Key()[SystemKey.DOWN]&0x80) == 0) && (Key()[SystemKey.UP]&0x80) == 0 )
        InputFlag |= 0x1; //true
    //!左右
    if( ((Key()[SystemKey.LEFT]&0x80) == 0) && (Key()[SystemKey.RIGHT]&0x80) == 0 )
        InputFlag |= 0x2; //true
    //決定キー
    if( (Key()[SystemKey.DECIDE]&0x80) == 0 )
        InputFlag |= 0x4; //true
    //左シフト
    if( (Key()[SystemKey.LSHIFT]&0x80) == 0 )
        InputFlag |= 0x8; //true
    return true;
}

}