//=======================================================
//DirectInput:入力の管理
//=======================================================
#ifndef Include_DirectInput_H
#define Include_DirectInput_H

//====<インクルード>=================================
namespace Input{

//---------------------------------------------------
// クラス：DirectInput
// --------------------------------------------------
// ・固定入力確認(キー)
// ・直接入力確認(マウス・キー)
// --------------------------------------------------
// ・シングルトンクラス
//--------------------------------------------------
class DirectInput{
//=====定義===========================
public:
//Struct:キー定義
    typedef struct KeyDef_t{
        u32 UP;
        u32 DOWN;
        u32 RIGHT;
        u32 LEFT;
        u32 DECIDE;
        u32 LSHIFT;
        KeyDef_t():UP(DIK_UP),DOWN(DIK_DOWN),RIGHT(DIK_RIGHT),
                    LEFT(DIK_LEFT),DECIDE(DIK_Z),LSHIFT(DIK_LSHIFT){}
    }KeyDef_t;

//Struct:入力フラグフォーマット
    typedef struct InputFlag_t{
        bool FlagUP;
        bool FlagDOWN;
        bool FlagRIGHT;
        bool FlagLEFT;
        bool FlagDECIDE;
        bool FlagLSHIFT;
        InputFlag_t():FlagUP(false),FlagDOWN(false),FlagRIGHT(false),
                        FlagLEFT(false),FlagDECIDE(false),FlagLSHIFT(false){}
    }InputFlag_t;

//struct:マウス情報
    typedef struct MouseState_t{
        DIMOUSESTATE    MainInfo;   //メイン情報
        POINT           MousePos;   //マウスの絶対座標
    }MouseState_t;

//enum:使用デバイス選択
    typedef enum InputMode_t{
        ModeKey = 0,                //キー
        ModeMouse,                  //マウス
        ModeKeyMouse,               //両方
    }InputMode_t;

//=====構築子：消去子==================
public:
    //!クラスへのアクセス手段
    static DirectInput *Get()
    {
        static DirectInput instance;
        return &instance;
    }

private:
    //!コンストラクタ<使用不可>
    DirectInput();
    DirectInput( const DirectInput &src ){}
    DirectInput &operator=( const DirectInput &src ){}
    ~DirectInput();

//=====変数===========================
private:
    //!インターフェイス
    LPDIRECTINPUT8          pDInput;                            //インターフェイス
    HWND                    hWnd;                               //ウィンドハンドル
    HINSTANCE               hInst;                              //インスタンスハンドル
    u8                      *pConfigData;                       //コンフィグデータ

    //!キー
    LPDIRECTINPUTDEVICE8    pKeyDevice;                         //キーデバイス
    BYTE                    KeyState[256];                      //キー状態格納変数
    KeyDef_t                SystemKey;                          //システムキー設定

    //!マウス
    LPDIRECTINPUTDEVICE8    pMouseDevice;                       //マウスデバイス
    MouseState_t            MouseState;                         //マウス状態格納変数

    //!コントロール
    InputMode_t             Mode;                               //使用デバイス
public:
    u32                     InputFlag;                          //キー入力受付状態

//=====関数===========================
private:
    bool CreateKeyDevice();                                     //キー   :デバイス作成
    bool CreateMouseDevice();                                   //マウス :デバイス作成
    bool LoadKeySet();                                          //キー設定の読み込み

public:
    //!入力:直接入力
    HRESULT SetDirectInput( HWND tmpWnd, HINSTANCE tmpInst,     //初期化処理
                            InputMode_t tmpMode );
    void    GetKeyState();                                      //キーボードの状態取得(直接データ)
    void    GetMouseState();                                    //マウスの状態取得(直接データ)
    bool    ReAcquire();                                        //中断された入力を再開させる
    const   BYTE* Key()             const;                      //キー情報取得
    const   MouseState_t* Mouse()   const;                      //マウス情報取得

    //!入力:固定入力
    bool    Check_FixUP();                                      //上→受付停止
    bool    Check_FixDOWN();                                    //下→受付停止
    bool    Check_FixRIGHT();                                   //右→受付停止
    bool    Check_FixLEFT();                                    //左→受付停止
    bool    Check_FixDECIDE();                                  //決定→受付停止
    bool    Check_FixLSHIFT();                                  //シフト→受付停止
    bool    ResetFixFlag();                                     //受付停止を解除するか判断
};

}
#endif
