//=======================================================
// ビューカメラ
//-------------------------------------------------------
// 注視点の回りを自由に動くカメラ
//=======================================================
#ifndef Include_ViewCamera_H
#define Include_ViewCamera_H

namespace Camera{

// ------------------------------------------
// クラス:ビューカメラ
// ------------------------------------------
class ViewCamera{

//=====構築子：消去子==================
public:
    ViewCamera();
    ~ViewCamera();

//=====変数===========================
private:
    VEC3        At;                 //注視点
    VEC3        MoveVec;            //移動方向ベクトル
    VEC3        CameraPos;          //注視点空間でのカメラ位置
    f32         AtDistance;         //カメラから注視点までの距離
    f32         DepthOffset;        //注視点空間での奥行き(Z)
    f32         ZAngle;             //Z軸回転角度(ラジアン)
    f32         MoveAngle;          //移動時の回転角度
    MATRIX      CameraPoseMat;      //カメラ姿勢行列
    MATRIX      ViewMat;            //ビュー行列
    bool        UpdateFlag;         //フラグ:前回から変更があったか

//=====関数===========================
public:
    //!カメラ移動
    void SetLookAt( const VEC3 &At );                                       //注視点の指定
    void SetRotateZ( const f32 &Angle );                                    //Z軸回転指定
    void MoveCamera( const f32 &X, const f32 &Y, const f32 &Depth );        //カメラ位置指定(差分)
    void MoveLookAt( const f32 &X, const f32 &Z, const f32 &Speed=0.1f );   //注視点移動
    void Update();                                                          //カメラ更新
    
    //!アクセサ
    MATRIX* GetViewMat( MATRIX *pViewMat );                                 //ビュー行列の取得
    MATRIX* GetCameraPoseMat( MATRIX *pViewMat );
};

}

#endif