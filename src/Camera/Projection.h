//=======================================================
// 射影変換:プロジェクション
//-------------------------------------------------------
// 射影変換行列の管理
//=======================================================
#ifndef Include_Projection_H
#define Include_Projection_H

namespace Camera{

// ------------------------------------------
// クラス:プロジェクション
//-------------------------------------------
// シングルトンクラス
// ------------------------------------------
class Projection{
//=====構築子：消去子==================
public:
    //!クラスへのアクセス手段
    static Projection *Get()
    {
        static Projection instance;
        return &instance;
    }
private:
    //!コンストラクタ<使用不可>
    Projection(){ D3DXMatrixIdentity( &ProjMat ); };
    Projection( const Projection &src ){}
    Projection &operator=( const Projection &src ){}
    ~Projection(){};

//=====変数==========================
private:
    MATRIX ProjMat;                                                       //射影変換行列

//=====関数==========================
public:
    void CreateProjMat( const f32 &WindowWidth, const f32 &WindowHeight );//射影変換行列の作成
    const MATRIX* GetProj();                                              //射影変換行列の取得
};

}

#endif