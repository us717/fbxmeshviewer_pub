//+****************************************************************
//| ビューカメラ
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <math.h>
#include "ViewCamera.h"

namespace Camera{

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
ViewCamera::ViewCamera():
    AtDistance(10.0f),
    DepthOffset(0.0f),
    ZAngle(0.0f),
    MoveAngle(0.06f),
    UpdateFlag(true)
{
    CameraPos = VEC3(0.0f, 0.0f, -10.0f);
    At        = VEC3(0.0f, 0.0f, 0.0f);
    MoveVec   = VEC3(0.0f, 0.0f, 0.0f);
    D3DXMatrixIdentity(&CameraPoseMat);
    D3DXMatrixIdentity(&ViewMat);
}

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
ViewCamera::~ViewCamera(){}

// -------------------------------------------------
// 注視点の指定
// -------------------------------------------------
// (in) At    :注視点
// -------------------------------------------------
void ViewCamera::SetLookAt( const VEC3 &At )
{
    this->At = At;
    UpdateFlag = true;
}

// -------------------------------------------------
// 注視点の移動
// -------------------------------------------------
// (in) X       :X方向
// (in) Z       :Z方向
// (in) Speed   :移動速度(0.1f)
// -------------------------------------------------
void ViewCamera::MoveLookAt( const f32 &X, const f32 &Z, const f32 &Speed )
{
    if( X == 0.0f && Z == 0.0f )
        return;
    //移動方向
    VEC3 MoveVec;
    MoveVec.x = X;
    MoveVec.y = 0.0f;
    MoveVec.z = Z;

    //視線方向
    VEC3 ViewDir;
    ViewDir.x = CameraPoseMat._31;
    ViewDir.y = 0.0f;
    ViewDir.z = CameraPoseMat._33;

    f32 Rotate = std::atan2( ViewDir.x, ViewDir.z );    //XZ面における移動角度
    MATRIX R;
    D3DXMatrixRotationY( &R, Rotate );                  //移動方向に回転
    D3DXVec3TransformCoord( &MoveVec, &MoveVec, &R );   //ワールド行列へ変換
    At += ( MoveVec * Speed );                          //注視点移動
    UpdateFlag = true;
}

// -------------------------------------------------
// カメラ位置指定
// -------------------------------------------------
// ・前回からの相対的位置を指定すること
// -------------------------------------------------
// (in) X       :移動ベクトルx
// (in) Y       :移動ベクトルy
// (in) Depth   :注視点からの距離
// -------------------------------------------------
void ViewCamera::MoveCamera( const f32 &X, const f32 &Y, const f32 &Depth )
{
    if( X == 0.0f && Y == 0.0f && Depth == 0.0f )
        return;
    MoveVec.x += X;
    MoveVec.y += Y;
    DepthOffset += Depth;
    UpdateFlag = true;
}

// -------------------------------------------------
// Z軸回転指定
// -------------------------------------------------
// (in) Angle   :回転角度(度数)
// -------------------------------------------------
void ViewCamera::SetRotateZ( const f32 &Angle )
{
    if( Angle == 0.0f )
        return;
    ZAngle = Angle * ( D3DX_PI / 180.0f );
    UpdateFlag = true;
}

// -------------------------------------------------
// カメラ更新
// -------------------------------------------------
void ViewCamera::Update()
{
    if(!UpdateFlag)
        return;

    //!Z回転姿勢更新
    D3DXQUATERNION ZAxisQ;
    VEC3 Axis;
    memcpy( &Axis, CameraPoseMat.m[2], sizeof( VEC3 ) );
    D3DXQuaternionRotationAxis( &ZAxisQ, &Axis, ZAngle );
    MATRIX ZAxisRotMat;
    D3DXMatrixRotationQuaternion( &ZAxisRotMat, &ZAxisQ );
    CameraPoseMat *= ZAxisRotMat;

    //!XとYのベクトル更新
    VEC3 tmpVec;
    D3DXVec3TransformCoord( &tmpVec, &MoveVec, &CameraPoseMat );

    if( tmpVec.x != 0.0f || tmpVec.y != 0.0f || tmpVec.z != 0.0f )
    {
        //!回転軸を求める
        VEC3 RotAxis;
        VEC3 *pCamZAxis = (VEC3*)CameraPoseMat.m[2];    //Z軸の方向
        D3DXVec3Cross( &RotAxis, &tmpVec, pCamZAxis );  //XYベクトル更新結果とZ軸を面にした回転面

        //!回転行列を求める
        D3DXQUATERNION RotateQ;
        D3DXQuaternionRotationAxis( &RotateQ, &RotAxis, MoveAngle );
        MATRIX RotateMat;
        D3DXMatrixRotationQuaternion( &RotateMat, &RotateQ );
    
        //!注視点空間移動
        D3DXVec3TransformCoord( &CameraPos, &CameraPos, &RotateMat );

        //!姿勢更新
        VEC3 x, y, z;
        z = -CameraPos;
        D3DXVec3Normalize( &z, &z );
        memcpy( &y, CameraPoseMat.m[1], sizeof( VEC3 ) );
        D3DXVec3Cross( &x, &y, &z );    //X軸
        D3DXVec3Normalize( &x, &x );
        D3DXVec3Cross( &y, &z, &x );    //Y軸
        D3DXVec3Normalize( &y, &y );

        D3DXMatrixIdentity( &CameraPoseMat );
        memcpy( CameraPoseMat.m[0], &x, sizeof( VEC3 ) );
        memcpy( CameraPoseMat.m[1], &y, sizeof( VEC3 ) );
        memcpy( CameraPoseMat.m[2], &z, sizeof( VEC3 ) );
    }
    //!奥行きの更新
    AtDistance = D3DXVec3Length( &CameraPos );  //距離を計算
    if( AtDistance - DepthOffset > 0 )          //距離はゼロより大きいこと
    {
        VEC3 dep = -CameraPos;                  //見た目と一致させる(+で近くなり、-で遠くなる)
        D3DXVec3Normalize( &dep, &dep );
        CameraPos += DepthOffset * dep;
    }

    //!ビュー行列作成
    VEC3 WorldPos = CameraPos + At;
    memcpy( &ViewMat, &CameraPoseMat, sizeof( MATRIX ) );
    memcpy( &ViewMat.m[3], &WorldPos, sizeof( VEC3 ) );
    ViewMat._44 = 1.0f;
    D3DXMatrixInverse( &ViewMat, 0, &ViewMat );

    //!リセット
    MoveVec = VEC3( 0.0f, 0.0f, 0.0f );
    DepthOffset = 0.0f;
    ZAngle = 0.0f;
    UpdateFlag = false;
}

// -------------------------------------------------
// ビュー行列の取得
// -------------------------------------------------
// (i/o) pViewMat   :ビュー行列を返すマトリクス
// (out) MATRIX     :ビュー行列
// -------------------------------------------------
MATRIX* ViewCamera::GetViewMat( MATRIX *pViewMat )
{
    if(pViewMat)
        memcpy( pViewMat, &ViewMat, sizeof( MATRIX ) );
    return &ViewMat;
}
MATRIX* ViewCamera::GetCameraPoseMat( MATRIX *pViewMat )
{
    if(pViewMat)
        memcpy( pViewMat, &CameraPoseMat, sizeof( MATRIX ) );
    return &CameraPoseMat;
}

}
