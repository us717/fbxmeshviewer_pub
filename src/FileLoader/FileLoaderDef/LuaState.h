//=======================================================
// フォーマット：LuaState(Lua)
//-------------------------------------------------------
// ・ファイルローダーで管理
// ・ローダーに登録→読み込みファイル個別登録→ロード
// ・一度ロードを行うと、ファイルを追加登録できない仕様なので
//   事前に全て登録しておくこと
//=======================================================
#ifndef Include_LuaState_H
#define Include_LuaState_H

//====<インクルード>===================
#include <lua.hpp>

//====<前方宣言>=======================
namespace FileLoader{ class FileLoader; }

namespace FileLoader{
namespace FileLoaderDef{

// ------------------------------------------
// クラス:LuaState
// ------------------------------------------
// (in) s8*         :ファイル名 s8型
// (in) LPCTSTR*    :ファイル名 LPCTSTR型
// ------------------------------------------
class LuaState{
//=====定義==========================
private:
    typedef struct LuaFileName{
        s8*         sFileName;
        LPCTSTR     tFileName;
    }LuaFileName;

//=====構築子：消去子=================
private:
//!コンストラクタ
    LuaState():pState(0){}

//!コピー禁止
    void operator = ( const LuaState &src ){}
    LuaState( const LuaState &src ){}

//!デストラクタ
    virtual ~LuaState();

//!フレンド
    friend class FileLoader;

//=====変数==========================
private:
    lua_State               *pState;
    std::list<LuaFileName>  FileName;

//=====関数==========================
private:
    bool Redy() const{ return( !pState?false:true ); }          //ロード済みかの確認
public:
    void RegisterFile( s8 *p_sFileName , LPCTSTR p_tFileName ); //読み込みファイルの登録
    lua_State* Get() const{ return pState; }                    //インターフェイスへのアクセス
    void PrintStack() const;                                    //デバッグ用:スタックの中身を表示
};

}}

#endif