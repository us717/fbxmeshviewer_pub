//+****************************************************************
//| フォーマット：LuaState(Lua)
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <lua.hpp>
#include <fstream>
#include "LuaState.h"

namespace FileLoader{
namespace FileLoaderDef{

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
LuaState::~LuaState()
{
    if(pState)
    {
        lua_close(pState); pState = 0;
    }
}

// -------------------------------------------------
// ファイル登録
// -------------------------------------------------
// (in) p_sFileName     :ファイル名 s8型
// (in) p_tFileName     :ファイル名 LPCTSTR型
// -------------------------------------------------
void LuaState::RegisterFile( s8 *p_sFileName , LPCTSTR p_tFileName )
{
#ifdef _DEBUG
    assert( pState == 0 && _T("既に登録が完了しています。初期化してください") );
#endif
    LuaFileName tmp;
    tmp.sFileName = p_sFileName;
    tmp.tFileName = p_tFileName;
    FileName.push_back(tmp);
}

// -------------------------------------------------
// デバッグ用:スタックの中身を表示
// -------------------------------------------------
void LuaState::PrintStack() const
{
#ifdef _DEBUG
    assert( pState != 0 && _T("ステイトが取得されていません") );
#endif

    s32 StackNum = lua_gettop(pState);
    ExOutputDebugString( _T( "-----------------------------\n" ) );
    if( StackNum == 0 )
    {
        ExOutputDebugString( _T("NoStack\n") );
    }

    for( s32 i = StackNum; i >= 1; i-- )
    {
        ExOutputDebugString( _T("%d\n"), i );
        ExOutputDebugString( _T("(%d) :\n"), -StackNum + i - 1 );
        s32 type = lua_type( pState, i );
        switch(type)
        {
            //====空==============
            case LUA_TNIL:
                OutputDebugString( _T( "NIL\n" ) );
            break;

            //====BOOL============
            case LUA_TBOOLEAN:
                ExOutputDebugString( _T( "BOOLEAN\t" ) );
                ExOutputDebugString( _T( "%d\n" ), lua_toboolean(pState, i) );
            break;

            //====voidポインタ=====
            case LUA_TLIGHTUSERDATA:
                ExOutputDebugString( _T( "LIGHTUSERDATA\n" ) );
            break;

            //====数字(f64)========
            case LUA_TNUMBER:
                ExOutputDebugString( _T( "NUMBER\t" ) );
                ExOutputDebugString( _T( "%f\n"), lua_tonumber(pState, i) );
            break;

            //====文字列===========
            case LUA_TSTRING:
                ExOutputDebugString( _T( "STRING\t" ) );
                ExOutputDebugString( _T( "%s\n"), lua_tostring(pState, i) );
            break;

            //====テーブル=========
            case LUA_TTABLE:
                ExOutputDebugString( _T( "TABLE\n" ) );
            break;

            //====関数=============
            case LUA_TFUNCTION:
                ExOutputDebugString( _T( "FUNCTION\n" ) );
            break;

            //====ユーザデータ======
            case LUA_TUSERDATA:
                ExOutputDebugString( _T( "USERDATA\n" ) );
            break;

            //====コルーチン========
            case LUA_TTHREAD:
                ExOutputDebugString( _T( "THREAD\n" ) );
            break;
        }
    }
    ExOutputDebugString( _T( "-----------------------------\n" ) );
}

}}