//=======================================================
// フォーマット：テクスチャ
//-------------------------------------------------------
// ファイルローダーで管理している
//=======================================================
#ifndef Include_Tex_H
#define Include_Tex_H
//====<インクルード>===================
#include <string>

//====<前方宣言>=======================
namespace FileLoader{ class FileLoader; }

namespace FileLoader{
namespace FileLoaderDef{

// ------------------------------------------
// クラス:テクスチャ
// ------------------------------------------
// (in) s8*         :ファイル名 s8型
// (in) LPCTSTR*    :ファイル名 LPCTSTR型
// ------------------------------------------
class Tex{

//=====構築子：消去子=================
private:
//!コンストラクタ
    Tex( const s8 *sFileName , LPCTSTR tFileName )
        :pTexture(0){ this->tFileName = tFileName; this->sFileName = sFileName; }

//!コピー禁止
    void operator = ( const Tex &src ){}
    Tex( const Tex &src ){}

//!デストラクタ
    ~Tex(){ SAFE_RELEASE(pTexture); }

//!フレンド
    friend class FileLoader;

//=====変数==========================
private:
    LPDIRECT3DTEXTURE9  pTexture;
    LPCTSTR             tFileName;
    std::string         sFileName;

//=====関数==========================
private:
    bool Redy() const{ return(!pTexture?false:true); }  //ロード済みかの確認
public:
    LPDIRECT3DTEXTURE9 Get() const{ return pTexture; }  //インターフェイスへのアクセス
};

}}

#endif