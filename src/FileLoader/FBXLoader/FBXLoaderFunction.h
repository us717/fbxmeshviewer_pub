//=======================================================
// FBXLoaderで使用するサブメソッド
// ------------------------------------------------------
// <<注意!>>benderに限定
// ・Blender側の設定ｚが安定しないためマテリアル1つのみに対応
//=======================================================
#ifndef Include_FBXLoaderFunction_H
#define Include_FBXLoaderFunction_H

//====<インクルード>================================
#include <fbxsdk.h>
#include <list>
#include "FBXData.h"

namespace FileLoader{
namespace FBXLoader{

// -------------------------------------------------
// Struct: アニメーションデータ構造体
// -------------------------------------------------
// FbxMainとFunction間のデータ受け渡し時に使用
// -------------------------------------------------
typedef struct FrameData_t{
    s32             RecentTake; //現在処理しているテイク番号
    FbxTime         PeriodTime; //単位時間(1フレーム時間)
    FbxTime         StartTime;  //スタート時間
    s32             FrameCount; //フレーム数
}FrameData_t;	

// -------------------------------------------------
// クラス:FBXデータ読み取り
// -------------------------------------------------
class FBXLoaderFunction{

//======定義============================
private:
    //!Struct:ボーン情報一時保管
    typedef struct TmpBoneData_t{
        std::list<u8>               BoneID;            //ボーン配列
        std::list<f32>              BoneWeight_t;      //ウェイト
    }TmpBoneData_t;

    //!Struct:最適化用データ一時保管
    typedef struct TmpFbxData_t{
        s32                         PolygonCount;       //ポリゴン数
        s32                         VertexCount;        //頂点数
        FbxVector4                  *pVertexBuf;        //頂点情報
        s32                         IndexCount;         //インデックス数
        s32                         *pIndexBuf;         //インデックス配列
        s32                         NormalCount;        //法線数
        FbxGeometryElementNormal    *pNormalBuf;        //法線
        s32                         BoneCount;          //ボーン数
        TmpBoneData_t               *pBoneWeightBuf;    //ボーン情報
    }TmpFbxData_t;

//=====構築子：消去子=================
private:
    //!コピー禁止
    void operator = (const FBXLoaderFunction& Data){}
    FBXLoaderFunction(const FBXLoaderFunction& Data){}
public:
    //!コンストラクタ・デストラクタ
    FBXLoaderFunction();
    ~FBXLoaderFunction();

//!=====変数===========================
private:
    TmpFbxData_t TmpData;                                           //最適化が必要なデータの一時保管領域

//!=====関数===========================
private:
    void ConvertMatFbxToDx( MATRIX &DxMat, FbxMatrix &FbxMat );     //行列変換(FBX→Directx)
public:
    //!メッシュ情報(最適化必要)
    bool GetMeshBuf     ( FbxMesh *pMesh );                         //メッシュバッファ取得
    bool GetNormalBuf   ( FbxMesh *pMesh );                         //法線バッファ取得
    
    //!メッシュ情報(最適化必要なし)
    bool GetMeshUV( FbxMesh *pMesh, FBXData **ppElement );          //UVの取得
    bool GetTexture( FbxMesh *pMesh, FBXData **ppElement );         //テクスチャの取得

    //!アニメーション情報(最適化必要)
    bool GetBoneInfo    ( FbxMesh *pMesh, FBXData **ppElement );    //ボーン情報の読み取り
    bool GetAnimFrame   ( FbxMesh *pMesh, FBXData **ppElement,      //ボーンフレームアニメ読み込み
                            const FrameData_t *pFrameData );

    //!最適化
    bool OptimizeData   ( FBXData **ppElement);                     //データ最適化(固定時)
    bool OptimizeDataEX ( FBXData **ppElement);                     //データ最適化(アニメ使用時)
};

}}
#endif
