//+****************************************************************
//| メッシュ描画用マテリアル群
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include "DrawFont.h"
#include "FBXData.h"
namespace FileLoader{
namespace FBXLoader{

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
FBXData::FBXData()
    :DataState(NONE), ErrorInfo(ERROR_NONE), ErrorCount(0), VertexCount(0), PolygonCount(0), UVSetCount(0), pVertex(0), pNormal(0), pUVSet(0),
        BoneCount(0), TakeNum(0), pBoneWeight(0), pInitPose(0),pFramePose(0), pFont(0)
{
    for( s32 i=0 ; i<INFO_NUM ; ++i )
    {
        for( s32 j=0 ; j<BUF_SIZE ; ++j )
        {
            Buf[i][j] = 0;
        }
    }
}

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
// (in) UseDebugInfo    :メッシュ情報表示機能を使用するか
// -------------------------------------------------
FBXData::FBXData( bool UseDebugInfo )
    :DataState(NONE), ErrorInfo(ERROR_NONE), ErrorCount(0), VertexCount(0), PolygonCount(0), UVSetCount(0), pVertex(0), pNormal(0), pUVSet(0),
        BoneCount(0), TakeNum(0), pBoneWeight(0), pInitPose(0), pFramePose(0), pFont(0)
{
    if(UseDebugInfo) pFont = new DrawFont;
    for( s32 i=0 ; i<INFO_NUM ; ++i )
    {
        for( s32 j=0 ; j<BUF_SIZE ; ++j )
        {
            Buf[i][j] = 0;
        }
    }
}

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
FBXData::~FBXData()
{
    SAFE_DELETE_ARRAY(pVertex);
    SAFE_DELETE_ARRAY(pNormal);
    SAFE_DELETE_ARRAY(pUVSet);
    SAFE_DELETE_ARRAY(pBoneWeight);
    SAFE_DELETE_ARRAY(pInitPose);
    SAFE_DELETE_ARRAY(pFramePose);
    SAFE_DELETE(pFont);
}


// -------------------------------------------------
// private:メッシュ情報配列コピー
// -------------------------------------------------
// (i/o)    pOutArray   :格納する配列へのポインタ
// (in)     pIn         :配列に入れる文字列
// -------------------------------------------------
void FBXData::InfoCopyArray( TCHAR *pOutArray, LPCTSTR pIn )
{
    u32 Count = 0;
    for( Count=0 ; pIn[Count]!='\0' ; ++Count )
    {
        pOutArray[Count] = pIn[Count];
    }
    pOutArray[Count+1]='\0';
#ifdef _DEBUG
    assert( Count+1 <= BUF_SIZE );
#endif
}

// -------------------------------------------------
// メッシュ情報の作成(バッファ)
// -------------------------------------------------
// (out) bool    :フラグ
// -------------------------------------------------
bool FBXData::CreateInfo()
{
#ifdef _DEBUG
    assert( pFont != 0 );
#endif

    //!初期化
    for( s32 i=0 ; i<INFO_NUM ; ++i )
    {
        for( s32 j=0 ; j<BUF_SIZE ; ++j )
        {
            Buf[i][j] = 0;
        }
    }

    //作成
    switch(DataState)
    {
        case NONE:
            InfoCopyArray( Buf[DATA_STATE], _T("NONE") );
        break;
        case FIX:
            InfoCopyArray( Buf[DATA_STATE], _T("FIX") );
        break;
        case ANIME:
            InfoCopyArray( Buf[DATA_STATE], _T("ANIME") );
        break;
    }
    switch(ErrorInfo)
    {
        case ERROR_NONE:
            InfoCopyArray( Buf[ERROR_INFO], _T("NONE") );
        break;
        case ERROR_INIT:
            InfoCopyArray( Buf[ERROR_INFO], _T("INIT") );
        break;
        case ERROR_VERTEX:
            InfoCopyArray( Buf[ERROR_INFO], _T("VERTEX") );
        break;
        case ERROR_NORMAL:
            InfoCopyArray( Buf[ERROR_INFO], _T("NORMAL") );
        break;
        case ERROR_UV:
            InfoCopyArray( Buf[ERROR_INFO], _T("UV") );
        break;
        case ERROR_TEX:
            InfoCopyArray( Buf[ERROR_INFO], _T("TEX") );
        break;
        case ERROR_WEIGHT:
            InfoCopyArray( Buf[ERROR_INFO], _T("WEIGHT") );
        break;
        case ERROR_NOTAKE:
            InfoCopyArray( Buf[ERROR_INFO], _T("NOTAKE") );
        break;
    }
    _stprintf_s( Buf[ERROR_COUNT], _T("%d"), ErrorCount );
    _stprintf_s( Buf[VERTEX],      _T("%d"), VertexCount );
    _stprintf_s( Buf[POLYGON],     _T("%d"), PolygonCount );
    _stprintf_s( Buf[BONE],        _T("%d"), BoneCount );
    _stprintf_s( Buf[TAKE],        _T("%d"), TakeNum );

    return true;
}

// -------------------------------------------------
// メッシュ情報の表示
// -------------------------------------------------
// (in)     MenuX   :メニュー表示位置X
// (in)     MenuY   :メニュー表示位置Y
// (in)     DataX   :データ表示位置X(Yはメニューと同じ高さ)
// (in)     Space   :メニュー間の幅(Y幅)
// (out)    bool    :フラグ
// -------------------------------------------------
bool FBXData::DrawInfo( const f32 &MenuX, const f32 &MenuY, const f32 &DataX, const f32 &Space )
{
#ifdef _DEBUG
    assert( pFont != 0 );
#endif
    pFont->SetScale(0.4f,0.4f);
    pFont->SetFontSpace(11.0f);

    pFont->Draw( MenuX, MenuY+Space*0, _T("DATA_STATE") );
    pFont->Draw( MenuX, MenuY+Space*1, _T("ERROR_INFO") );
    pFont->Draw( MenuX, MenuY+Space*2, _T("ERROR_COUNT") );
    pFont->Draw( MenuX, MenuY+Space*3, _T("VERTEX") );
    pFont->Draw( MenuX, MenuY+Space*4, _T("POLYGON") );
    pFont->Draw( MenuX, MenuY+Space*5, _T("BONE") );
    pFont->Draw( MenuX, MenuY+Space*6, _T("TAKE") );

    pFont->Draw( DataX, MenuY+Space*0, Buf[DATA_STATE] );
    pFont->Draw( DataX, MenuY+Space*1, Buf[ERROR_INFO] );
    pFont->Draw( DataX, MenuY+Space*2, Buf[ERROR_COUNT] );
    pFont->Draw( DataX, MenuY+Space*3, Buf[VERTEX] );
    pFont->Draw( DataX, MenuY+Space*4, Buf[POLYGON] );
    pFont->Draw( DataX, MenuY+Space*5, Buf[BONE] );
    pFont->Draw( DataX, MenuY+Space*6, Buf[TAKE] );

    return true;
}

}}