//+****************************************************************
//| FBXLoaderで使用するサブメソッド
//|----------------------------------------------------------------
//| <<注意!>>アニメーション関係(テンプレート外)
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <fbxsdk.h>
#include <list>
#include "FBXData.h"
#include "FBXLoaderFunction.h"

namespace FileLoader{
namespace FBXLoader{

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
FBXLoaderFunction::FBXLoaderFunction()
{
    //一時保管領域の初期化
    TmpFbxData_t Init = {0};
    TmpData = Init;
}
// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
FBXLoaderFunction::~FBXLoaderFunction()
{
    TmpData.pVertexBuf  = 0;
    TmpData.pIndexBuf   = 0;
    TmpData.pNormalBuf  = 0;
    SAFE_DELETE_ARRAY(TmpData.pBoneWeightBuf);
}

// -------------------------------------------------
// メッシュバッファ取得
// -------------------------------------------------
// (in)     pMesh   :FBXメッシュクラス
//                  →NodeAttributeからダウンキャスト指定する
// (out)    bool    :フラグ
// -------------------------------------------------
bool FBXLoaderFunction::GetMeshBuf( FbxMesh *pMesh )
{
    TmpData.PolygonCount    = pMesh->GetPolygonCount();         //ポリゴン数
    TmpData.VertexCount     = pMesh->GetControlPointsCount();   //頂点数取得
    TmpData.pVertexBuf      = pMesh->GetControlPoints();        //頂点配列へのポインタ取得
    TmpData.IndexCount      = pMesh->GetPolygonVertexCount();   //インデックス数
    TmpData.pIndexBuf       = pMesh->GetPolygonVertices();      //インデックスへのポインタ取得
    return true;
}

//------------------------------------------------------------
// 法線バッファの取得
//------------------------------------------------------------
// <注意!> Blenderに限定
//------------------------------------------------------------
// (in)     pMesh   :FBXメッシュクラス
//                  →NodeAttributeからダウンキャスト指定する
// (out)    bool    :フラグ
//------------------------------------------------------------
bool FBXLoaderFunction::GetNormalBuf( FbxMesh *pMesh )
{
    //!Blenderに対応しているか確認
    if( pMesh->GetElementNormalCount() != 1 )
    {
        return false; //法線がない
    }

    FbxGeometryElement::EMappingMode mapping	 = pMesh->GetElementNormal()->GetMappingMode();     //マッピングモード取得
    FbxGeometryElement::EReferenceMode reference = pMesh->GetElementNormal()->GetReferenceMode();   //リファレンスモード取得

    if( !mapping == FbxGeometryElement::eByControlPoint ||
        !reference == FbxGeometryElement::eDirect )
    {
        return false; //法線取得未対応
    }
        
    //!対応しているならば一時バッファに保存
        TmpData.NormalCount = pMesh->GetElementNormal()->GetDirectArray().GetCount();               //法線数取得
        TmpData.pNormalBuf	= pMesh->GetElementNormal();                                            //法線バッファの保存
    return true;
}

//------------------------------------------------------------
// UVの取得
//------------------------------------------------------------
// 最適化の必要がないため直接マテリアルデータに渡している
//------------------------------------------------------------
// <注意!>   ・現状UV座標は1セットのみに対応している
//          ・Blenderのみに対応
//          ・複数セットにすぐ対応できるような設計に一応している
//------------------------------------------------------------
// (in)     pMesh           :FBXメッシュクラス
//          →NodeAttributeからダウンキャスト指定する
// (i/o)    ppElement       :メッシュ用マテリアル
// (out)    bool            :フラグ
//------------------------------------------------------------	
bool FBXLoaderFunction::GetMeshUV( FbxMesh *pMesh, FBXData **ppElement )
{
    (*ppElement)->UVSetCount = pMesh->GetElementUVCount(); //UVセット取得
    if( (*ppElement)->UVSetCount != 0 )
    {
        (*ppElement)->pUVSet = new UVSet[(*ppElement)->UVSetCount]; //セットの数だけメモリ確保
    }

    if( (*ppElement)->UVSetCount > 1) //複数のUVセットには現在対応していない
        return false;

    //==========<セットごとの取得処理============
    for( u32 i=0 ; i<(*ppElement)->UVSetCount ; ++i )
    {
        FbxGeometryElementUV *pUVElement = pMesh->GetElementUV(i);                      //UVバッファの取得
        FbxLayerElement::EMappingMode mapping = pUVElement->GetMappingMode();           //マッピングモード取得
        FbxLayerElement::EReferenceMode reference = pUVElement->GetReferenceMode();     //リファレンスモード取得

        if( mapping == FbxGeometryElement::eByPolygonVertex ||
            reference == FbxGeometryElement::eIndexToDirect )
        {
            //!UV情報取得
            FbxLayerElementArrayTemplate<s32> *UVIndex = &pUVElement->GetIndexArray();  //インデックスバッファ取得
            s32 UVIndexCount = UVIndex->GetCount();                                     //インデックス数取得
            (*ppElement)->pUVSet[i].pUVBuf = new VEC2[UVIndexCount];                    //UV座標メモリ確保
                
            //!UV取得
            VEC2 tmpCood;
            for( s32 j=0 ; j<UVIndexCount ; ++j )
            {
                (*ppElement)->pUVSet[i].pUVBuf[j].x = static_cast<f32>(pUVElement->GetDirectArray().
                                                                            GetAt( UVIndex->GetAt(j) )[0] );
                (*ppElement)->pUVSet[i].pUVBuf[j].y = 1.0f - static_cast<f32>(pUVElement->GetDirectArray().
                                                                            GetAt( UVIndex->GetAt(j) )[1] );
            }
            //!UV名取得
                (*ppElement)->pUVSet[i].UVSetName = pUVElement->GetName();
        }else
        {
            //blenderのみに対応しているため、他を使用するときは以下にマッピング、リファレンスの項目を
            //追加していくこと
            #ifdef _DEBUG
                MessageBox( Dx::Get()->GetMainHwnd(), _T("UV取得未対応"),_T("FBXエラー") , MB_OK );	
            #endif
            return false;
        }
    }
    return true;
}

//------------------------------------------------------------
//!テクスチャの取得(UVとの合わせ処理)
//------------------------------------------------------------
// <注意!>      ・ディフューズのみ・テクスチャ単一のみ対応
//              ・複数セットに即時対応可(現状未対応)
//------------------------------------------------------------
// (in)     pMesh       :FBXメッシュクラス
//                       →NodeAttributeからダウンキャスト指定する
// (i/o)    ppElement   :メッシュ用マテリアル
// (out)    bool        :フラグ
//------------------------------------------------------------
bool FBXLoaderFunction::GetTexture( FbxMesh *pMesh,  FBXData**ppElement )
{
    //UVカウントが0ならエラー
    if( !(*ppElement)->UVSetCount )
        return false;
    if( !(*ppElement)->pUVSet )
        return false;

    FbxNode *pNode = pMesh->GetNode();                                              //ノードの取得

    //マテリアルが0ならエラー
    s32 MaterialCount = pNode->GetMaterialCount();                                  //マテリアル数取得
    if( MaterialCount==0 )
        return false;
    
    for( s32 i=0 ; i<MaterialCount ; ++i)
    {
        FbxSurfaceMaterial *pMaterial = pNode->GetMaterial(i);                      //マテリアル取得
        FbxProperty prop = pMaterial->FindProperty
                            (FbxSurfaceMaterial::sDiffuse);                         //検索要素指定<ディフューズ>
        s32 LayerTexCount = prop.GetSrcObjectCount<FbxLayeredTexture>();            //レイヤーテクスチャー数取得

        //!レイヤテクスチャーが複数時(テクスチャブレンド使用時)
        if(0 < LayerTexCount)
        {
            for( s32 n=0 ; n<LayerTexCount ; ++n )
            {
                FbxLayeredTexture *pTex = prop.GetSrcObject<FbxLayeredTexture>(n);  //レイヤーテクスチャ取得
                s32 LayerCount = pTex->GetSrcObjectCount<FbxFileTexture>();         //レイヤー数取得
                for( s32 j=0 ; j<LayerCount ; ++j )
                {
                    FbxFileTexture *ptex = prop.GetSrcObject<FbxFileTexture>(j);     //テクスチャ取得
                    if(ptex)
                    {
                        std::string TexName = ptex->GetRelativeFileName();          //テクスチャ名取得<相対>
                        std::string UVSetName = ptex->UVSet.Get().Buffer();         //UVSet名を取得
                        for( u32 p=0 ; p<(*ppElement)->UVSetCount ; ++p )
                        {
                            if( (*ppElement)->pUVSet[p].UVSetName == UVSetName )
                            {
                                (*ppElement)->pUVSet[p].TEXName.push_back(TexName);
                            }
                        }
                    }
                }
            }
        }
        //!レイヤテクスチャー単一時
        else
        {
            s32 FileTexCount = prop.GetSrcObjectCount<FbxFileTexture>();
            if(0 < FileTexCount)
            {
                for( s32 k=0 ; k<FileTexCount ; ++k )
                {
                    FbxFileTexture *ptex = prop.GetSrcObject<FbxFileTexture>(k);//テクスチャ取得
                    if(ptex)
                    {
                        std::string TexName = ptex->GetRelativeFileName();      //テクスチャ名取得<相対>
                        std::string UVSetName = ptex->UVSet.Get().Buffer();     //UVSet名を取得
                        for( u32 p=0 ; p<(*ppElement)->UVSetCount ; ++p )
                        {
                            if( (*ppElement)->pUVSet[p].UVSetName == UVSetName )
                            {
                                (*ppElement)->pUVSet[p].TEXName.push_back(TexName);
                            }
                        }
                    }
                }
            }
        }
    }
    return true;
}

// -------------------------------------------------
// ボーン情報の読み込み
// -------------------------------------------------
// ・ボーン情報→最適化必要
// ・ボーン初期姿勢→最適化必要なし
// -------------------------------------------------
// <注意!>:先にメッシュ情報を読み込むこと!
// ・ボーンが無い場合は何もしないでfalseを返す
// -------------------------------------------------
// (in)     pMesh               :FBXメッシュクラス
//           →NodeAttributeからダウンキャスト指定する
// (i/o)    ppElement           :メッシュ用マテリアル
// (out)    bool                :フラグ
// -------------------------------------------------
bool FBXLoaderFunction::GetBoneInfo( FbxMesh *pMesh, FBXData **ppElement )
{
#ifdef _DEBUG
    if( !TmpData.pVertexBuf )
    {
        MessageBox( Dx::Get()->GetMainHwnd(), _T("ボーンの前にメッシュ情報が読まれてません") , _T("FBXLoaderFunction") , MB_OK );
        return false;
    }
#endif

//=====<ローカル変数>=============
    s32         SkinCount       = 0;    //スキン数
    FbxCluster  *pCluster       = 0;    //ボーン情報バッファ
    s32         LinkNum         = 0;    //影響を与えている頂点数
    s32         *pLinkArray     = 0;    //影響を与えている頂点配列
    f64         *pWeightArray   = 0;    //影響を与えているウェイト配列
    MATRIX      tmpDxMat;               //初期姿勢一時保管行列
    MATRIX      LeftMat;                //左手系変換行列(Z反転)
    MATRIX      RotateMat;              //左手系変換行列(x-90度)

    //左手座標系変換行列作成
    D3DXMatrixIdentity(&LeftMat);
    LeftMat._33 = -1.0f;
    D3DXMatrixRotationX( &RotateMat, -1.0f * (D3DX_PI * (1.0f/2.0f) ));

//=====<スキン数取得>=============
    //スキン情報は一つのみに対応
    SkinCount =  pMesh->GetDeformerCount( FbxDeformer::eSkin );
    if( !SkinCount )    return false;   //スキン情報がない
    if( SkinCount>1 )   return false;   //スキン情報が多い

//=====<ボーン取得>======
    //!スキン内ボーン数取得
    TmpData.BoneCount = ( (FbxSkin*)pMesh->GetDeformer( 0, FbxDeformer::eSkin ) )->GetClusterCount();
    if( TmpData.BoneCount == 0 )
    {
        return false;
    }

    //!保管領域確保
    TmpData.pBoneWeightBuf  = new TmpBoneData_t[TmpData.VertexCount];   //頂点一時保管領域
    (*ppElement)->pInitPose = new MATRIX[TmpData.BoneCount];            //初期姿勢領域

    //!ボーン情報取得(ウェイト・初期姿勢)
    for( s32 i=0 ; i<TmpData.BoneCount ; ++i )
    {
        pCluster    = ( (FbxSkin*)pMesh->GetDeformer( 0, FbxDeformer::eSkin ) )->GetCluster(i);
        LinkNum     = pCluster->GetControlPointIndicesCount();  //影響を与えている頂点数取得
        pLinkArray  = pCluster->GetControlPointIndices();       //影響を与えている頂点配列取得
        pWeightArray= pCluster->GetControlPointWeights();       //影響を与えているウェイト配列取得

        //!頂点インデックスとウェイト取得(最適化必要)
        for( s32 j=0; j<LinkNum ; ++j )
        {
            //ボーン番号
            TmpData.pBoneWeightBuf[ pLinkArray[j] ].BoneID.push_back(i);
            //ウェイト値
            TmpData.pBoneWeightBuf[ pLinkArray[j] ].BoneWeight_t.push_back((f32)pWeightArray[j]);
        }

        //!ボーン初期姿勢取得(最適化必要なし)
        //取得
        FbxMatrix tmpFbxMat = pCluster->GetLink()->EvaluateGlobalTransform();
        ConvertMatFbxToDx( tmpDxMat, tmpFbxMat);
        //座標系合わせ(左手系へ)
        tmpDxMat *= LeftMat;
        tmpDxMat *= RotateMat;
        (*ppElement)->pInitPose[i] = tmpDxMat;
    }
    return true;
}

// ---------------------------------------------------------
// アニメーション(ボーンフレーム姿勢)取出し
// ---------------------------------------------------------
// ・最適化必要なし
// ---------------------------------------------------------
// <注意!>:先にボーン情報を読み込むこと!
//----------------------------------------------------------
// (in)     pMesh       :FBXメッシュクラス
//                      →NodeAttributeからダウンキャスト指定する
// (i/o)    ppElement   :メッシュ用マテリアル
// (in)     pFrameData  :一時保管しているフレームデータ
// (out)    bool        :フラグ
// ---------------------------------------------------------
bool FBXLoaderFunction::GetAnimFrame( FbxMesh *pMesh, FBXData **ppElement, const FrameData_t *pFrameData )
{
//====<ローカル変数>============
    FbxCluster  *pCluster = 0;  //クラスタ
    MATRIX      LeftMat;        //左手系変換行列(Z反転)
    MATRIX      RotateMat;      //左手系変換行列(x-90度)

    //左手座標系変換行列作成
    D3DXMatrixIdentity(&LeftMat);
    LeftMat._33 = -1.0f;
    D3DXMatrixRotationX( &RotateMat, -1.0f * (D3DX_PI * (1.0f/2.0f) ));

//====<フレーム姿勢取出し>======

    //!マテリアルのメモリ確保、初期化
    (*ppElement)->pFramePose[pFrameData->RecentTake].FrameNum = pFrameData->FrameCount;
    (*ppElement)->pFramePose[pFrameData->RecentTake].pBonePoseMat = new BonePoseMat[pFrameData->FrameCount];

    //!フレーム毎の取得
    for( s32 i=0 ; i<pFrameData->FrameCount ; ++i )
    {
        //ボーンメモリ確保
        (*ppElement)->pFramePose[pFrameData->RecentTake].pBonePoseMat[i].pBoneMat = new MATRIX[TmpData.BoneCount];

        //!ボーンごとに取得
        for( s32 j=0 ; j<TmpData.BoneCount ; ++j )
        {
            //ボーン切り替え
            pCluster = ( (FbxSkin*)pMesh->GetDeformer( 0, FbxDeformer::eSkin ) )->GetCluster(j);
            
            //取得・変換
            FbxMatrix tmpFbxMat = pCluster->GetLink()->EvaluateGlobalTransform( pFrameData->StartTime + pFrameData->PeriodTime * i );
            ConvertMatFbxToDx( (*ppElement)->pFramePose[pFrameData->RecentTake].pBonePoseMat[i].pBoneMat[j], tmpFbxMat );
            (*ppElement)->pFramePose[pFrameData->RecentTake].pBonePoseMat[i].pBoneMat[j] *= LeftMat;
            (*ppElement)->pFramePose[pFrameData->RecentTake].pBonePoseMat[i].pBoneMat[j] *= RotateMat;;
        }
    }
    return true;
}

// ---------------------------------------------------------
// <固定>:最適化処理
// ---------------------------------------------------------
// ・一時保管領域のデータを最適化してメッシュデータに渡す
// ---------------------------------------------------------
// (i/o)    ppElement   :メッシュ用マテリアル(固定)
// (out)    bool        :フラグ
// ---------------------------------------------------------
bool FBXLoaderFunction::OptimizeData( FBXData **ppElement )
{
    //====<領域確保>================
    if(TmpData.pVertexBuf)
        (*ppElement)->pVertex = new VEC3[TmpData.IndexCount];
    if(TmpData.pNormalBuf)
        (*ppElement)->pNormal = new VEC3[TmpData.IndexCount];

    //====<基本情報：データ格納>=======
    (*ppElement)->VertexCount   = TmpData.IndexCount;
    (*ppElement)->PolygonCount  = TmpData.IndexCount / 3;

    //====<最適化：データ格納>=======
    for( s32 i=0 ; i<TmpData.IndexCount ; ++i )
    {
        //<頂点データ>
        if(TmpData.pVertexBuf)
        {
            (*ppElement)->pVertex[i].x = (f32)TmpData.pVertexBuf[TmpData.pIndexBuf[i]][0];
            (*ppElement)->pVertex[i].y = (f32)TmpData.pVertexBuf[TmpData.pIndexBuf[i]][1];
            (*ppElement)->pVertex[i].z = -1.0f * (f32)TmpData.pVertexBuf[TmpData.pIndexBuf[i]][2];
        }

        //<法線>
        if(TmpData.pNormalBuf)
        {
            (*ppElement)->pNormal[i].x = (f32)TmpData.pNormalBuf->GetDirectArray().GetAt(TmpData.pIndexBuf[i])[0];
            (*ppElement)->pNormal[i].y = (f32)TmpData.pNormalBuf->GetDirectArray().GetAt(TmpData.pIndexBuf[i])[1];
            (*ppElement)->pNormal[i].z = -1.0f * (f32)TmpData.pNormalBuf->GetDirectArray().GetAt(TmpData.pIndexBuf[i])[2];
        }
    }

    return true;
}

// ---------------------------------------------------------
// <アニメ>最適化処理
// ---------------------------------------------------------
// ・一時保管領域のデータを最適化してメッシュデータに渡す
// ---------------------------------------------------------
// (i/o)    ppElement   :メッシュ用マテリアル(アニメ)
// (out)    bool        :フラグ
// ---------------------------------------------------------
bool FBXLoaderFunction::OptimizeDataEX( FBXData **ppElement )
{
    //====<領域確保>================
    if(TmpData.pVertexBuf)
        (*ppElement)->pVertex       = new VEC3[TmpData.IndexCount];
    if(TmpData.pNormalBuf)
        (*ppElement)->pNormal       = new VEC3[TmpData.IndexCount];
    if(TmpData.pBoneWeightBuf)
        (*ppElement)->pBoneWeight   = new BoneWeight_t[TmpData.IndexCount];

    //====<基本情報：データ格納>=======
    (*ppElement)->VertexCount   = TmpData.IndexCount;
    (*ppElement)->PolygonCount  = TmpData.IndexCount / 3;
    (*ppElement)->BoneCount     = TmpData.BoneCount;

    //====<最適化：データ格納>=======
    for( s32 i=0 ; i<TmpData.IndexCount ; ++i )
    {
        //<頂点データ>
        if(TmpData.pVertexBuf)
        {
            (*ppElement)->pVertex[i].x = (f32)TmpData.pVertexBuf[TmpData.pIndexBuf[i]][0];
            (*ppElement)->pVertex[i].y = (f32)TmpData.pVertexBuf[TmpData.pIndexBuf[i]][1];
            (*ppElement)->pVertex[i].z = -1.0f * (f32)TmpData.pVertexBuf[TmpData.pIndexBuf[i]][2];
        }

        //<法線>
        if(TmpData.pNormalBuf)
        {
            (*ppElement)->pNormal[i].x = (f32)TmpData.pNormalBuf->GetDirectArray().GetAt(TmpData.pIndexBuf[i])[0];
            (*ppElement)->pNormal[i].y = (f32)TmpData.pNormalBuf->GetDirectArray().GetAt(TmpData.pIndexBuf[i])[1];
            (*ppElement)->pNormal[i].z = -1.0f * (f32)TmpData.pNormalBuf->GetDirectArray().GetAt(TmpData.pIndexBuf[i])[2];
        }
    }

    for( s32 i=0 ; i<TmpData.IndexCount ; ++i )
    {
        //<ボーン情報>
        if(TmpData.pBoneWeightBuf)
        {
            //===ウェイトID============

            //初期化
            (*ppElement)->pBoneWeight[i].ID[0] = 0; (*ppElement)->pBoneWeight[i].ID[1] = 0;
            (*ppElement)->pBoneWeight[i].ID[2] = 0; (*ppElement)->pBoneWeight[i].ID[3] = 0;

            //エラーチェック(ウェイトが多い)
            if( TmpData.pBoneWeightBuf[TmpData.pIndexBuf[i]].BoneID.size() > 4)
            {
                (*ppElement)->ErrorInfo = FBXData::ERROR_WEIGHT;
                (*ppElement)->DataState = FBXData::FIX; //固定のみの描画に変更
                (*ppElement)->ErrorCount++;
                SAFE_DELETE_ARRAY((*ppElement)->pBoneWeight);
                return false;
            }
            
            //格納
            s32 j = 0;
            std::list<u8>::iterator	Ite1 = TmpData.pBoneWeightBuf[TmpData.pIndexBuf[i]].BoneID.begin();
            while( Ite1 != TmpData.pBoneWeightBuf[TmpData.pIndexBuf[i]].BoneID.end() )
            {
                (*ppElement)->pBoneWeight[i].ID[j] = *Ite1;
                ++Ite1;
                ++j;
            }

            //===ウェイト==============

            //初期化
            f32 tmp[4] = {0.0f,0.0f,0.0f,0.0f};
            s32 n = 0;

            //イテレーター
            std::list<f32>::iterator Ite2 = TmpData.pBoneWeightBuf[TmpData.pIndexBuf[i]].BoneWeight_t.begin();
            while( Ite2 != TmpData.pBoneWeightBuf[TmpData.pIndexBuf[i]].BoneWeight_t.end() )
            {
                tmp[n] = *Ite2;
                ++Ite2;
                ++n;
            }

            //格納
            (*ppElement)->pBoneWeight[i].Weight.x = tmp[0]; (*ppElement)->pBoneWeight[i].Weight.y = tmp[1];
            (*ppElement)->pBoneWeight[i].Weight.z = tmp[2]; (*ppElement)->pBoneWeight[i].Weight.w = tmp[3];
        }
    }

    return true;
}

// ---------------------------------------------------------
// <Private>:行列変換(FBX→Directx)
// ---------------------------------------------------------
// (i/o)    DxMat       :変換後
// (in)     FbxMat      :変換前
// ---------------------------------------------------------
void FBXLoaderFunction::ConvertMatFbxToDx( MATRIX &DxMat, FbxMatrix &FbxMat )
{
        DxMat._11 = (f32)FbxMat.Get(0,0);
        DxMat._12 = (f32)FbxMat.Get(0,1);
        DxMat._13 = (f32)FbxMat.Get(0,2);
        DxMat._14 = (f32)FbxMat.Get(0,3);
        DxMat._21 = (f32)FbxMat.Get(1,0);
        DxMat._22 = (f32)FbxMat.Get(1,1);
        DxMat._23 = (f32)FbxMat.Get(1,2);
        DxMat._24 = (f32)FbxMat.Get(1,3);
        DxMat._31 = (f32)FbxMat.Get(2,0);
        DxMat._32 = (f32)FbxMat.Get(2,1);
        DxMat._33 = (f32)FbxMat.Get(2,2);
        DxMat._34 = (f32)FbxMat.Get(2,3);
        DxMat._41 = (f32)FbxMat.Get(3,0);
        DxMat._42 = (f32)FbxMat.Get(3,1);
        DxMat._43 = (f32)FbxMat.Get(3,2);
        DxMat._44 = (f32)FbxMat.Get(3,3);
}

}}
