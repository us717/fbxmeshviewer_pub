//====================================================================
// メッシュ関連データ定義
//====================================================================
#ifndef Include_FBXData_H
#define Include_FBXData_H

//====<インクルード>============================================
#include <string>

//====<前方宣言>================================================
namespace Draw{ namespace Font{ class DrawFont; }}

namespace FileLoader{
namespace FBXLoader{

//=========================================================
// マテリアル用定義:サブ
//=========================================================

// ------------------------------------------
// クラス：<ボーン姿勢>
// ------------------------------------------
class BonePoseMat{
public:
    //!コンストラクタ・デストラクタ
    BonePoseMat():pBoneMat(0){}
    ~BonePoseMat(){ SAFE_DELETE_ARRAY(pBoneMat); }
public:
    //!変数
    MATRIX *pBoneMat; //ボーンごとの姿勢行列
};

//=========================================================
// マテリアル用定義:メイン
//=========================================================

// ------------------------------------------
// クラス：<UV・テクスチャデータ>
// ------------------------------------------
// <注意> テクスチャ情報は直接管理するので、
//        読み込まないことにする
// ------------------------------------------
class UVSet{
public:
    //!コンストラクタ・デストラクタ
    UVSet():pUVBuf(0){}
    ~UVSet(){ SAFE_DELETE_ARRAY(pUVBuf); }
public:
    //!変数
    std::string             UVSetName;  //UVセット名
    VEC2                   *pUVBuf;     //UV座標
    std::list<std::string>  TEXName;    //テクスチャ名
};

// ------------------------------------------
// 構造体：<ウェイト情報>
// ------------------------------------------
typedef struct BoneWeight_t{
    u8       ID[4];     //ボーン配列
    VEC4     Weight;    //ウェイト
}BoneWeight_t;

// ------------------------------------------
// クラス：<フレーム>
// ------------------------------------------
class Frame{
public:
    //!コンストラクタ・デストラクタ
    Frame():pBonePoseMat(0){}
    ~Frame(){ SAFE_DELETE_ARRAY(pBonePoseMat); }
public:
    //!変数
    u32             FrameNum;       //フレーム数
    BonePoseMat    *pBonePoseMat;   //フレームごとの各ボーン姿勢
};

//=========================================================
// マテリアル定義
//=========================================================

// ------------------------------------------
// クラス:メッシュ描画用マテリアル群
// ------------------------------------------
class FBXData{

//=====定義==========================
public:
    //!データの状態
    typedef enum DataState_t{
        NONE   = 0,     //データが存在しない
        FIX,            //固定データ
        ANIME,          //アニメデータ
    }DataState_t;

    //!エラー情報
    typedef enum ErrorState_t{
        ERROR_NONE = 0, //なし
        ERROR_INIT,     //初期化
        ERROR_VERTEX,   //頂点
        ERROR_NORMAL,   //法線
        ERROR_UV,       //UV
        ERROR_TEX,      //テクスチャ
        ERROR_WEIGHT,   //ウェイトが多い
        ERROR_NOTAKE,   //テイク情報が存在しない
    }ErrorState_t;

private:
    //!メッシュ情報
    typedef enum MeshInfo_t{
        DATA_STATE = 0,
        ERROR_INFO,
        ERROR_COUNT,
        VERTEX,
        POLYGON,
        BONE,
        TAKE,
    }MeshInfo_t;

    //!内部定義
    enum{ INFO_NUM = 7, BUF_SIZE = 126, };

//=====名前空間======================
private:
    typedef Draw::Font::DrawFont DrawFont;

//=====構築子：消去子=================
private:
    //!コピー禁止
    void operator = (const FBXData& Data){}
    FBXData(const FBXData& Data){}

public:
    //!コンストラクタ・デストラクタ
    FBXData();
    FBXData( bool UseDebugInfo );
    virtual ~FBXData();

//=====変数===========================
private:
    DrawFont       *pFont;                  //情報表示用フォント
    TCHAR          Buf[INFO_NUM][BUF_SIZE]; //情報表示用バッファ

public:
    DataState_t    DataState;               //データの状態
    ErrorState_t   ErrorInfo;               //エラー情報
    u32            ErrorCount;              //エラーの数

    //!メッシュデータ
    u32            VertexCount;             //頂点数
    u32            PolygonCount;            //ポリゴン数
    u32            UVSetCount;              //UVセットの数
    VEC3           *pVertex;                //頂点データ
    VEC3           *pNormal;                //法線データ
    UVSet          *pUVSet;                 //UV座標・テクスチャセット
    
    //!アニメデータ
    u32            BoneCount;               //ボーン数
    u32            TakeNum;                 //テイク数
    BoneWeight_t   *pBoneWeight;            //各頂点のウェイト情報
    MATRIX         *pInitPose;              //初期姿勢
    Frame          *pFramePose;             //フレーム姿勢

//=====関数===========================

    //!メッシュ情報(デバグ用表示)
private:
    void InfoCopyArray( TCHAR *pOutArray, LPCTSTR pIn );    //メッシュ情報配列コピー
public:
    bool CreateInfo();                                      //メッシュ情報の作成
    bool DrawInfo( const f32 &MenuX, const f32 &MenuY,      //メッシュ情報の表示
                     const f32 &DataX, const f32 &Space );

};

}}
#endif
