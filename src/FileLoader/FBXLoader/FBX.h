//=======================================================
// フォーマット：FBX
//-------------------------------------------------------
// ファイルローダーで管理している
//=======================================================
#ifndef Include_FBX_H
#define Include_FBX_H

//====<インクルード>===================
#include"FBXData.h"

//====<前方宣言>=======================
namespace FileLoader{ class FileLoader; }

namespace FileLoader{
namespace FBXLoader{
// ------------------------------------------
// クラス:FBX
// ------------------------------------------
// (in) sFileName   :ファイル名 s8型
// (in) tFileName   :ファイル名 LPCTSTR型
// ------------------------------------------
class FBX{
//=====構築子：消去子=================
public:
//!コンストラクタ
    FBX( const s8 *sFileName , LPCTSTR tFileName )
        :pData(0),flag(false)
    {
        this->tFileName = tFileName;
        this->sFileName = sFileName;
        pData = new FBXData;
    }

    FBX( const s8 *sFileName , LPCTSTR tFileName, bool UseDebugInfo )
    :pData(0),flag(false)
    {
        this->tFileName = tFileName;
        this->sFileName = sFileName;
        pData = new FBXData(UseDebugInfo);
    }

private:
//!コピー禁止
    FBX( const FBX &src ){}
    void operator = ( const FBX &src ){}

//!デストラクタ
    ~FBX(){ SAFE_DELETE(pData); }

//!フレンド
    friend class FileLoader;

//=====変数==========================
private:
    LPCTSTR     tFileName;
    const s8    *sFileName;
    bool        flag;
    FBXData     *pData;

//=====関数==========================
private:
    //!ロード済みかの確認
    bool Redy() const{ return(!flag?false:true);}

    //!ロード済みにする
    void Set()
    {
    #ifdef _DEBUG
        assert( flag == false && tFileName );
    #endif
        flag = true;
    }

public:
    //!固定用データへのアクセス
    FBXData* GetData()
    {
    #ifdef _DEBUG
        assert( pData != 0 && tFileName && _T("データがありません"));
    #endif
        return pData;
    }
};

}}

#endif