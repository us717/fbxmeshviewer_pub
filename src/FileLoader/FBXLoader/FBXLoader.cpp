//+****************************************************************
//| FBXファイルの読み込み
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <fbxsdk.h>
#include "FBXLoaderFunction.h"
#include "FBXData.h"
#include "FBXLoader.h"

namespace FileLoader{
namespace FBXLoader{

// -------------------------------------------------
// コンストラクタ
// -------------------------------------------------
FBXLoader::FBXLoader():
    pFbxManager(0),
    pFbxImporter(0),
    pScene(0),
    pFbxIoSet(0),
    pTmpFrameData(0),
    pElement(0),
    pFunc(0)
{
    //!FBX_SDKマネージャーの生成
    pFbxManager = FbxManager::Create();
    //!IOセッティングの生成
    pFbxIoSet = FbxIOSettings::Create( pFbxManager, IOSROOT );
}

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
FBXLoader::~FBXLoader()
{
    //!データ格納領域クリア
    if(pScene)
    {
        pScene->Destroy();
        pScene=0;
    }
    if(pElement)	pElement=0;
    SAFE_DELETE(pFunc);
    SAFE_DELETE(pTmpFrameData);

    //!IOセッティング解放
    if(pFbxIoSet)
        pFbxIoSet->Destroy();
        pFbxIoSet = 0;

    //!FBX_SDKマネージャーの解放
    if(pFbxManager)
        pFbxManager->Destroy();
        pFbxManager = 0;
}

// -------------------------------------------------
// private:FBXインポーター解放処理
// -------------------------------------------------
void FBXLoader::Release()
{
    pScene->Destroy();
    pScene = 0;
    this->pElement = 0;
    SAFE_DELETE(pFunc);
    FileName = 0;
}

// -------------------------------------------------
// FBXファイルの読み込み
// -------------------------------------------------
// (in)     pfilename   :ファイル名
// (in)     tFilename   :ファイル説明(デバグ表示用)
// (i/o)    pElement    :格納先へのポインタ
// (out)    bool        :フラグ
// -------------------------------------------------
bool FBXLoader::ImportFBXFile( const s8* pfilename, LPCTSTR tFilename,
                            FBXData *pElement )
{
    //!格納要素初期化
    if(!this->pElement )
    this->pElement = 0;
    this->pElement = pElement;
    FileName       = tFilename;
    pFunc          = new FBXLoaderFunction;

    //!FBXインポータの生成
    pFbxImporter = FbxImporter::Create( pFbxManager, "");

    //!ファイル名エラーチェック
    for( u32 i=0 ; pfilename[i]!='\0' ; ++i )
    {
        if( i >=512 )
        {
            MessageBox( Dx::Get()->GetMainHwnd(), _T("ファイルパスが長すぎます"), _T("エラー"), MB_OK );
            return false;
        }
    }
    
    //!パスを日本語対応に(関数にすると文字化けが発生、SDKのバグの可能性あり)
    char FullPath[512] = {0};
    char* Path = 0;
    _fullpath( FullPath, pfilename, 512 );
    FbxAnsiToUTF8( FullPath, Path );

    //!ファイルからインポータに読み込み
    if( !pFbxImporter->Initialize( Path, -1, pFbxIoSet) )
    {
        pElement->DataState = FBXData::NONE;
        pElement->ErrorInfo	= FBXData::ERROR_INIT;
        pElement->ErrorCount++;
        SAFE_DELETE_ARRAY(Path);
        return false;
    }
    SAFE_DELETE_ARRAY(Path);

    //!シーンの作成
    pScene = FbxScene::Create( pFbxManager, "" );

    //インポータから読み込み
    pFbxImporter->Import(pScene);
    pFbxImporter->Destroy(); //FBXインポータの解放
    pFbxImporter = 0;

    //!メッシュノード解析
    GetMeshData(pScene);
    if( pElement->ErrorInfo != FBXData::ERROR_NONE )
    {
        pElement->DataState = FBXData::NONE;
        pElement->ErrorCount++;
        Release();
        return false;
    }

    //!アニメノード解析
    if( pElement->DataState == FBXData::ANIME )
    {
        GetAnimData(pScene);
    }

    //!最適化
    switch( pElement->DataState )
    {
        //====固定=================
        case FBXData::FIX:
            pFunc->OptimizeData( &pElement );
        break;

        //====アニメ===============
        case FBXData::ANIME:
            pFunc->OptimizeDataEX( &pElement );
        break;
    }

    //!解放処理
    Release();
    return true;
}

//=====<メッシュノード:解析>=========================================================

// -------------------------------------------------
// <private>:メッシュノード解析
// -------------------------------------------------
// (in) pScene:     インポータから読み込んだシーン
// -------------------------------------------------
void FBXLoader::GetMeshData( FbxScene *pScene )
{
    //!変数
    FbxNode *pRootNode  = 0; //ルートノード
    FbxNode *pChildNode = 0; //子ノード

    //!再帰：ノード解析
    pRootNode = pScene->GetRootNode();                 //ルートノードの取得
    for( s32 i=0 ; i<pRootNode->GetChildCount(); i++ ) //子ノードを取得
    {
        MeshNodeRecursive(pRootNode->GetChild(i));
    }
}

// -------------------------------------------------
// <private>:メッシュデータ取得用再帰関数
// -------------------------------------------------
// (in) pNode   :親ノード
// -------------------------------------------------
void FBXLoader::MeshNodeRecursive( FbxNode *pNode )
{
    //!データ取得
    for( s32 i=0 ; i<pNode->GetNodeAttributeCount() ; i++ )
    {
        if( !PrintAttribute( pNode->GetNodeAttributeByIndex(i) ) )
        {
            break;
        }
    }

    //!再帰処理
    for( s32 j=0 ; j<pNode->GetChildCount() ; j++ )
    {
        MeshNodeRecursive(pNode->GetChild(j));
    }
}

// -------------------------------------------------
// private3:要素取出し
// -------------------------------------------------
//・FBXからメッシュ情報を取り出す
// -------------------------------------------------
// (in)     pAttribute  :属性
// (out)    bool        :フラグ
// -------------------------------------------------
bool FBXLoader::PrintAttribute( FbxNodeAttribute* pAttribute )
{
    if(pAttribute)
    {
        FbxNodeAttribute::EType type = pAttribute->GetAttributeType();
        switch(type)
        {
            //====不明ノード===================
            case FbxNodeAttribute::eUnknown:
                return true;

            //====なし=========================
            case FbxNodeAttribute::eNull:
                return true;

            //====メッシュノード================
            case FbxNodeAttribute::eMesh:
                if( !pFunc->GetMeshBuf( (FbxMesh*)pAttribute) )             //基本情報・頂点・インデックスバッファ
                {
                    pElement->DataState = FBXData::NONE;
                    pElement->ErrorInfo = FBXData::ERROR_VERTEX;
                    pElement->ErrorCount++;
                    return false;
                }
                if( !pFunc->GetNormalBuf( (FbxMesh*)pAttribute) )           //法線
                {
                    pElement->DataState = FBXData::NONE;
                    pElement->ErrorInfo = FBXData::ERROR_NORMAL;
                    pElement->ErrorCount++;
                    return false;
                }
                if( !pFunc->GetMeshUV ( (FbxMesh*)pAttribute, &pElement ) ) //UV
                {
                    pElement->DataState = FBXData::NONE;
                    pElement->ErrorInfo = FBXData::ERROR_UV;
                    pElement->ErrorCount++;
                    return false;
                }
                if( !pFunc->GetTexture( (FbxMesh*)pAttribute, &pElement ) ) //テクスチャ
                {
                    pElement->DataState = FBXData::NONE;
                    pElement->ErrorInfo = FBXData::ERROR_TEX;
                    pElement->ErrorCount++;
                    return false;
                }
                
                pElement->DataState = FBXData::FIX; //固定描画が可能になる
                
                if( !pFunc->GetBoneInfo( (FbxMesh*)pAttribute, &pElement ) ) //ボーン情報
                {
                    //固定データの可能性があるのでエラー処理しない
                    return false;
                }
                pElement->DataState = FBXData::ANIME; //アニメ描画が可能になる

            break;
        }
    }
    return true;
}

//=====<アニメーションノード解析>=============================================

// -------------------------------------------------
// <private>:アニメーションノード解析
// -------------------------------------------------
// <注意!>:先にメッシュノードを解析しておくこと!
// -------------------------------------------------
// (in)     pScene  :インポータから読み込んだシーン
// (out)    bool    :フラグ
// -------------------------------------------------
bool FBXLoader::GetAnimData( FbxScene *pScene )
{
    //!変数
    FbxArray<FbxString*>TakeName; //テイク情報
    FbxAnimStack *pStack = 0;
    pTmpFrameData = new FrameData_t;

    //!テイク情報取得
    pScene->FillAnimStackNameArray(TakeName); //各アニメーションの名前取得
    pElement->TakeNum = TakeName.GetCount();  //テイク数

    //!テイク情報エラーチェック
    if( pElement->TakeNum == 0 )
    {
        //テイク情報がない場合は固定を使用する
        pElement->DataState = FBXData::FIX;
        pElement->ErrorInfo = FBXData::ERROR_NOTAKE;
        pElement->ErrorCount++;
    
        //メモリ解放
        FbxArrayDelete(TakeName);
        SAFE_DELETE(pTmpFrameData);

        return false;
    }
    
    //!メモリ確保
    pElement->pFramePose = new Frame[pElement->TakeNum];                //テイク情報の数だけメモリ確保

    //!時間モードの設定
    FbxGlobalSettings &GlobalTimeSet = pScene->GetGlobalSettings();
    FbxTime::EMode TimeMode  = GlobalTimeSet.GetTimeMode();
    pTmpFrameData->PeriodTime.SetTime( 0, 0, 0, 1, 0, TimeMode);        //単位時間(1フレーム時間)取得
    
    //!テイク毎にアニメノード解析
    for( u32 i=0 ; i<pElement->TakeNum ; ++i )
    {
        pStack = pScene->GetSrcObject<FbxAnimStack>(i);                 //各テイク情報取得
        pScene->GetEvaluator()->SetContext(pStack);                     //シーン切り替え
        pTmpFrameData->RecentTake = i;                                  //現在のテイク番号格納

        //フレーム時間取得
        FbxTakeInfo *pCurrntTake = pScene->GetTakeInfo( *(TakeName[i]) );
        if(pCurrntTake)
        {
            //FbxTime取得
            FbxTime Start = pCurrntTake->mLocalTimeSpan.GetStart();
            pTmpFrameData->StartTime = Start;
            FbxTime Stop = pCurrntTake->mLocalTimeSpan.GetStop();
            
            //フレーム数の取得
            s32 StartFrame = (s32)( Start.Get() / pTmpFrameData->PeriodTime.Get() );    //フレーム:開始
            s32 StopFrame  = (s32)( Stop.Get() / pTmpFrameData->PeriodTime.Get() );     //フレーム:終了
            pTmpFrameData->FrameCount = StopFrame - StartFrame;                         //総フレーム数
        }

        //再帰：ノード解析
        FbxNode *pRootNode = pScene->GetRootNode();        //ルートノードの取得
        for( s32 j=0 ; j<pRootNode->GetChildCount(); ++j ) //子ノードを取得
        {
            AnimNodeRecursive(pRootNode->GetChild(j));
        }
    }

    //!メモリ解放
    FbxArrayDelete(TakeName);
    SAFE_DELETE(pTmpFrameData);

    return true;
}

// -------------------------------------------------
// <private>2:アニメーション取得用再帰関数
// -------------------------------------------------
// (in) pNode   :親ノード
// -------------------------------------------------
void FBXLoader::AnimNodeRecursive( FbxNode *pNode )
{
    //!データ取得
    FbxNodeAttribute *pAttribute = pNode->GetNodeAttribute();
    if(pAttribute)
    {
        FbxNodeAttribute::EType type = pAttribute->GetAttributeType();
        //アニメーションフレーム取出し
        if( type ==  FbxNodeAttribute::eMesh)
        {
            pFunc->GetAnimFrame( (FbxMesh*)pAttribute, &pElement, pTmpFrameData) ;
        }
    }
        
    //!再帰処理
    for( s32 j=0 ; j<pNode->GetChildCount() ; ++j )
    {
        AnimNodeRecursive(pNode->GetChild(j));
    }
}

}}