//=======================================================
// FBXファイルの読み込み
//-------------------------------------------------------
//
//=======================================================
#ifndef Include_FBXLoader_H
#define Include_FBXLoader_H

//====<インクルード>================================
#include <fbxsdk.h>

//====<前方宣言>====================================
namespace FileLoader{ namespace FBXLoader{ class FBXData; }}
namespace FileLoader{ namespace FBXLoader{ class FBXLoaderFunction; }}
namespace FileLoader{ namespace FBXLoader{ struct FrameData_t; typedef struct FrameData_t FrameData_t; }}

namespace FileLoader{
namespace FBXLoader{

// -------------------------------------------------
// クラス:FBXファイル読み込み
// -------------------------------------------------
class FBXLoader{
//=====構築子：消去子=========================
public:
//!コンストラクタ<使用不可>
    FBXLoader();
    ~FBXLoader();
private:
    //!コピー禁止
    void operator = (const FBXLoader& Data){}
    FBXLoader(const FBXLoader& Data){}

//=====変数=================================
protected:
    //!FBX関連
    LPCTSTR             FileName;       //ファイル名<エラー表示用>
    FbxManager          *pFbxManager;   //SDKマネージャー
    FbxIOSettings       *pFbxIoSet;     //IOセッティング
    FbxImporter         *pFbxImporter;  //インポータ
    FbxScene            *pScene;        //シーン
    FBXLoaderFunction   *pFunc;         //各データを読み込む機能
    FrameData_t         *pTmpFrameData; //フレームデータ一時保存領域

public:
    //!格納データ関連
    FBXData             *pElement;     //データ格納要素

//=====関数=================================
protected:
    //!FbxSdk制御
    void Release();                                             //FBXインポーター解放処理
    //!メッシュ情報
    void GetMeshData       ( FbxScene *pScene );                //メッシュ :ノード解析
    bool PrintAttribute    ( FbxNodeAttribute* pAttribute );    //要素取出し
    void MeshNodeRecursive ( FbxNode *pNode );                  //メッシュ :取得用再帰関数
    void AnimNodeRecursive ( FbxNode *pNode );                  //アニメ   :取得用再帰関数

    //!アニメーションフレーム情報
    bool GetAnimData( FbxScene *pScene );                       //アニメーションフレーム:ノード解析

public:
    bool ImportFBXFile( const s8 *pfilename, LPCTSTR tFilename, //FBXファイルの読み込み
                            FBXData *pElement );
};


}}
#endif