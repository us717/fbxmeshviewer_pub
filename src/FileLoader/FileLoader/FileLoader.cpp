//+****************************************************************
//| ファイルローダー
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include <map>
#include <fstream>
#include <string>
#include <lua.hpp>
//ファイルリスト
#include "Tex.h"
#include "FBX.h"
#include "SoundFile.h"
#include "SoundStream.h"
#include "FBXLoader.h"
#include "LuaState.h"
#include "FileLoader.h"

namespace FileLoader{

//------------------------------------------
// リソース読み込み方法の設定
//------------------------------------------
// <注意> ・デバッグのみで使用すること
// ・リリース時に面倒なことになる
//------------------------------------------
// (in)     Flag    :フラグ
//------------------------------------------
void FileLoader::SetLoadSystem( bool Flag )
{
    #ifdef _DEBUG
        UsingArchive = Flag;
    #else 
        UsingArchive = true;    //リリース時は強制的にアーカイブ使用にしている(エラー防止のため)
    #endif
}

//------------------------------------------
// ファイルテーブルの作成
//------------------------------------------
// アーカイブ内のファイル一覧表作成
//------------------------------------------
//<データファイル内部仕様>
//#データ部
//  ・データ
//      -データ
//      -ファイル情報の開始位置
//#情報部
//  ・ファイル数       :s32
//  ・ファイル情報
//    -場所           :s32
//    -ファイルサイズ  :s32
//    -名前のサイズ    :s32
//     -ファイル名     :名前のサイズ+1(\0)
//  ・情報部開始位置    :s32
//------------------------------------------
bool FileLoader::CreateFileTable()
{
    //!ファイルを開ける
    std::ifstream File( "sys.bin", std::ifstream::binary );
    if(!File)
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データファイルが見つかりません"), _T("アーカイブエラー"), MB_OK );
    #else
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データファイルが見つかりません"), _T("エラー"), MB_OK );
        Dx::Get()->SystemError(false);
        File.close();
        return false;
    #endif
    }

    //!データを取得するための情報読み取り
    
    //情報開始位置取得
    File.seekg( -4, std::ifstream::end );
    s32 InfoStart = GetInt( &File );
    
    //ファイル数取得
    File.seekg( InfoStart, std::ifstream::beg );
    s32 FileNum = GetInt( &File );
    
    //情報取得
    for( s32 i=0 ; i<FileNum ; ++i )
    {
        FileInfo_t tmpInfo;
        tmpInfo.Position    = GetInt( &File );  //ファイル位置
        tmpInfo.Size        = GetInt( &File );  //ファイルサイズ
        s32 NameLength      = GetInt( &File );  //名前のサイズ
        s8 *pName = new s8[NameLength + 1];
        File.read( pName, NameLength );         //ファイル名
        pName[NameLength] = '\0';

        //ファイルテーブルにファイル情報を登録
        FileTable.insert( std::map< std::string, FileInfo_t >::value_type( pName, tmpInfo ) );

        SAFE_DELETE_ARRAY(pName);
    }

    File.close();

    return true;
}

//------------------------------------------
// テクスチャの登録
//------------------------------------------
//  (i/o)   ppFile      :登録するテクスチャクラス
//  (in)    pC_FileName :テクスチャ名(直接用)
//  (in)    pT_FileName :テクスチャ名(アーカイブ用)
//------------------------------------------
void FileLoader::CreateTex( Tex **ppFile, const s8* pC_FileName, LPCTSTR pT_FileName )
{
#ifdef _DEBUG
    assert( (*ppFile ) == 0 && _T("ポインタを初期化してください") ) ;
#endif
    *ppFile =  new Tex( pC_FileName, pT_FileName );
    ListTex.push_back(*ppFile);
}

//------------------------------------------
// テクスチャ読み込み
//------------------------------------------
void FileLoader::LoadTex()
{
    std::map< std::string, FileInfo_t >::iterator ItMap;
    std::list<Tex*>::iterator it = ListTex.begin();
    while( it != ListTex.end() )
    {
        if( !(*it)->Redy() )
        {
            switch(UsingArchive)
            {
            //===アーカイブ使用時===========
                case true:
                    //検索
                    ItMap = FileTable.find( (*it)->sFileName );
                    if( ItMap != FileTable.end() )
                    {
                        //データ読み込み
                        s8 *pData = 0;
                        FileInfo_t tmp = ItMap->second;
                        Read( tmp, &pData );

                        //テクスチャの作成
                        if( FAILED(D3DXCreateTextureFromFileInMemory( Dx::Get()->GetD3DDevice(), pData, tmp.Size , &(*it)->pTexture ) ) )
                        {
                            #ifdef _DEBUG
                                MessageBox( Dx::Get()->GetMainHwnd(), (*it)->tFileName , _T("テクスチャエラー") , MB_OK );
                            #else
                                MessageBox( Dx::Get()->GetMainHwnd(), (*it)->tFileName , _T("テクスチャエラー") , MB_OK );
                                Dx::Get()->SystemError(false);
                            #endif
                        }
                        SAFE_DELETE_ARRAY(pData);
                    }
                    else
                    {
                        MessageBox( Dx::Get()->GetMainHwnd(), (*it)->tFileName , _T("ファイルが存在しません") , MB_OK );
                        Dx::Get()->SystemError(false);
                    }
                break;
            //===直接読み込み=============
                case false:
                    if( FAILED(D3DXCreateTextureFromFile( Dx::Get()->GetD3DDevice(), (*it)->tFileName, &(*it)->pTexture ) ) )
                    {
                    #ifdef _DEBUG
                        MessageBox( Dx::Get()->GetMainHwnd(), (*it)->tFileName, _T("テクスチャエラー") , MB_OK );	
                    #endif
                    }
            break;
            }
        }
        ++it;
    }
}

//------------------------------------------
// テクスチャの解放
//------------------------------------------
// (i/o)    ppFile :解放するテクスチャクラス
//------------------------------------------
void FileLoader::DeleteTex( Tex **ppFile )
{
    if( !(*ppFile) ) return; //指定ファイルがロードされていなかったら戻る
    //検索削除
    std::list<Tex*>::iterator it = ListTex.begin();
    while( it != ListTex.end() )
    {
        if( *it == *ppFile )
        {
            SAFE_DELETE(*ppFile);
            ListTex.erase(it);
            *ppFile = 0;
            break;
        }
        ++it;
    }
}

//------------------------------------------
// FBX登録
//------------------------------------------
// (i/o)    ppFile          :登録するFBXクラス
// (in)     pC_FileName     :FBX名(直接用)
// (in)     pT_FileName     :FBX名(デバグ用)
//------------------------------------------
void FileLoader::CreateFBX( FBX **ppFile, const s8* pC_FileName, LPCTSTR pT_FileName )
{
#ifdef _DEBUG
    assert( (*ppFile ) == 0 && _T("ポインタを初期化してください") ) ;
#endif
    *ppFile =  new FBX( pC_FileName, pT_FileName );
    ListFBX.push_back(*ppFile);
}


//------------------------------------------
// FBX登録
//------------------------------------------
// (i/o)    ppFile          :登録するFBXクラス
// (in)     pC_FileName     :FBX名(直接用)
// (in)     pT_FileName     :FBX名(デバグ用)
// (in)     UseDebugInfo    :デバグ表示機能の使用
//------------------------------------------
void FileLoader::CreateFBX( FBX **ppFile, const s8* pC_FileName, LPCTSTR pT_FileName, bool UseDebugInfo )
{
#ifdef _DEBUG
    assert( (*ppFile ) == 0 && _T("ポインタを初期化してください") ) ;
#endif
    *ppFile =  new FBX( pC_FileName, pT_FileName, UseDebugInfo );
    ListFBX.push_back(*ppFile);
}

//------------------------------------------
// FBX読み込み
//------------------------------------------
void FileLoader::LoadFBX()
{
    FBXLoader *pLoader	= new FBXLoader;

    std::list<FBX*>::iterator it = ListFBX.begin();
    while( it != ListFBX.end() )
    {
        if( !(*it)->Redy() )
        {
            pLoader->ImportFBXFile( (*it)->sFileName, (*it)->tFileName, (*it)->pData );
            (*it)->Set();
        }
        ++it;
    }

    SAFE_DELETE(pLoader);
}

//------------------------------------------
// FBX解放
//------------------------------------------
// (i/o)    ppFile  :解放するFBXクラス
//------------------------------------------
void FileLoader::DeleteFBX( FBX **ppFile )
{
    if( !(*ppFile) ) return; //指定ファイルがロードされていなかったら戻る
    //検索削除
    std::list<FBX*>::iterator it = ListFBX.begin();
    while( it != ListFBX.end() )
    {
        if( *it == *ppFile )
        {
            SAFE_DELETE(*ppFile);
            ListFBX.erase(it);
            *ppFile = 0;
            break;
        }
        ++it;
    }
}

//------------------------------------------
// サウンド登録
//------------------------------------------
// (i/o)    ppFile      :登録するサウンドクラス
// (in)     pFileName   :サウンド名
//------------------------------------------
void FileLoader::CreateSound( Sound::SoundFile **ppFile, LPTSTR pFileName )
{
#ifdef _DEBUG
    assert( (*ppFile) == 0 && _T("ポインタを初期化してください") );
#endif
    *ppFile =  new Sound::SoundFile( pFileName );
    ListSoundFile.push_back(*ppFile);
}

//------------------------------------------
// サウンド読み込み
//------------------------------------------
// ・現状アーカイブからの読み込み未対応
//------------------------------------------
void FileLoader::LoadSound()
{
    std::list<Sound::SoundFile*>::iterator it = ListSoundFile.begin();
    while( it != ListSoundFile.end() )
    {
        if( !(*it)->Redy() )
        {
            //読み込み
            (*it)->CreateBuffer();
        }
        ++it;
    }
}

//------------------------------------------
// サウンド解放
//------------------------------------------
// (i/o)    ppFile  :解放するサウンドクラス
//------------------------------------------
void FileLoader::DeleteSound( Sound::SoundFile **ppFile )
{
    if( !(*ppFile) ) return; //指定ファイルがロードされていなかったら戻る
    //検索削除
    std::list<Sound::SoundFile*>::iterator it = ListSoundFile.begin();
    while( it != ListSoundFile.end() )
    {
        if( *it == *ppFile )
        {
            SAFE_DELETE(*ppFile);
            ListSoundFile.erase(it);
            *ppFile = 0;
            break;
        }
        ++it;
    }
}

//------------------------------------------
// ストリーミング登録
//------------------------------------------
// (i/o)    ppFile      :登録するストリームクラス
// (in)     pFileName   :ストリーム名
//------------------------------------------
void FileLoader::CreateStream( Sound::SoundStream **ppFile, LPTSTR pFileName )
{
#ifdef _DEBUG
    assert( (*ppFile) == 0 && _T("ポインタを初期化してください") ) ;
#endif
    *ppFile =  new Sound::SoundStream(pFileName);
    ListSoundStream.push_back(*ppFile);
}

//------------------------------------------
// ストリーミング読み込み
//------------------------------------------
// ・現状アーカイブからの読み込み未対応
//------------------------------------------
void FileLoader::LoadStream()
{
    std::list<Sound::SoundStream*>::iterator it = ListSoundStream.begin();
    while( it != ListSoundStream.end() )
    {
        if( !(*it)->Redy() )
        {
            //読み込み
            (*it)->SetStream();
        }
        ++it;
    }
}

//------------------------------------------
// ストリーミング解放
//------------------------------------------
// (i/o) ppFile :解放するストリームクラス
//------------------------------------------
void FileLoader::DeleteStream( Sound::SoundStream **ppFile )
{
    if( !(*ppFile) ) return; //指定ファイルがロードされていなかったら戻る
    //検索削除
    std::list<Sound::SoundStream*>::iterator it = ListSoundStream.begin();
    while( it != ListSoundStream.end() )
    {
        if( *it == *ppFile)
        {
            SAFE_DELETE(*ppFile);
            ListSoundStream.erase(it);
            *ppFile = 0;
            break;
        }
        ++it;
    }
}

//------------------------------------------
// LuaState登録
//------------------------------------------
// ・クラス作成、登録
//------------------------------------------
// (i/o)    ppFile  :登録するLuaクラス
//------------------------------------------
void FileLoader::CreateLuaState( LuaState **ppFile )
{
#ifdef _DEBUG
    assert( (*ppFile ) == 0 && _T("ポインタを初期化してください") ) ;
#endif
    *ppFile =  new LuaState;
    ListLua.push_back(*ppFile);
}

//------------------------------------------
// LuaState読み込み
//------------------------------------------
// ・メンバ変数State作成
//------------------------------------------
void FileLoader::LoadLuaState()
{
    std::map< std::string, FileInfo_t >::iterator ItMap;
    std::list<LuaState*>::iterator it1 = ListLua.begin();
    while( it1 != ListLua.end() )
    {
        if( !(*it1)->Redy() )
        {
            std::list<LuaState::LuaFileName>::iterator it2 = (*it1)->FileName.begin();
            while( it2 != (*it1)->FileName.end() )
            {
                switch(UsingArchive)
                {
                //===アーカイブ使用時===========
                    case true:
                        //検索
                        ItMap = FileTable.find( (*it2).sFileName );
                        if( ItMap != FileTable.end() )
                        {
                            //データ読み込み
                            s8 *pData = 0;
                            FileInfo_t tmp = ItMap->second;
                            Read( tmp, &pData );
                        
                            //ステート作成
                            if( !(*it1)->pState )
                                (*it1)->pState = luaL_newstate();
                        
                            //スクリプト読み込み
                            if( luaL_loadbuffer( (*it1)->pState, pData, tmp.Size, (*it2).sFileName ) || lua_pcall( (*it1)->pState, 0, 0, 0) )
                            {
                            #ifdef _DEBUG
                                (*it1)->PrintStack();
                                if( !(*it1)->pState ) lua_close((*it1)->pState);
                                MessageBox( Dx::Get()->GetMainHwnd(), (*it2).tFileName , _T("スクリプトエラー") , MB_OK );
                                Dx::Get()->SystemError(false);
                            #else
                                if( !(*it1)->pState ) lua_close((*it1)->pState);
                                MessageBox( Dx::Get()->GetMainHwnd(), (*it2).tFileName, _T("スクリプトエラー") , MB_OK );
                                Dx::Get()->SystemError(false);
                            #endif
                            }
                            SAFE_DELETE_ARRAY(pData);
                        }
                        else
                        {
                        #ifdef _DEBUG
                            MessageBox( Dx::Get()->GetMainHwnd(), (*it2).tFileName , _T("ファイルが存在しません") , MB_OK );
                        #else
                            MessageBox( Dx::Get()->GetMainHwnd(), (*it2).tFileName , _T("スクリプトエラー") , MB_OK );
                            Dx::Get()->SystemError(false);
                        #endif
                        }
                break;
                //===直接読み込み=============
                    case false:
                        //ステート作成
                        if( !(*it1)->pState )
                            (*it1)->pState = luaL_newstate();
                        //スクリプト読み込み
                        if( luaL_dofile( (*it1)->pState, (*it2).sFileName ) ) 
                        {
                        #ifdef _DEBUG
                            (*it1)->PrintStack();
                            MessageBox( Dx::Get()->GetMainHwnd(), (*it2).tFileName, _T("スクリプトエラー") , MB_OK );
                            Dx::Get()->SystemError(false);
                        #endif
                            if( !(*it1)->pState ) lua_close((*it1)->pState);
                        }
                break;
                }
                ++it2;
            }
        }
        ++it1;
    }
}

//------------------------------------------
// LuaState解放
//------------------------------------------
// (i/o)    ppFile  :解放するサウンドクラス
//------------------------------------------
void FileLoader::DeleteLuaState( LuaState **ppFile )
{
    if( !(*ppFile) ) return; //指定ファイルがロードされていなかったら戻る
    //検索削除
    std::list<LuaState*>::iterator it = ListLua.begin();
    while( it != ListLua.end() )
    {
        if( *it == *ppFile )
        {
            SAFE_DELETE(*ppFile);
            ListLua.erase(it);
            *ppFile = 0;
            break;
        }
        ++it;
    }
}

//------------------------------------------
// ローディング
//------------------------------------------
void FileLoader::LoadALL()
{
    LoadLuaState();
    LoadTex();
    LoadFBX();
    LoadSound();
    LoadStream();
}

//------------------------------------------
//外部ファイルから安全にs32を取り出す
//------------------------------------------
//・アライメント問題の回避
//・リトルエンディアンで格納されてるので直す
//------------------------------------------
// (in)     pInput  :s32を取得する文字
// (out)    s32     :取得したs32型
//------------------------------------------
s32 FileLoader::GetInt( std::ifstream *pInput )
{
    u8 str[4];
    pInput->read( reinterpret_cast<s8*> ( str ), 4 );
    s32 tmp = str[0];
    tmp |= ( str[1] << 8 );
    tmp |= ( str[2] << 16 );
    tmp |= ( str[3] << 24 );
    return tmp;
}

//---------------------------------------------
//安全にs32を外部ファイルから書き出す
//---------------------------------------------
// 書き込み(リトルエンディアンに変換)
//---------------------------------------------
// (in) pO  :書き込み用ストリーム
// (in) a   :変換する要素
//---------------------------------------------
void FileLoader::WriteInt( std::ofstream *pO, s32 a )
{
    s8 str[4];
    str[0] = static_cast< s8 >( ( a & 0x000000ff ) >> 0 );
    str[1] = static_cast< s8 >( ( a & 0x0000ff00 ) >> 8 );
    str[2] = static_cast< s8 >( ( a & 0x00ff0000 ) >> 16 );
    str[3] = static_cast< s8 >( ( a & 0xff000000 ) >> 24 );
    pO->write( str, 4);
}

//------------------------------------------
// private:データの読み込み
//------------------------------------------
// (in)     Info    :ファイル情報
// (i/o)    ppData  :格納先
//------------------------------------------
void FileLoader::Read( const FileInfo_t &Info, s8 **ppData )
{
    std::ifstream File( "sys.bin", std::ifstream::binary );
    File.seekg( Info.Position, std::ifstream::beg );
    *ppData = new s8[Info.Size];
    File.read( *ppData, Info.Size );
}

}