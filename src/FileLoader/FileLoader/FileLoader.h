//=======================================================
// ファイルローダー
// ------------------------------------------------------
// アーカイブ内データ関連のロード管理を行う
//=======================================================
#ifndef Include_FileLoader_H
#define Include_FileLoader_H

//====<インクルード>==================
#include <string>
#include <map>
#include <list>
#include "FBX.h"

//====<前方宣言>======================
namespace FileLoader{ namespace FileLoaderDef{ class Tex; }}
namespace FileLoader{ namespace FileLoaderDef{ class LuaState; }}
namespace FileLoader{ namespace FBXLoader{ class FBXLoader; }}
namespace Sound{ class SoundFile; }
namespace Sound{ class SoundStream; }

namespace FileLoader{

// ------------------------------------------
// クラス:ファイルローダー
// ------------------------------------------
// ・シングルトンクラス
// ------------------------------------------
class FileLoader{
//=====定義==========================
private:
    //ファイル情報格納用
    typedef struct FileInfo_t{
        s32 Position;   //ファイル位置
        s32 Size;       //ファイルサイズ
    }FileInfo_t;

//=====名前空間======================
private:
    typedef FileLoaderDef::LuaState LuaState;
    typedef FileLoaderDef::Tex      Tex;
    typedef FBXLoader::FBX          FBX;
    typedef Sound::SoundFile        SoundFile;
    typedef Sound::SoundStream      SoundStream;
    typedef FBXLoader::FBXLoader    FBXLoader;

//=====構築子：消去子=================
public:
    //!クラスへのアクセス手段
    static FileLoader *Get()
    {
        static FileLoader instance;
        return &instance;
    }

private:
    //!コンストラクタ<使用不可>
    FileLoader():UsingArchive(false){}
    FileLoader( const FileLoader &src ){}
    FileLoader &operator=( const FileLoader &src ){}
    ~FileLoader(){};

//=====変数==========================
private:
    std::map<std::string, FileInfo_t>   FileTable;                              //ファイルテーブル
    std::list<Tex*>                     ListTex;                                //テクスチャ管理リスト
    std::list<FBX*>                     ListFBX;                                //FBX管理リスト
    std::list<SoundFile*>               ListSoundFile;                          //サウンド管理リスト
    std::list<SoundStream*>             ListSoundStream;                        //サウンドストリーム管理リスト
    std::list<LuaState*>                ListLua;                                //LuaState管理リスト
    bool                                UsingArchive;                           //アーカイブから読み込むかのフラグ

//=====関数==========================
private:
    //内部機能
    void Read( const FileInfo_t &Info, s8 **ppData );                           //データの読み込み

public:
    //基本設定
    void SetLoadSystem( bool Flag );                                            //リソース読み込み方法の設定

    //ファイルテーブルの作成
    bool CreateFileTable();                                                     //アーカイブ内のファイル一覧表作成

    //!オブジェクト登録・解放
    void CreateTex( Tex **ppFile, const s8 *pC_FileName, LPCTSTR pT_FileName ); //テクスチャ登録
    void LoadTex();                                                             //テクスチャ読み込み
    void DeleteTex( Tex **ppFile );                                             //テクスチャ解放

    void CreateFBX( FBX **ppFile, const s8 *pC_FileName, LPCTSTR pT_FileName ); //FBX登録
    void CreateFBX( FBX **ppFile, const s8 *pC_FileName, LPCTSTR pT_FileName,   //FBX登録
                    bool UseDebugInfo );
    void LoadFBX();                                                             //FBX読み込み
    void DeleteFBX( FBX **ppFile );                                             //FBX解放

    void CreateSound( SoundFile **ppFile, LPTSTR pFileName );                   //サウンド登録
    void LoadSound();                                                           //サウンド読み込み
    void DeleteSound( SoundFile **ppFile );                                     //サウンド解放

    void CreateStream( SoundStream **ppFile, LPTSTR pFileName );                //サウンドストリーミング登録
    void LoadStream();                                                          //サウンドストリーミング読み込み
    void DeleteStream( SoundStream **ppFile );                                  //サウンドストリーミング解放

    void CreateLuaState( LuaState **ppFile );                                   //LuaState登録
    void LoadLuaState();                                                        //Luaの読み込み
    void DeleteLuaState( LuaState **ppFile );                                   //LuaState解放

    //!ローディング
    void LoadALL();                                                             //リソースの一括読み込み

    //!アライメント対応
    s32 GetInt( std::ifstream *pInput );                                        //外部ファイルから安全にs32を取り出す
    void WriteInt( std::ofstream *pO, s32 a );                                  //安全にs32を外部ファイルから書き出す
};

}
#endif
