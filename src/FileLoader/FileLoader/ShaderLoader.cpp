//+****************************************************************
//| シェーダーローダー
//+****************************************************************

//====<インクルード>=================================
#include "Lib.h"
#include "ShaderLoader.h"

namespace FileLoader{

//--------------------------------------------------
// シェーダーの読み込み(オブジェクトファイル)
//--------------------------------------------------
// オブジェクトから読み込み・デバイスロスト管理への登録
//--------------------------------------------------
// (in)     ObjFileName     :読み込むファイル名
// (i/o)    pData           :読み込み先
//--------------------------------------------------
bool ShaderLoader::LoadFromObject( LPCTSTR ObjFileName, System::Com_ptr<ID3DXEffect> *pData )
{
#ifdef _DEBUG
    if( !pData )
        return false;
#endif
    if( FAILED(D3DXCreateEffectFromFile( Dx::Get()->GetD3DDevice(), ObjFileName, 0, 0,
                                        D3DXSHADER_SKIPVALIDATION, 0, pData->ToCreator(), 0) ) )
    {
        return false;
    }

    //ロスト管理登録
    System::Smart_ptr<System::LostHLSL_File> kk( new System::LostHLSL_File );
    kk->RegisterResource(*pData);
    System::LostDevice *pDev = System::LostDevice::Get();
    pDev->RegisterResourse(kk);
    pDev = 0;

    return true;
}

//--------------------------------------------------
// シェーダーの読み込み(ヘッダファイル)
//--------------------------------------------------
// ヘッダファイルから読み込み・デバイスロスト管理への登録
//--------------------------------------------------
// (in)     pHeadNam    :オブジェクト・ヘッダ名
// (in)     Size        :オブジェクト・ヘッダサイズ
// (i/o)    pData       :読み込み先
//--------------------------------------------------
bool ShaderLoader::LoadFromHeader( const BYTE *pHeadName, size_t &Size, System::Com_ptr<ID3DXEffect> *pData )
{
#ifdef _DEBUG
    if( !pData )
        return false;
#endif

    if( FAILED ( D3DXCreateEffect( Dx::Get()->GetD3DDevice(), pHeadName, Size, 0, 0,
                                D3DXSHADER_SKIPVALIDATION, 0, pData->ToCreator(), 0 ) ) )
    {
        return false;
    }

    //ロスト管理登録
    System::Smart_ptr<System::LostHLSL_File> kk( new System::LostHLSL_File );
    kk->RegisterResource(*pData);
    System::LostDevice *pDev = System::LostDevice::Get();
    pDev->RegisterResourse(kk);

    return true;
}

}