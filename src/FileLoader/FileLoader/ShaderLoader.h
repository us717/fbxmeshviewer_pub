//=======================================================
// シェーダーローダー
// ------------------------------------------------------
// シェーダの登録
// ロストデバイス用の登録を行う
//=======================================================
#ifndef Include_ShaderLoader_H
#define Include_ShaderLoader_H

//====<マクロ定義>======================
//#define SH_OBJ        //ヘッダから読み込む場合はずす

namespace FileLoader{

// ------------------------------------------
// クラス:シェーダーローダー
// ------------------------------------------
// ・シングルトンクラス
// ・自動でメモリ管理を行ってくれるので、登録
// ・しか用意していない
// ------------------------------------------
class ShaderLoader{

//=====構築子：消去子=================
public:
    //!クラスへのアクセス手段
    static ShaderLoader &Get()
    {
        static ShaderLoader instance;
        return instance;
    }

private:
    //!コンストラクタ<使用不可>
    ShaderLoader(){}
    ShaderLoader( const ShaderLoader &src ){}
    ShaderLoader &operator=( const ShaderLoader &src ){}
    ~ShaderLoader(){};

//=====関数==========================
public:
    //オブジェクトファイルから読み込み
    bool LoadFromObject( LPCTSTR ObjFileName, System::Com_ptr<ID3DXEffect> *pData );

    //ヘッダーファイルから読み込み
    bool LoadFromHeader( const BYTE *pHeadName, size_t &Size, System::Com_ptr<ID3DXEffect> *pData );
};

}

#endif
