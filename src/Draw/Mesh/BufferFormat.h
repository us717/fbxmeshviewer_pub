//=======================================================
// 頂点バッファ作成用フォーマット
//=======================================================
#ifndef Include_BufferFormat_H
#define Include_BufferFormat_H

//====<前方宣言>=====================================
namespace FileLoader{ namespace FBXLoader{ class UVSet; }}
namespace FileLoader{ namespace FBXLoader{ struct BoneWeight_t; typedef struct BoneWeight_t BoneWeight_t; }}

namespace Draw{
namespace Mesh{

// --------------------------------------------------------
// クラス:頂点バッファ作成用フォーマット
// --------------------------------------------------------
class BufferFormat{
//=====定義===========================
public:
    //!インデックスウェイト(ウェイト番号)
    typedef struct IndexWeightDef_t{
        u8 IndexWeight[4];
    }IndexWeightDef_t;
    
//=====構築子：消去子==================
private:
    //!コピー禁止
    void operator = ( const BufferFormat &src ){}
    BufferFormat( const BufferFormat &src ){}

public:
    //!コンストラクタ・デストラクタ
    BufferFormat();
    ~BufferFormat();

//====名前空間========================
private:
    typedef FileLoader::FBXLoader::UVSet        UVSet;
    typedef FileLoader::FBXLoader::BoneWeight_t BoneWeight_t;

//=====変数===========================
//=====関数===========================
public:
    static bool CreateOffsetBuf( const u32 &VertexNum, const VEC3 *pData,       //頂点・法線
                                    LPDIRECT3DVERTEXBUFFER9 *ppBuf );
    static bool CreateColorBuf( const u32 &VertexNum, const D3DCOLOR *pData,    //カラー
                                    LPDIRECT3DVERTEXBUFFER9 *ppBuf );

    static bool CreateUVBuf( const u32 &VertexNum, UVSet *pData,                //UV
                                LPDIRECT3DVERTEXBUFFER9 *ppBuf );

    static bool CreateWeightBuf( const u32 &VertexNum, BoneWeight_t *pData,     //ウェイト
                                    LPDIRECT3DVERTEXBUFFER9 *ppBuf );

    static bool CreateWeightIndexBuf( const u32 &VertexNum, BoneWeight_t *pData,//ウェイトインデックス
                                        LPDIRECT3DVERTEXBUFFER9 *ppBuf );
};

}}
#endif
