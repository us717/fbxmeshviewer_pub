//=======================================================
// アニメーションコントローラー
//=======================================================
#ifndef Include_AnimController_H
#define Include_AnimController_H

//====<インクルード>=================================

//====<前方宣言>=====================================
namespace FileLoader{ class ShaderLoader; }
namespace FileLoader{ namespace FBXLoader{ class UVSet; }}
namespace FileLoader{ namespace FBXLoader{ struct BoneWeight_t; typedef struct BoneWeight_t BoneWeight_t; }}
namespace FileLoader{ namespace FBXLoader{ class FBXData; }}

namespace Draw{
namespace Mesh{

// -------------------------------------------------
// クラス:アニメーションコントローラー
// -------------------------------------------------
class AnimController{
//=====定義===========================
public:
    //!インデックスウェイト(ウェイト番号)
    typedef struct IndexWeightDef_t{
        u8 IndexWeight[4];
    }IndexWeightDef_t;

    //!アニメハンドル
    typedef struct AnimHandle{
        bool Init;              //初期化フラグ
        u32  TakeNum;           //テイク番号
        u32  FrameNum;          //フレーム番号
        u32  PreFrameNum;       //前のフレーム番号
        u32  MaxAnimFrame;      //アニメの終わりとなるフレーム番号
        u32  LoopCount;         //ループ回数
        f32  NextUpdate;        //次にフレームを更新するメインループの回数
        AnimHandle():Init(0),TakeNum(0),FrameNum(0),PreFrameNum(0),
                        MaxAnimFrame(0),LoopCount(0),NextUpdate(0){}
    }AnimHandle;

//====名前空間========================
protected:
    typedef FileLoader::ShaderLoader                ShaderLoader;
    typedef FileLoader::FBXLoader::UVSet            UVSet;
    typedef FileLoader::FBXLoader::BoneWeight_t     BoneWeight_t;
    typedef FileLoader::FBXLoader::FBXData          FBXData;

//=====構築子：消去子==================
private:
    //!コピー禁止
    void operator = ( const AnimController &src ){}
    AnimController( const AnimController &src ){}

public:
    //!コンストラクタ・デストラクタ
    AnimController();
    ~AnimController();

//=====変数===========================
    //!アニメーショ管理
    MATRIX      *pAnimMat[3];   //アニメーション行列
    f32         AnimSpeed;      //描画速度
    f32         BlendRate;      //変更率
    f32         BlendWeight;    //現在の変更率
    bool        FlagPlay;       //フラグ:再生するかどうか
    bool        FlagAnimBlend;  //フラグ:ブレンドするかどうか
    bool        CurHandle;      //現在使用しているアニメハンドル
    AnimHandle  Handle[2];      //アニメーション個別操作

//=====関数===========================
private:
    //!アニメーション管理
    void AnimeBlend( const u32 &BoneNum );                                  //アニメーション補間
    void AnimCaluculate( FBXData *pData, MATRIX *pInvMat, bool HandleNum ); //アニメーション行列算出

public:
    bool    CreateInvMat( const FBXData *pData, MATRIX *pInvMat );          //逆行列の作成
    bool    CreateAnimMemory( const u32 &BoneNum );                         //アニメ行列メモリ確保
    bool    DeleteAnimMemory();                                             //アニメメモリーの削除
    bool    AnimUpdate( FBXData *pData, MATRIX *pInvMat );                  //更新
    MATRIX* GetAnimMat();                                                   //アニメーション行列取得
    void    SetAnimTake( const u32 &TakeNum );                              //再生するテイク指定
    void    SetAnimTake( const u32 &TakeNum, const f32 &BlendRate );        //再生するテイク指定(ブレンド使用)
    void    SetAnimSpeed( const f32 &Speed );                               //速度指定
    void    AnimPlay();                                                     //再生
    void    AnimStop();                                                     //停止
    void    AnimReset();                                                    //リセット
};

}}
#endif
