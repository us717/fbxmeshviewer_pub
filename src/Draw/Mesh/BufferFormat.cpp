//+****************************************************************
//| 頂点バッファ作成用フォーマット
//+****************************************************************
#include "Lib.h"
#include "FBXData.h"
#include "BufferFormat.h"

namespace Draw{
namespace Mesh{

// --------------------------------------------------------
// バッファ作成: 頂点オフセット・法線 (VEC3)
// --------------------------------------------------------
// (in)     VertexNum   :頂点数
// (i/o)    pData       :頂点データ
// (i/o)    ppBuf       :バッファ用インターフェイス
// (out)    bool        :エラーフラグ
// --------------------------------------------------------
bool BufferFormat::CreateOffsetBuf( const u32 &VertexNum, const VEC3 *pData,
                                     LPDIRECT3DVERTEXBUFFER9 *ppBuf )
{
#ifdef _DEBUG
    if( pData == 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データがありません"), _T("頂点オフセット") , MB_OK );
        return false;
    }
    if( *ppBuf != 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("初期化してください"), _T("頂点オフセット") , MB_OK );
        return false;
    }
#endif
    //!オフセットバッファ作成
    size_t VertexSize = sizeof(VEC3) * VertexNum;                       //バッファサイズ
    Dx::Get()->GetD3DDevice()->CreateVertexBuffer( VertexSize, 0, 0,    //頂点バッファの作成
                                        D3DPOOL_MANAGED, ppBuf, NULL );
    VEC3 *pOffsetBuf = 0;                                               //頂点格納用データ

    (*ppBuf)->Lock( 0, 0, (LPVOID*)&pOffsetBuf, 0 );                    //頂点バッファへ書き込み
    for( u32 i=0 ; i<VertexNum; ++i )
    {
        pOffsetBuf[i].x = pData[i].x;
        pOffsetBuf[i].y = pData[i].y;
        pOffsetBuf[i].z = pData[i].z;
    }
    (*ppBuf)->Unlock();
    pOffsetBuf = 0;
    ppBuf = 0;

    return true;
}

// --------------------------------------------------------
// バッファ作成: 頂点カラー (D3DCOLOR)
// --------------------------------------------------------
// (in)     VertexNum   :頂点数
// (i/o)    pData       :色データ
// (i/o)    ppBuf       :バッファ用インターフェイス
// (out)    bool        :エラーフラグ
// --------------------------------------------------------
bool BufferFormat::CreateColorBuf( const u32 &VertexNum, const D3DCOLOR *pData,
                                     LPDIRECT3DVERTEXBUFFER9 *ppBuf )
{
#ifdef _DEBUG
    if( pData == 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データがありません"), _T("頂点オフセット") , MB_OK );
        return false;
    }
    if( *ppBuf != 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("初期化してください"), _T("頂点オフセット") , MB_OK );
        return false;
    }
#endif
    //!カラーバッファ作成
    size_t VertexSize = sizeof(D3DCOLOR) * VertexNum;                   //バッファサイズ
    Dx::Get()->GetD3DDevice()->CreateVertexBuffer( VertexSize, 0, 0,    //頂点バッファの作成
                                        D3DPOOL_MANAGED, ppBuf, NULL );

    D3DCOLOR *pColorBuf = 0;                                            //頂点格納用データ
    (*ppBuf)->Lock( 0, 0, (LPVOID*)&pColorBuf, 0 );                     //頂点バッファへ書き込み
    for( u32 i=0 ; i<VertexNum; ++i )
    {
        pColorBuf[i] = pData[i];
    }
    (*ppBuf)->Unlock();
    pColorBuf = 0;
    ppBuf = 0;

    return true;
}

// --------------------------------------------------------
// バッファ作成: UV (VEC2)
// --------------------------------------------------------
// (in)     VertexNum   :頂点数
// (i/o)    pData       :UVデータへのポインタ
// (i/o)    ppBuf       :バッファ用インターフェイス
// (out)    bool        :エラーフラグ
// --------------------------------------------------------
bool BufferFormat::CreateUVBuf( const u32 &VertexNum, UVSet *pData,
                                 LPDIRECT3DVERTEXBUFFER9 *ppBuf )
{
#ifdef _DEBUG
    if( pData == 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データがありません"), _T("頂点オフセット") , MB_OK );
        return false;
    }
    if( *ppBuf != 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("初期化してください"), _T("頂点オフセット") , MB_OK );
        return false;
    }
#endif
    VEC2 *pUVBuf = 0;
    size_t UVSize = sizeof(VEC2) * VertexNum;

    Dx::Get()->GetD3DDevice()->CreateVertexBuffer( UVSize, 0,0, 
                                    D3DPOOL_MANAGED, ppBuf, NULL);
    (*ppBuf)->Lock( 0, 0,(LPVOID*)&pUVBuf, 0 );
        
    for( u32 i=0 ; i<VertexNum ; ++i )
    {
        pUVBuf[i].x = pData->pUVBuf[i].x;
        pUVBuf[i].y = pData->pUVBuf[i].y;
    }

    (*ppBuf)->Unlock();
    pUVBuf = 0;

    return true;
}

// --------------------------------------------------------
// バッファ作成: ウェイト (VEC4)
// --------------------------------------------------------
// (in)  VertexNum  :頂点数
// (i/o) pData      :ウェイト情報へのポインタ
// (i/o) ppBuf      :バッファ用インターフェイス
// (out) bool       :エラーフラグ
// --------------------------------------------------------
bool BufferFormat::CreateWeightBuf( const u32 &VertexNum, BoneWeight_t *pData, LPDIRECT3DVERTEXBUFFER9 *ppBuf )
{
#ifdef _DEBUG
    if( pData == 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データがありません"), _T("頂点オフセット") , MB_OK );
        return false;
    }
    if( *ppBuf != 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("初期化してください"), _T("頂点オフセット") , MB_OK );
        return false;
    }
#endif
    size_t WeightSize = sizeof(VEC4) * VertexNum;
    Dx::Get()->GetD3DDevice()->CreateVertexBuffer( WeightSize, 0, 0,
                                        D3DPOOL_MANAGED, ppBuf, NULL );
    VEC4 *pWeightBuf;
    (*ppBuf)->Lock( 0, 0, (LPVOID*)&pWeightBuf, 0 );
    for( u32 i=0 ; i<VertexNum ; ++i )
    {
        pWeightBuf[i].x = pData[i].Weight.x;
        pWeightBuf[i].y = pData[i].Weight.y;
        pWeightBuf[i].z = pData[i].Weight.z;
        pWeightBuf[i].w = pData[i].Weight.w;
    }
    (*ppBuf)->Unlock();
    pWeightBuf = 0;

    return true;
}

// --------------------------------------------------------
// バッファ作成:ウェイトインデックス (IndexWeightDef_t)
// --------------------------------------------------------
// (in)     VertexNum  :頂点数
// (i/o)    pData      :ウェイト情報へのポインタ
// (i/o)    ppBuf      :バッファ用インターフェイス
// (out)    bool       :エラーフラグ
// --------------------------------------------------------
bool BufferFormat::CreateWeightIndexBuf( const u32 &VertexNum, BoneWeight_t *pData, LPDIRECT3DVERTEXBUFFER9 *ppBuf )
{
#ifdef _DEBUG
    if( pData == 0 ){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データがありません"), _T("頂点オフセット") , MB_OK );
        return false;
    }
    if( *ppBuf != 0 ){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("初期化してください"), _T("頂点オフセット") , MB_OK );
        return false;
    }
#endif
    size_t IndexWeightSize = sizeof( IndexWeightDef_t ) * VertexNum;
    Dx::Get()->GetD3DDevice()->CreateVertexBuffer( IndexWeightSize, 0, 0,
                                        D3DPOOL_MANAGED, ppBuf, NULL );
    IndexWeightDef_t *pIndexWeightBuf;
    (*ppBuf)->Lock( 0, 0, (LPVOID*)&pIndexWeightBuf, 0 );
    for( u32 i=0 ; i<VertexNum; ++i )
    { 
        pIndexWeightBuf[i].IndexWeight[0] = pData[i].ID[0];
        pIndexWeightBuf[i].IndexWeight[1] = pData[i].ID[1];
        pIndexWeightBuf[i].IndexWeight[2] = pData[i].ID[2];
        pIndexWeightBuf[i].IndexWeight[3] = pData[i].ID[3];
    }
    (*ppBuf)->Unlock();
    pIndexWeightBuf=0;

    return true;
}

}}