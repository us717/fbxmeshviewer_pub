//=======================================================
// メッシュ要素:変換行列
//=======================================================
#ifndef Include_EleCoordData_H
#define Include_EleCoordData_H

//====<インクルード>=================================

//====<前方宣言>=====================================
namespace FileLoader{ namespace FileLoaderDef{ class LuaState; }}

namespace Draw{
namespace Mesh{

// ------------------------------------------------
// クラス:座標系データ
//-------------------------------------------------
class CoordData{
//=====定義===========================
public:
    //メッシュタイプ
    typedef enum MeshType_t{
        NONE = 0,
        CHARA,
        BLOCK,
    }MeshType_t;

//=====構築子：消去子==================
public:
    CoordData();
    ~CoordData(){}

//=====名前空間=======================
private:
    typedef FileLoader::FileLoaderDef::LuaState LuaState;

//=====変数===========================
public:
    //メッシュ要素
    MeshType_t  MeshType;       //メッシュタイプ
    VEC3        Scale;          //スケール
    VEC3        Rotate;         //回転
    VEC3        Offset;         //オフセット
    MATRIX      WorldMat[2];    //変換行列(前フレ、現フレ)
    bool        CurMat;         //現在のフレームを示す配列番号
    bool        FlagUpdate;     //フラグ:更新

//=====関数===========================
    void InitFromLua( const LuaState &Lua, u32 StartStack ); //Luaから読み込み、変換行列を作成する
    void ChangeCurMat(){ CurMat = !CurMat; }                 //現在の行列を前のフレームの行列にする
    MATRIX* GetCurMat(){ return &WorldMat[CurMat]; }         //現在の変換行列取得
    MATRIX* GetPreMat(){ return &WorldMat[!CurMat]; }        //前のフレームの変換行列取得
};


// ------------------------------------------------
// struct: 座標系、コリジョン枠(後で追加)
//-------------------------------------------------
typedef struct CoordAndFrame_t{
    CoordData *pCoord; //座標系データ

    CoordAndFrame_t():pCoord(0){}
}CoordAndFrame_t;

}}

#endif
