//+****************************************************************
//| メッシュに使用する要素
//+****************************************************************

//====<インクルード>==================================
#include "Lib.h"
#include <lua.hpp>
#include "LuaState.h"
#include "EleCoordData.h"

namespace Draw{
namespace Mesh{

// ------------------------------------------------
// コンストラクタ
//-------------------------------------------------
CoordData::CoordData()
    :FlagUpdate(false),CurMat(false)
{
    Scale.x     = 0.0f;     Scale.y  = 0.0f;    Scale.z  = 0.0f;
    Rotate.x    = 0.0f;     Rotate.y = 0.0f;    Rotate.z = 0.0f;
    Offset.x    = 0.0f;     Offset.y = 0.0f;    Offset.z = 0.0f;
    D3DXMatrixIdentity(&WorldMat[0]);
    D3DXMatrixIdentity(&WorldMat[1]);
}

// ------------------------------------------------
// Luaから読み込み、変換行列を作成する
//-------------------------------------------------
// (in) Lua             :スタック取得済みのState
// (in) StartStack      :スタック読み取り開始位置(+表記)
//-------------------------------------------------
void CoordData::InitFromLua( const LuaState &Lua, u32 StartStack )
{
#ifdef _DEBUG
    assert( &Lua != 0 );
    assert( StartStack != 0);
#endif
    D3DXMatrixIdentity(GetCurMat());
    Scale.x = static_cast<f32>( lua_tonumber( Lua.Get(), StartStack ) );
    Scale.y = static_cast<f32>( lua_tonumber( Lua.Get(), (StartStack + 1) ) );
    Scale.z = static_cast<f32>( lua_tonumber( Lua.Get(), (StartStack + 2) ) );
    Rotate.x = static_cast<f32>( lua_tonumber( Lua.Get(), (StartStack + 3) ) );
    Rotate.y = static_cast<f32>( lua_tonumber( Lua.Get(), (StartStack + 4) ) );
    Rotate.z = static_cast<f32>( lua_tonumber( Lua.Get(), (StartStack + 5) ) );
    Offset.x = static_cast<f32>( lua_tonumber( Lua.Get(), (StartStack + 6) ) );
    Offset.y = static_cast<f32>( lua_tonumber( Lua.Get(), (StartStack + 7) ) );
    Offset.z = static_cast<f32>( lua_tonumber( Lua.Get(), (StartStack + 8) ) );

    //!回転角の設定
    if( Rotate.x != 0.0f || Rotate.y != 0.0f || Rotate.z != 0.0f )
    D3DXMatrixRotationYawPitchRoll( &WorldMat[CurMat], D3DXToRadian(Rotate.y), D3DXToRadian(Rotate.x), D3DXToRadian(Rotate.z) );

    //!スケール
    WorldMat[CurMat]._11 = Scale.x * WorldMat[CurMat]._11;
    WorldMat[CurMat]._12 = Scale.x * WorldMat[CurMat]._12;
    WorldMat[CurMat]._13 = Scale.x * WorldMat[CurMat]._13;

    WorldMat[CurMat]._21 = Scale.y * WorldMat[CurMat]._21;
    WorldMat[CurMat]._22 = Scale.y * WorldMat[CurMat]._22;
    WorldMat[CurMat]._23 = Scale.y * WorldMat[CurMat]._23;

    WorldMat[CurMat]._31 = Scale.z * WorldMat[CurMat]._31;
    WorldMat[CurMat]._32 = Scale.z * WorldMat[CurMat]._32;
    WorldMat[CurMat]._33 = Scale.z * WorldMat[CurMat]._33;

    //!オフセット
    WorldMat[CurMat]._41 = Offset.x;
    WorldMat[CurMat]._42 = Offset.y;
    WorldMat[CurMat]._43 = Offset.z;
    FlagUpdate = true;
}

}}