//+****************************************************************
//| アニメーションコントローラー
//+****************************************************************

//====<インクルード>==================================
#include "Lib.h"
#include "AnimController.h"
#include "FBXData.h"

namespace Draw{
namespace Mesh{

// --------------------------------------------------------
// コンストラクタ
// --------------------------------------------------------
AnimController::AnimController():

    AnimSpeed(2.4f),
    BlendRate(0.0f),BlendWeight(0.0f),FlagPlay(true),FlagAnimBlend(false),CurHandle(0)
{
    for( s32 i=0 ; i<3 ; ++i ){pAnimMat[i] = 0;}
}

// --------------------------------------------------------
// デストラクタ
// --------------------------------------------------------
AnimController::~AnimController()
{
    for( u32 i=0 ; i<3 ; ++i )
    {
        SAFE_DELETE_ARRAY(pAnimMat[i]);
    }
}

// --------------------------------------------------------
// アニメ:逆行列の作成
// --------------------------------------------------------
// (in)     pData       :取得済みのデータへのポインタ
// (i/o)    pInvMat     :確保済み逆行列へのポインタ
// (out)    bool        :エラーフラグ
// --------------------------------------------------------
bool AnimController::CreateInvMat( const FBXData *pData, MATRIX *pInvMat )
{
#ifdef _DEBUG
    if ( pData == 0 ){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データがありません"), _T("アニメ:逆行列") , MB_OK );
        return false;
    }
    if( pInvMat== 0 ){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("メモリを確保して下さい"), _T("アニメ:逆行列") , MB_OK );
        return false;
    }

#endif
    //!逆行列の作成
    for( u32 i=0 ; i<pData->BoneCount ; ++i )
    {
        D3DXMatrixInverse( &pInvMat[i], 0, &pData->pInitPose[i] );
    }
    return true;
}

// --------------------------------------------------------
// アニメ：メモリ確保
// --------------------------------------------------------
// ・アニメ使用のために変換行列のメモリを確保する
// --------------------------------------------------------
// (in)     BoneNum   :ボーンの数
// (out)    bool      :エラーフラグ
// --------------------------------------------------------
bool AnimController::CreateAnimMemory( const u32 &BoneNum )
{
#ifdef _DEBUG
    if( pAnimMat[0] != 0)
    {
        MessageBox( Dx::Get()->GetMainHwnd(), _T("初期化してください"), _T("アニメ：メモリ") , MB_OK );
        return false;
    }
#endif
    
    //!メモリ確保
    for( u32 i=0 ; i<3 ; ++i )
    {
        pAnimMat[i] = new MATRIX[BoneNum];
    }
    return true;
}

// --------------------------------------------------------
// アニメ：メモリ削除
// --------------------------------------------------------
// (out) bool   :フラグ
// --------------------------------------------------------
bool AnimController::DeleteAnimMemory()
{
    for( u32 i=0 ; i<3 ; ++i )
    {
        SAFE_DELETE_ARRAY(pAnimMat[i]);
    }
    return true;
}


// --------------------------------------------------------
// アニメ:速度指定
// --------------------------------------------------------
// ・固定の場合は反映されない
// ・指定速度が小さいほど早くなる
// ・0指定の場合、動かない
// --------------------------------------------------------
// (in) Speed   :描画速度(FPS)
// --------------------------------------------------------
void AnimController::SetAnimSpeed( const f32 &Speed )
{
    AnimSpeed = Speed / 24.0f; //Blenderに合わせておく(24FPS/S)
}

// --------------------------------------------------------
// アニメ:テイク番号指定
// --------------------------------------------------------
// ・固定の場合は反映されない
// ・即時切替
// --------------------------------------------------------
// (in) TakeNum :テイク番号
// --------------------------------------------------------
void AnimController::SetAnimTake( const u32 &TakeNum )
{
    //テイク変更と初期化
    Handle[CurHandle].TakeNum   = TakeNum;
    Handle[CurHandle].FrameNum  = 0;
    Handle[CurHandle].Init      = false;
    FlagAnimBlend               = false;
}

// --------------------------------------------------------
// アニメ:テイク番号指定(ブレンド使用)
// --------------------------------------------------------
// ・固定の場合は反映されない
// ・ブレンドを使用して切り替える
// --------------------------------------------------------
// (in) TakeNum     :テイク番号
// (in) BlendRate   :変更率
// --------------------------------------------------------
void AnimController::SetAnimTake( const u32 &TakeNum, const f32 &BlendRate )
{
    //カレントハンドル変更
    CurHandle = !CurHandle;
    //テイク変更と初期化
    Handle[CurHandle].TakeNum       = TakeNum;
    Handle[CurHandle].FrameNum      = 0;
    Handle[CurHandle].Init          = true;
    this->BlendRate                 = BlendRate;
    BlendWeight                     = 0.0f;
    FlagAnimBlend                   = true;
}

// --------------------------------------------------------
// アニメ:再生
// --------------------------------------------------------
// ・固定の場合は反映されない
// --------------------------------------------------------
void AnimController::AnimPlay()
{
    FlagPlay = true;
}

// --------------------------------------------------------
// アニメ:停止
// --------------------------------------------------------
// ・固定の場合は反映されない
// --------------------------------------------------------
void AnimController::AnimStop()
{
    FlagPlay = false;
}

// --------------------------------------------------------
// アニメ:リセット(初期状態にする)
// --------------------------------------------------------
// ・固定の場合は反映されない
// --------------------------------------------------------
void AnimController::AnimReset()
{
    Handle[CurHandle].Init = false;
}

// --------------------------------------------------------
// アニメ:アニメーション行列算出
// --------------------------------------------------------
// ・固定の場合は使用できない
// ・メンバであるpAnimMatに結果が格納される
// --------------------------------------------------------
// (in) pData           :取得済みのデータへのポインタ
// (in) pInvMat         :逆行列へのポインタ
// (in) HandleNum       :使用ハンドル番号
// --------------------------------------------------------
void AnimController::AnimCaluculate( FBXData *pData, MATRIX *pInvMat, bool HandleNum )
{
//====フレーム更新==============
    //初回時(0フレーム目の処理、初期化処理)
    if( Handle[HandleNum].Init == false )
    {
        for( u32 i=0 ; i<pData->BoneCount ; ++i )
        {
            D3DXMatrixIdentity( &pAnimMat[HandleNum][i] );
            pAnimMat[HandleNum][i] = pInvMat[i] * pData->pFramePose[Handle[HandleNum].TakeNum].pBonePoseMat[0].pBoneMat[i];
        }
        Handle[HandleNum].PreFrameNum = Handle[HandleNum].FrameNum;
        Handle[HandleNum].Init = true;
    }
    
    //通常時
    if( FlagPlay == true && FL_LARGE(AnimSpeed,0.0f))
    {
        f32 tmpLoopCount = static_cast<f32>(Handle[HandleNum].LoopCount);

        //予定カウントとループカウントが等しい
        if( FL_EQUAL( Handle[HandleNum].NextUpdate, tmpLoopCount ) )
        {
            Handle[HandleNum].FrameNum++;                   //フレームを更新する
            Handle[HandleNum].NextUpdate += AnimSpeed;      //次の予定時間を更新
        }
        //予定カウントがループカウントより小さい
        else if( FL_SMALL(Handle[HandleNum].NextUpdate, tmpLoopCount ) )
        {
            while( FL_SMALL(Handle[HandleNum].NextUpdate, tmpLoopCount ) )
            {
                Handle[HandleNum].FrameNum++;               //フレームを更新する
                Handle[HandleNum].NextUpdate += AnimSpeed;  //次のフレーム時間を更新
            }
        }
        Handle[HandleNum].LoopCount++;                      //ループカウントを足していく
    }
    
//====フレーム初期化チェック=======
    //フレーム数が既定より多くなったら初期化
    if( Handle[HandleNum].FrameNum >= pData->pFramePose[Handle[HandleNum].TakeNum].FrameNum )
    {
        Handle[HandleNum].FrameNum = 0;
        Handle[HandleNum].LoopCount = 0;
        Handle[HandleNum].NextUpdate = AnimSpeed;
    }
    
//====ボーン取り出し==============
    if( Handle[HandleNum].FrameNum != Handle[HandleNum].PreFrameNum )
    {
        for( u32 i=0 ; i<pData->BoneCount ; ++i )
        {
            D3DXMatrixIdentity( &pAnimMat[HandleNum][i] );
            pAnimMat[HandleNum][i] = pInvMat[i] * pData->pFramePose[Handle[HandleNum].TakeNum]
                                                    .pBonePoseMat[Handle[HandleNum].FrameNum].pBoneMat[i];
        }
        Handle[HandleNum].PreFrameNum = Handle[HandleNum].FrameNum;
    }
}

// --------------------------------------------------------
// アニメーション補間
// --------------------------------------------------------
// (in) BoneNum     :ボーンの数
// --------------------------------------------------------
void AnimController::AnimeBlend( const u32 &BoneNum )
{
    BlendWeight += BlendRate;   //ブレンド係数を足していく
    
    if( FL_LARGE(BlendWeight, 1.0f) || FL_EQUAL(BlendRate, 0.0f) )
    {
        BlendWeight = 1.0f;     //1.0以上は増えないようにしているする
    }
    
    //!ボーンの数だけアニメーション補間
    for( u32 i=0 ; i<BoneNum ; ++i ) //各要素分解→補間→再構成
    {
        D3DXMatrixIdentity( &pAnimMat[2][i]);
        
        //====オフセット==== :線形補間
        VEC3 OffsetResult;
        OffsetResult.x = pAnimMat[!CurHandle][i]._41 * ( 1.0f - BlendWeight ) + pAnimMat[CurHandle][i]._41 * BlendWeight;
        OffsetResult.y = pAnimMat[!CurHandle][i]._42 * ( 1.0f - BlendWeight ) + pAnimMat[CurHandle][i]._42 * BlendWeight;
        OffsetResult.z = pAnimMat[!CurHandle][i]._43 * ( 1.0f - BlendWeight ) + pAnimMat[CurHandle][i]._43 * BlendWeight;

        //====スケール====== :線形補間
        VEC3 ScResult;
        VEC3 ScPrime;
        VEC3 ScSub;

        //x
        ScPrime.x = pAnimMat[!CurHandle][i]._11; ScPrime.y = pAnimMat[!CurHandle][i]._12; ScPrime.z = pAnimMat[!CurHandle][i]._13;
        ScSub.x   = pAnimMat[CurHandle][i]._11;  ScSub.y   = pAnimMat[CurHandle][i]._12;  ScSub.z   = pAnimMat[CurHandle][i]._13;
        ScResult.x = D3DXVec3Length(&ScPrime) * ( 1.0f - BlendWeight ) + D3DXVec3Length(&ScSub) * BlendWeight;
        
        //y
        ScPrime.x = pAnimMat[!CurHandle][i]._21; ScPrime.y = pAnimMat[!CurHandle][i]._22; ScPrime.z = pAnimMat[!CurHandle][i]._23;
        ScSub.x   = pAnimMat[CurHandle][i]._21;  ScSub.y   = pAnimMat[CurHandle][i]._22;  ScSub.z   = pAnimMat[CurHandle][i]._23;
        ScResult.y = D3DXVec3Length(&ScPrime) * ( 1.0f - BlendWeight ) + D3DXVec3Length(&ScSub) * BlendWeight;

        //z
        ScPrime.x = pAnimMat[!CurHandle][i]._31; ScPrime.y = pAnimMat[!CurHandle][i]._32; ScPrime.z = pAnimMat[!CurHandle][i]._33;
        ScSub.x   = pAnimMat[CurHandle][i]._31;  ScSub.y   = pAnimMat[CurHandle][i]._32;  ScSub.z   = pAnimMat[CurHandle][i]._33;
        ScResult.z = D3DXVec3Length(&ScPrime) * ( 1.0f - BlendWeight ) + D3DXVec3Length(&ScSub) * BlendWeight;

        //====回転=========:変換行列からクオータニオン作成→球面線形補間
    
        //変換行列→クオータニオン
        D3DXQUATERNION QPrim;
        D3DXQUATERNION QSub;
        D3DXQUATERNION QResult;
        D3DXQuaternionRotationMatrix( &QPrim, &pAnimMat[!CurHandle][i] );   //サブ:変換行列→クオータニオン
        D3DXQuaternionRotationMatrix( &QSub,  &pAnimMat[CurHandle][i] );    //メイン:変換行列→クオータニオン
        
        //球面線形補間
        D3DXQuaternionSlerp( &QResult, &QPrim, &QSub, BlendWeight );
        D3DXMatrixRotationQuaternion( &pAnimMat[2][i], &QResult );

        //====変換行列作成===
        //スケール・回転
        pAnimMat[2][i]._11 = ScResult.x * pAnimMat[2][i]._11;
        pAnimMat[2][i]._12 = ScResult.x * pAnimMat[2][i]._12;
        pAnimMat[2][i]._13 = ScResult.x * pAnimMat[2][i]._13;

        pAnimMat[2][i]._21 = ScResult.y * pAnimMat[2][i]._21;
        pAnimMat[2][i]._22 = ScResult.y * pAnimMat[2][i]._22;
        pAnimMat[2][i]._23 = ScResult.y * pAnimMat[2][i]._23;

        pAnimMat[2][i]._31 = ScResult.z * pAnimMat[2][i]._31;
        pAnimMat[2][i]._32 = ScResult.z * pAnimMat[2][i]._32;
        pAnimMat[2][i]._33 = ScResult.z * pAnimMat[2][i]._33;

        //オフセット
        pAnimMat[2][i]._41 = OffsetResult.x;
        pAnimMat[2][i]._42 = OffsetResult.y;
        pAnimMat[2][i]._43 = OffsetResult.z;
    }

    //終了判定
    if( FL_LARGE(BlendWeight,1.0f ) )
    {
        FlagAnimBlend = false;
    }
}

// --------------------------------------------------------
// アニメ:更新
// --------------------------------------------------------
// (in) pData       :取得済みFBXデータへのポインタ
// (in) pInvMat     :取得済み逆行列データへのポインタ
// --------------------------------------------------------
bool AnimController::AnimUpdate( FBXData *pData, MATRIX *pInvMat )
{
#ifdef _DEBUG
    if( pData == 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("FBXデータがありません"), _T("アニメ：更新") , MB_OK );
        return false;
    }
    if( pInvMat == 0){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("Invデータがありません"), _T("アニメ：更新") , MB_OK );
        return false;
    }
    if( Handle[CurHandle].TakeNum > pData->TakeNum ){
        MessageBox( Dx::Get()->GetMainHwnd(), _T("不正なテイク番号"), _T("アニメ：更新") , MB_OK );
        return false;
    }
#endif
    switch(FlagAnimBlend)
    {
        //====通常時============
        case false:
            AnimCaluculate( pData, pInvMat, CurHandle ); //カレントハンドルの変換行列の作成
        break;
        //====テイク変更時=======
        case true:
            AnimCaluculate( pData, pInvMat, CurHandle );
            AnimCaluculate( pData, pInvMat, !CurHandle );
            AnimeBlend( pData->BoneCount );
        break;
    }
    return true;
}

// --------------------------------------------------------
// アニメーション行列取得
// --------------------------------------------------------
// (out) MATRIX*    :アニメーション行列
// --------------------------------------------------------
MATRIX* AnimController::GetAnimMat()
{
#ifdef _DEBUG
    if( pAnimMat[CurHandle] == 0 )
        MessageBox( Dx::Get()->GetMainHwnd(), _T("データがありません"), _T("アニメ：取得") , MB_OK );
#endif
    switch(FlagAnimBlend)
    {
    //====通常(プライム)=====
        case false:
        return pAnimMat[CurHandle]; //カレント指定の変換行列

    //====テイク変更時=======
        case true:
        return pAnimMat[2];         //ブレンドした変換行列
    }
    return 0;
}

}}
