//=======================================================
// スプライト描画
//-------------------------------------------------------
// 動くオブジェクト：要素変更→変換行列再生成を繰り返すこと
//                  つまり、変更→更新→描画
//=======================================================
#ifndef Include_Sprite_H
#define Include_Sprite_H

//=====前方宣言========================
namespace FileLoader{ namespace FileLoaderDef{ class Tex; }}

namespace Draw{
namespace Sprite{

// -------------------------------------------------
// クラス：Sprite
// -------------------------------------------------
class Sprite{

//=====定義===========================
public:
    //!描画データ構造体
    typedef struct DrawData_t{
        f32    ScaleX, ScaleY;   //スケール値
        f32    Rotate;           //回転値<度数>
        f32    OffSetX, OffSetY; //オフセット値
        RECT   rect;             //使用テクスチャ範囲(全描画の時は適当で問題ない)
    }DrawData_t;

//=====構築子：消去子=================
public:
    Sprite();
    Sprite( const s8 *sFileName, LPCTSTR tFileName, const DrawData_t &Src );
    ~Sprite();

//=====変数===========================
private:
    //!描画インターフェイス
    FileLoader::FileLoaderDef::Tex *pTex; //テクスチャ

    //!描画データ
    MATRIX    TFMat;        //背景変換行列
    VEC3      Scale;        //スケール値
    f32       Rotate;       //回転値
    VEC3      Offset;       //オフセット値
    RECT      Rect;         //使用テクスチャ範囲
    VEC3      CenterOffset; //中心座標

    //!内部制御用
    bool      ChangeFlag;   //データ変更確認

//=====関数===========================
private:
    void CalPosCenter();                                             //中心制御点の計算
    void CreateTFMatrix();                                           //変換行列の再生成

public:
    //!登録用関数
    void RegisterFileName( const s8 *sFileName, LPCTSTR tFileName ); //テクスチャの登録
    void RegisterDrawData( const DrawData_t &Src);                   //描画データの登録
    
    //!変更用関数
    void SetRotate( const f32 &Rotate );                             //回転値の設定
    void SetScale ( const f32 &X, const f32 &Y );                    //スケール値の設定
    void SetOffSet( const f32 &X, const f32 &Y );                    //オフセット値の設定
    void SetRect  ( const RECT &Rect );                              //使用範囲の設定

    //!描画
    void Draw( bool UseRect, bool ControlCenter, DWORD Color );      //描画処理
};

}}
#endif
