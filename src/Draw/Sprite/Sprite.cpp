//+***********************************************************************
//| スプライト描画
//+***********************************************************************

//====<インクルード>======================================
#include "Lib.h"
#include "Tex.h"
#include "FileLoader.h"
#include "Sprite.h"

namespace Draw{
namespace Sprite{

// ------------------------------------------------------
// コンストラクタ
// ------------------------------------------------------

//======デフォルト=====================
// 初期化のみ
//------------------------------------
Sprite::Sprite()
    //!private:メンバ変数初期化
:   CenterOffset(0.0f,0.0f,0.0f),
    ChangeFlag(false),
    pTex(0)
{
    //!データ初期化
    Scale   = VEC3(0.0f, 0.0f, 0.0f);
    Offset  = VEC3(0.0f, 0.0f, 0.0f);
    Rotate  = 0.0f;
    RECT a  = {0};
    Rect =  a;
    D3DXMatrixIdentity(&TFMat);
}

//======デフォルト<引数あり>===========
// (in) sFileName   :ファイル名 s8型
// (in) tFileName   :ファイル名 LPCTSTR型
// (in) Src         :描画データ構造体
//------------------------------------
// ファイル読み込み、変換行列の作成
//------------------------------------
Sprite::Sprite( const s8 *sFileName, LPCTSTR tFileName, const DrawData_t &Src )
    //!private:メンバ変数初期化
    :CenterOffset(0.0f,0.0f,0.0f),
    ChangeFlag(false),
    pTex(0)
{
    //!描画用データ登録
    Scale.x  = Src.ScaleX * Dx::Get()->GetCurWidthRate();
    Scale.y  = Src.ScaleY * Dx::Get()->GetCurHeightRate();
    Scale.z  = 0.0f;
    Offset.x = Src.OffSetX * Dx::Get()->GetCurWidthRate();
    Offset.y = Src.OffSetY * Dx::Get()->GetCurHeightRate();
    Offset.z = 0.0f;
    Rotate   = Src.Rotate;
    Rect     = Src.rect;

    //!中心制御点の計算
    CalPosCenter();

    //!画像ファイル登録
    FileLoader::FileLoader::Get()->CreateTex( &pTex, sFileName, tFileName );

    //!変換行列の作成
    CreateTFMatrix();
}

// ------------------------------------------------------
// デストラクタ
// ------------------------------------------------------
Sprite::~Sprite()
{
    FileLoader::FileLoader::Get()->DeleteTex(&pTex);
}

// ------------------------------------------------------
// ファイルの再登録
// ------------------------------------------------------
// ・コンストラクタで指定してない場合に使用
// ------------------------------------------------------
// (in) sFileName   :ファイル名 s8型
// (in) tFileName   :ファイル名 LPCTSTR型
// ------------------------------------------------------
void Sprite::RegisterFileName( const s8 *sFileName, LPCTSTR tFileName )
{
    //既に登録したファイルがあるならば、リセット
    if(pTex)
    {
        FileLoader::FileLoader::Get()->DeleteTex(&pTex);
    }

    //登録
    FileLoader::FileLoader::Get()->CreateTex( &pTex, sFileName, tFileName );
}

// ------------------------------------------------------
// 描画用データ再登録
// ------------------------------------------------------
// ・コンストラクタで指定してない場合に使用
// ------------------------------------------------------
// (in) Src     :描画データ構造体
// ------------------------------------------------------
void Sprite::RegisterDrawData( const DrawData_t &Src )
{
    //!描画用データ登録
    Scale.x  = Src.ScaleX * Dx::Get()->GetCurWidthRate(); 
    Scale.y  = Src.ScaleY * Dx::Get()->GetCurHeightRate();
    Offset.x = Src.OffSetX * Dx::Get()->GetCurWidthRate(); 
    Offset.y = Src.OffSetY * Dx::Get()->GetCurHeightRate();
    Rotate   = Src.Rotate;
    Rect     = Src.rect;

    //!中心制御点の計算
    CalPosCenter();

    //!変換行列の作成
    CreateTFMatrix();
}

// ------------------------------------------------------
// 回転値の設定
// ------------------------------------------------------
// (in) Rotate  :メンバの回転値(度数)
// ------------------------------------------------------
void Sprite::SetRotate( const f32 &Rotate )
{
    this->Rotate = Rotate;
    ChangeFlag = true;
}

// ------------------------------------------------------
// スケール値の設定
// ------------------------------------------------------
// (in) X   :スケールX
// (in) Y   :スケールY
// ------------------------------------------------------
void Sprite::SetScale( const f32 &X, const f32 &Y )
{
    Scale.x = X * Dx::Get()->GetCurWidthRate(); 
    Scale.y = Y * Dx::Get()->GetCurHeightRate();
    ChangeFlag = true;
}

// ------------------------------------------------------
// オフセット値の設定
// ------------------------------------------------------
// (in) X    :オフセットX
// (in) Y    :オフセットY
// ------------------------------------------------------
void Sprite::SetOffSet( const f32 &X, const f32 &Y )
{
    Offset.x = X * Dx::Get()->GetCurWidthRate();
    Offset.y = Y * Dx::Get()->GetCurHeightRate();
    ChangeFlag = true;
}

// ------------------------------------------------------
// 変換行列の再生成
// ------------------------------------------------------
void Sprite::CreateTFMatrix()
{
    //!回転角の設定
    D3DXMatrixRotationZ( &TFMat, D3DXToRadian(Rotate));
    //!スケール
    TFMat._11 = Scale.x * TFMat._11;
    TFMat._12 = Scale.x * TFMat._12;
    TFMat._13 = Scale.x * TFMat._13;

    TFMat._21 = Scale.y * TFMat._21;
    TFMat._22 = Scale.y * TFMat._22;
    TFMat._23 = Scale.y * TFMat._23;

    TFMat._31 = Scale.z * TFMat._31;
    TFMat._32 = Scale.z * TFMat._32;
    TFMat._33 = Scale.z * TFMat._33;

    //!オフセット
    TFMat._41 = Offset.x;
    TFMat._42 = Offset.y;
    TFMat._43 = Offset.z;
}

// -------------------------------------------------
// 描画範囲の登録
// -------------------------------------------------
// (in) Rect    :描画範囲
// -------------------------------------------------
void Sprite::SetRect( const RECT &Rect )
{
    this->Rect = Rect;
    CalPosCenter();
}

// -------------------------------------------------
// 描画
// -------------------------------------------------
// (in) UseRect         :true==範囲指定描画 false==全体描画
// (in) ControlCenter   :true==中心制御  flase==左上制御
// (in) Color           :色情報
// -------------------------------------------------
void Sprite::Draw( bool UseRect, bool ControlCenter, DWORD Color )
{
    //!変換行列の再計算
    if(ChangeFlag)
    {
        CreateTFMatrix();
        ChangeFlag = false;
    }

    //!変換行列のセット
    Dx::Get()->GetSprite()->SetTransform(&TFMat);
    //!描画
    switch(UseRect)
    {
        case true:
            switch(ControlCenter)
            {
                //範囲指定・中心制御
                case true:
                    Dx::Get()->GetSprite()->Draw( pTex->Get(), &Rect, &CenterOffset, 0, Color );
                    return;
                //範囲指定・左上制御
                case false:
                    Dx::Get()->GetSprite()->Draw( pTex->Get(), &Rect, 0, 0, Color );
                    return;
            }
        case false:
            switch(ControlCenter)
            {
                //全体描画・中心制御
                case true:
                    Dx::Get()->GetSprite()->Draw( pTex->Get(), 0, &CenterOffset, 0, Color );
                    return;
                //全体描画・左上制御
                case false:
                    Dx::Get()->GetSprite()->Draw( pTex->Get(), 0, 0, 0, Color );
                    return;
            }
    }
}


// -------------------------------------------------
// <private>:制御点を画像の中心にする
// -------------------------------------------------
void Sprite::CalPosCenter()
{
    CenterOffset.x = (Rect.right  - Rect.left) / 2.0f;
    CenterOffset.y = (Rect.bottom - Rect.top) / 2.0f;
    CenterOffset.z = 0.0f;
}

}}