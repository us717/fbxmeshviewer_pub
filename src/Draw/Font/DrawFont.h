//=======================================================
// フォントの描画
//=======================================================
#ifndef Include_DrawFont_H
#define Include_DrawFont_H

//====<前方宣言>==========================
namespace Draw{ namespace Sprite{ class Sprite; }}

namespace Draw{
namespace Font{
// -------------------------------------------------
// クラス:フォント描画
// -------------------------------------------------
class DrawFont{

//=====構築子：消去子=================
private:
    //!コピー禁止
    void operator = (const DrawFont &src){}
    DrawFont(const DrawFont &src ){}
public:
    //!コンストラクタ・デストラクタ
    DrawFont();
    ~DrawFont();

//=====変数===========================
private:
    Sprite::Sprite *pFontMap; //フォント用画像
    DWORD          color;     //色情報
    f32            FontSpace; //文字間隔

//=====関数===========================
public:
    void SetScale( const f32 &X, const f32 &Y );            //文字スケールの変更
    void SetFontSpace( const f32 &Space );                  //文字間隔の変更
    void SetColor( const DWORD &Color );                    //色の変更
    void Init();                                            //初期化
    void Draw( const f32 &X, const f32 &Y, LPCTSTR pStr );  //描画
};

}}
#endif
