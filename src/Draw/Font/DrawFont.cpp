//+****************************************************************
//| フォントの描画
//+****************************************************************

//====<インクルード>==========================
#include "Lib.h"
#include "Sprite.h"
#include "DrawFont.h"

namespace Draw{
namespace Font{
// -------------------------------------------------
// コンストラクタ
// -----------------------------------------------
// デフォルトセッティングの定義
// -------------------------------------------------
DrawFont::DrawFont()
{
    //!領域確保
    pFontMap = new Sprite::Sprite;

    //!描画データ初期化
    pFontMap->RegisterFileName( "data/bin/image/SystemFont.dds", _T("data/bin/image/SystemFont.dds") );
    color = D3DCOLOR_ARGB(255,255,255,255);

    //!デフォルトセッティング
    color = D3DCOLOR_ARGB(255,255,255,255); //色	
    pFontMap->SetScale( 1.0f, 1.0f );       //スケール
    FontSpace = 15.0f;                      //文字間隔
}

// -------------------------------------------------
// デストラクタ
// -------------------------------------------------
DrawFont::~DrawFont()
{
    SAFE_DELETE(pFontMap);
}

// -------------------------------------------------
// 文字スケールの変更
// -------------------------------------------------
// (in) X   :スケールX座標
// (in) Y   :スケールY座標
// -------------------------------------------------
void DrawFont::SetScale( const f32 &X, const f32 &Y )
{
    pFontMap->SetScale(X, Y);
}

// -------------------------------------------------
// 文字間隔の変更
// -------------------------------------------------
// (in) Space   :文字間隔
// -------------------------------------------------
void DrawFont::SetFontSpace( const f32 &Space )
{
    this->FontSpace = Space;
}

// -------------------------------------------------
// 色の変更
// -------------------------------------------------
// (in) Color   :色情報(D3DCOLOR_ARGB有効)
// -------------------------------------------------
void DrawFont::SetColor( const DWORD &Color )
{
    this->color = color;
}
// -------------------------------------------------
// 初期化(セッティングを初期状態に戻す)
// -------------------------------------------------
void DrawFont::Init()
{
    color = D3DCOLOR_ARGB(255,255,255,255); //色:<白>
    pFontMap->SetScale( 1.0f, 1.0f );       //スケール
    FontSpace = 15.0f;                      //文字間隔
}

// -------------------------------------------------
// 描画
// -------------------------------------------------
// (in) X       :初期位置X座標
// (in) Y       :初期Y座標
// (in) pStr    :文字列 <ASCIIのみ対応>
// -------------------------------------------------
void DrawFont::Draw( const f32 &X, const f32 &Y, LPCTSTR pStr )
{
    //!描画位置初期化
    f32 OffsetX = X;
    f32 OffsetY = Y;
    
    //!RECT関係初期化
    const s32 Width  = 51; //文字の横幅
    const s32 Height = 51; //文字の縦幅

    //!描画
    for( s32 i=0 ; pStr[i] != '\0' ; ++i )
    {
        s32 FontNumber = pStr[i];
        
        //整合性チェック
        if( FontNumber<32 || FontNumber >=128 )
        {
            FontNumber = 127; //範囲外の場合、空白指定
        }
        FontNumber-=32;

        //フォント切り取り
        s32 RectLeft = ( FontNumber % 10 ) * Width;
        s32 RectTop = ( FontNumber / 10 ) * Height;
        RECT Pos = { RectLeft, RectTop, RectLeft+Width, RectTop+Height };
        
        //描画情報位置指定
        pFontMap->SetRect(Pos);
        pFontMap->SetOffSet( OffsetX, OffsetY );

        //!フォント描画
        pFontMap->Draw(true,true,color); //回転も考慮して中心点制御

        //描画位置更新
        OffsetX += FontSpace;
    }
}

}}