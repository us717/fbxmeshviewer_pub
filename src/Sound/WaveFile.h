//=======================================================
// WaveFile
//-------------------------------------------------------
// ・Waveファイル操作(読み込み)
// ・各サウンドフォーマットで利用するクラス
//=======================================================
#ifndef Include_WaveFile_H
#define Include_WaveFile_H

namespace Sound{

// ------------------------------------------
// クラス:WaveFile
// ------------------------------------------
class WaveFile{

//=====構築子：消去子=================
private:
//!コピー禁止
    void operator = ( const WaveFile &src ){}
    WaveFile( const WaveFile &src ){}
public:
    WaveFile();
    ~WaveFile();

//=====変数==========================
private:
    //ファイル読み込み
    LPTSTR          pFileName;                      //ファイル名
    HMMIO           hMmio;                          //MMIOハンドル
    MMIOINFO        mmioInfo;                       //MMIOINFO構造体

    //ファイル内データ
    WAVEFORMATEX    WaveFormat;                     //Waveフォーマット構造体
    MMCKINFO        DataChunk;                      //データチャンク
    DWORD           DataSize;                       //データサイズ

    //ファイル内操作
    s32             CurOffset;                      //現在のデータ位置
    bool            FileState;                      //ファイルの状態

//=====関数==========================
public:
    bool            OpenFile( LPTSTR pFileName );   //ファイルを開く
    DWORD           GetFileSize();                  //ファイルのサイズを返す
    bool            Reset();                        //読み取り位置を先頭に戻す
    WAVEFORMATEX*   GetFormat();                    //Waveフォーマットを返す
    void            CloseFile();                    //ファイルを閉じる
    bool            Read( BYTE* pBuffer, DWORD ReadSize, DWORD *pDoneSize );//データ読み込み
};

}

#endif
