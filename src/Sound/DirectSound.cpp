//+******************************************************************************
//| DirectSound
//+******************************************************************************

//====<インクルード>========================
#include "Lib.h"
#include "DirectSound.h"

namespace Sound{

// ------------------------------------------------
// デストラクタ
// ------------------------------------------------
// プライマリバッファ・サウンドデバイス解放
// ・セカンダリは別途個別に解放すること
// ------------------------------------------------
DirectSound::~DirectSound()
{
    //エラー時の場合、終了時にエラーメッセージを出しておく
    if(ErrorFlag)
    {
        MessageBox( Dx::Get()->GetMainHwnd(), _T("起動時DirectSoundを初期化できませんでした"),_T("DirectSound") , MB_OK );
    }

    SAFE_RELEASE(pPrimaryBuffer);   //プライマリバッファ解放
    SAFE_RELEASE(pDirectSound);     //サウンドデバイス解放
}

// -----------------------------------------------
// DirectSound初期化
// -----------------------------------------------
// (in)     HWND    :ウィンドハンドル
// (out)    HRESULT :フラグ
// -----------------------------------------------
bool DirectSound::InitDirectSound(HWND hWnd)
{
//====<サウンドデバイスの作成>==============
    if( FAILED( DirectSoundCreate8(NULL, &pDirectSound, NULL )))
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("サウンドデバイス取得失敗"),_T("DirectSound") , MB_OK );
    #endif
        ErrorFlag = true;
        return false;
    }

//====<協調レベルの設定>===================
    if( FAILED( pDirectSound->SetCooperativeLevel(hWnd, DSSCL_PRIORITY)))
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("協調レベル設定失敗"),_T("DirectSound") , MB_OK );
    #endif
        SAFE_RELEASE(pDirectSound);
        ErrorFlag = true;
        return false;
    }
//====<プライマリバッファの作成>===========

    //!プライマリバッファ初期化
    DSBUFFERDESC	DSDesc;	
    SecureZeroMemory( &DSDesc, sizeof(DSBUFFERDESC));
    DSDesc.dwSize = sizeof(DSBUFFERDESC);

    //!プライマリバッファ設定
    DSDesc.dwFlags			= DSBCAPS_CTRLVOLUME|DSBCAPS_PRIMARYBUFFER;
    DSDesc.dwBufferBytes	= 0;
    DSDesc.lpwfxFormat		= NULL;
    //!プライマリバッファ作成
    if( FAILED( pDirectSound->CreateSoundBuffer(&DSDesc, &pPrimaryBuffer, NULL) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("プライマリバッファ作成失敗"),_T("DirectSound") , MB_OK );
    #endif
        SAFE_RELEASE(pDirectSound);
        ErrorFlag = true;
        return false;
    }

    //!プライマリWaveフォーマットの設定
    WAVEFORMATEX PWF;
    SecureZeroMemory(&PWF, sizeof(WAVEFORMATEX ) );
    PWF.wFormatTag      = WAVE_FORMAT_PCM;  //2ch用
    PWF.nChannels       = 2;                //2ch指定
    PWF.nSamplesPerSec  = 44100;            //サンプリングステート
    PWF.nBlockAlign     = 4;                //ブロックアライメント
    PWF.wBitsPerSample  = 16;               //サンプル当たりのビット数
    PWF.nAvgBytesPerSec = PWF.nSamplesPerSec * PWF.nBlockAlign;

    pPrimaryBuffer->SetFormat(&PWF);

    return true;
}

// -----------------------------------------------
// マスターボリュームの設定
// -----------------------------------------------
// 0~100指定・Max100でミュート
// -----------------------------------------------
bool DirectSound::SetVolume( LONG Volume )
{
#ifdef _DEBUG
    if( Volume < 0 || Volume > 100 )
        MessageBox( Dx::Get()->GetMainHwnd(), _T("ボリューム値不正"), _T("プライマリサウンド") , MB_OK );
#endif
    if(pPrimaryBuffer == 0)
        return false;

    Volume *= -100;
    pPrimaryBuffer->SetVolume(Volume);

    return true;
}

// ------------------------------------------------
// プライマリバッファ・デバイス取得
// ------------------------------------------------
// ・プライマリのデバイスを返す
// ------------------------------------------------
const LPDIRECTSOUND8 DirectSound::GetPrimSoundDevice()
{
    return pDirectSound;
}

}