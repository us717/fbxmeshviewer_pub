//+******************************************************************************
//|フォーマット:コピーサウンドファイル
//+******************************************************************************

//====<インクルード>========================
#include "Lib.h"
#include "CopySoundFile.h"

namespace Sound{

// ----------------------------------------------------
// デストラクタ
// ----------------------------------------------------
CopySoundFile::~CopySoundFile()
{
    SAFE_RELEASE(pBuffer);
}

// ----------------------------------------------------
// 再生
// ----------------------------------------------------
//  (in)    bool    :ループ再生のフラグ(trueでループ)
//  (out)   bool    :フラグ
// ----------------------------------------------------
bool CopySoundFile::Play(bool Loop)
{
    if(!pBuffer)
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("親バッファがコピーされていないか存在しません"), _T("CopySoundFile"), MB_OK );
    #endif
        return false;
    }
    if( Loop == true )
    {
        pBuffer->Play( 0, 0, DSBPLAY_LOOPING );
    }
    else
    {
        pBuffer->Play( 0, 0, 0 );
    }
    return true;
}

// ----------------------------------------------------
// 停止
// ----------------------------------------------------
bool CopySoundFile::Stop()
{
    if(!pBuffer)
        return false;
    pBuffer->Stop();
    pBuffer->SetCurrentPosition(0); //バッファの位置を先頭にする
    return true;
}

// -----------------------------------------------
// ボリューム設定(バッファ別)
// -----------------------------------------------
// 0~100指定・Max100でミュート
// -----------------------------------------------
// (out)    bool    :フラグ
// -----------------------------------------------
bool CopySoundFile::SetVolume( LONG Volume )
{
#ifdef _DEBUG
    if( Volume < 0 || Volume > 100 )
        MessageBox( Dx::Get()->GetMainHwnd(), _T("ボリューム値不正"), _T("セカンダリ") , MB_OK );
#endif
    if(pBuffer == 0)
        return false;
    Volume *= -100;
    pBuffer->SetVolume(Volume);

    return true;
}

}