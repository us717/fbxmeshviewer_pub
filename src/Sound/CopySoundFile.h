//=======================================================
// フォーマット:コピーサウンドファイル
//-------------------------------------------------------
// ・SoundFileで読み込んだバッファを共有(コピー)して使用する
// ・ファイルローダーでは管理していない
// ・メインのSoundFile→コピーサウンドファイル
// ・同じ音を同時再生
//=======================================================
#ifndef Include_CopySoundFile_H
#define Include_CopySoundFile_H

//====<前方宣言>============================
namespace Sound{

class CopySoundFile{
//=====構築子：消去子=================
private:
    //!コピー禁止
    void operator = ( const CopySoundFile &src ){}
    CopySoundFile( const CopySoundFile &src ){}

public:
    //!コンストラクタ・デストラクタ
    CopySoundFile():pBuffer(0){}
    ~CopySoundFile();

    friend class SoundFile;

//=====変数==========================
    LPDIRECTSOUNDBUFFER8    pBuffer;        //セカンダリバッファ

//=====関数==========================
public:
    bool Play( bool Loop = false );         //再生
    bool Stop();                            //停止
    bool SetVolume( LONG Volume );          //ボリューム設定(バッファ別)
};

}

#endif
