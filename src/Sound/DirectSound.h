//=======================================================
//DirectSound
// -------------------------------------------------------
// DirectSoundの初期化処理を行う
// サウンドデバイスの管理
//=======================================================
#ifndef Include_DirectSound_H
#define Include_DirectSound_H

namespace Sound{

// ------------------------------------------
// クラス:DirectSound
// ------------------------------------------
//	シングルトンクラス
// ------------------------------------------
class DirectSound{
//=====構築子：消去子=================
public:
    //!クラスへのアクセス手段
    static DirectSound &Get()
    {
        static DirectSound instance;
        return instance;
    }
private:
    //!コンストラクタ<使用不可>
    DirectSound():pDirectSound(0),pPrimaryBuffer(0),ErrorFlag(false){}
    DirectSound( const DirectSound &src ){}
    DirectSound &operator=( const DirectSound &src ){}
    ~DirectSound();

//=====変数==========================
private:
    LPDIRECTSOUND8          pDirectSound;       //サウンドデバイス
    LPDIRECTSOUNDBUFFER     pPrimaryBuffer;     //プライマリバッファ格納用
    bool                    ErrorFlag;          //エラーフラグ

//=====関数==========================
public:
    bool InitDirectSound(HWND hWnd);            //初期化処理(プライマリバッファ作成)
    const LPDIRECTSOUND8 GetPrimSoundDevice();  //プライマリ・サウンドデバイス取得
    bool SetVolume( LONG Volume );              //マスターボリュームの設定
};

}
#endif
