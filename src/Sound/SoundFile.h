//=======================================================
// フォーマット:SoundFile
//-------------------------------------------------------
// ・セカンダリバッファ
// ・効果音など、容量の少ないデータをメモリに全コピーして使用
// ・容量が多い場合はストリーミングクラスを使用すること
// ・ファイルローダで管理している
//=======================================================
#ifndef Include_SoundFile_H
#define Include_SoundFile_H

//====<インクルード>========================
#include "WaveFile.h"

//====<前方宣言>============================
namespace FileLoader{ class FileLoader; }
namespace Sound{ class CopySoundFile; }

namespace Sound{

// ------------------------------------------
// クラス:SoundFile
// ------------------------------------------
class SoundFile{

//=====構築子：消去子=================
private:
//!コンストラクタ
    SoundFile( LPTSTR pFileName );

//!コピー禁止
    SoundFile( const SoundFile&src ){}
    void operator = ( const SoundFile &src ){}

//!デストラクタ
    ~SoundFile();

//!フレンド
    friend class FileLoader::FileLoader;

//=====変数==========================
private:
    LPTSTR                  pFileName;                      //ファイル名
    WaveFile                WaveData;                       //Waveデータ
    LPDIRECTSOUNDBUFFER8    pBuffer;                        //セカンダリバッファ
//=====関数==========================
private:
    bool Redy() const{ return(!pBuffer?false:true); }       //ロード済みかの確認
    bool CreateBuffer();                                    //バッファを作成する
public:
    bool Play( bool Loop = false );                         //再生
    bool Stop();                                            //停止
    bool SetVolume( LONG Volume );                          //ボリューム設定(バッファ別)
    bool CopyBuffer( CopySoundFile &Buf );                  //バッファをコピーする
};

}

#endif