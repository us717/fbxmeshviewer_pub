//=======================================================
// フォーマット：サウンドストリーミング
//-------------------------------------------------------
// ストリーミング再生
// ファイルローダーで管理している
// 効果音とは違い長い音声再生に適している
//=======================================================
#ifndef Include_SoundStream_H
#define Include_SoundStream_H

//=====インクルード=================
#include "WaveFile.h"

//====<前方宣言>====================
namespace FileLoader{ class FileLoader; }

namespace Sound{
// ------------------------------------------
// クラス:ストリーミング再生
// ------------------------------------------
class SoundStream{

//=====構築子：消去子=================
private:
//!コンストラクタ
    SoundStream( LPTSTR pFileName );

//!コピー禁止
    void operator = ( const SoundStream &src ){}
    SoundStream( const SoundStream &src ){}

//!デストラクタ
    ~SoundStream();

//!フレンド
    friend class FileLoader::FileLoader;

//=====変数===========================
private:
    LPTSTR                  pFileName;              //ファイル名
    LPDIRECTSOUNDBUFFER8    pBuffer;                //セカンダリバッファ
    LPDIRECTSOUNDNOTIFY8    pNotify;                //イベント通知用インターフェイス
    HANDLE                  Event[2];               //イベントハンドル
    HANDLE                  ThreadHandle;           //スレッドハンドル
    DSBUFFERDESC            DSDesc;                 //DSBUFFERDESC構造体
    WaveFile                Wave;                   //音データ

public:
    DWORD                   BlockSize;              //一回の更新でのブロックサイズ
    DWORD                   BufferNum;              //サンドバッファのブロック数
    DWORD                   NextOffset;             //ブロック書き込み位置
    bool                    StreamState;            //再生できるかどうか

//=====関数===========================
private:
    bool Redy() const{ return(!pBuffer?false:true); }//ロード済みかの確認
    static unsigned __stdcall ThreadFunction(void*);//スレッド(再生)
    bool CopyBlock();                               //再生中バッファ作成
    bool SetStream();                               //再生ファイル読み込み・準備
public:
    bool Play();                                    //再生(スレッド作成して再生する)
    void Stop();                                    //停止
    bool SetVolume( LONG Volume );                  //ボリューム設定(バッファ別)
};

}
#endif
