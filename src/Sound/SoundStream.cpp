//+******************************************************************************
//| フォーマット:ストリーミング再生
//+******************************************************************************
#include "Lib.h"
#include <process.h>
#include "WaveFile.h"
#include "DirectSound.h"
#include "SoundStream.h"

namespace Sound{

// ------------------------------------------------
// コンストラクタ
// ------------------------------------------------
// (in) pFileName   ：読み込むファイル名
// ------------------------------------------------
SoundStream::SoundStream( LPTSTR pFileName ):
    pFileName(0),pBuffer(0),pNotify(0),BlockSize(0),BufferNum(0),NextOffset(0),StreamState(false)
{
    Event[0] = 0;
    Event[1] = 0;

    //!ファイルの指定
    this->pFileName = pFileName;
}

// ------------------------------------------------
// デストラクタ
// ------------------------------------------------
SoundStream::~SoundStream()
{
    //!サウンドバッファ解放
    Wave.CloseFile();
    SAFE_RELEASE(pNotify);
    SAFE_RELEASE(pBuffer);

    //!イベントハンドル解放
    if((Event[1])){ CloseHandle(Event[1]); Event[1] = 0;}
    if((Event[0])){ CloseHandle(Event[0]); Event[0] = 0;}
}

// ------------------------------------------------
// private:バッファに音データをコピーする
// ------------------------------------------------
// (out)    bool    :フラグ
// ------------------------------------------------
bool SoundStream::CopyBlock()
{
    //!バッファのロック
    LPBYTE pBlock1, pBlock2;
    DWORD BlockSize1, BlockSize2;
    if (FAILED( pBuffer->Lock( NextOffset, BlockSize, (LPVOID*)&pBlock1, &BlockSize1,
                    (LPVOID*)&pBlock2, &BlockSize2, 0 ) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("バッファロック失敗"), pFileName , MB_OK );
    #endif
        return false;
    }

    if( pBlock2 != 0 )
    {
        pBuffer->Unlock( pBlock1, BlockSize1, pBlock2, BlockSize2 );
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("バッファサイズ異常"), pFileName , MB_OK );
    #endif
        return false;
    }

    //!バッファ書き込み
    DWORD Donesize; //書き込んだサイズ
    DWORD Totalsize;//書き込んだ合計サイズ

    for( Totalsize=0 ; Totalsize<BlockSize1 ; Totalsize += Donesize )
    {
        Wave.Read( pBlock1 + Totalsize, BlockSize1 - Totalsize, &Donesize);

        //書き込み領域が余ったら、先頭に戻って書き込む
        if( BlockSize1 - Totalsize != Donesize )
        {
            Wave.Reset();
        }
    }

    //!バッファアンロック
    pBuffer->Unlock( pBlock1, BlockSize1, pBlock2, BlockSize2 );

    //!書き込みブロック位置更新
    NextOffset += BlockSize;
    if( NextOffset >= BufferNum )
        NextOffset = 0;

    return true;
}
// ------------------------------------------------
// private:スレッドが実行する関数
// ------------------------------------------------
// ・読み込みと再生
// ------------------------------------------------
// (i/o)    Param   :this(自分のクラス)ポインタ
// ------------------------------------------------
unsigned SoundStream::ThreadFunction( void* Param )
{
    //!初回2ブロック書き込む
    SoundStream *pData = (SoundStream*)Param;
    pData->Wave.Reset();
    pData->NextOffset = 0;
    pData->CopyBlock();
    pData->CopyBlock();

    //!再生開始
    pData->pBuffer->Play( 0, 0, DSBPLAY_LOOPING );

    //ストリーミング開始
    while(true)
    {
        DWORD i = WaitForMultipleObjects( 2, pData->Event, FALSE, INFINITE );
    
        switch(i)
        {
            //シグナル状態・次のブロックへ
            case WAIT_OBJECT_0:
                pData->CopyBlock();
                break;
            //それ以外
            case WAIT_OBJECT_0 + 1:
            default:
                _endthreadex(true);
        }
    }

    return 0;
}

// ------------------------------------------------
// 再生ファイル読み込み・準備
// ------------------------------------------------
// (out)    bool    :フラグ
// ------------------------------------------------
bool SoundStream::SetStream()
{
    //!プライムがない場合は作らない
    if( !DirectSound::Get().GetPrimSoundDevice() )
        return false;

//====<初期化>==================================
    StreamState = false;
    SAFE_RELEASE(pNotify);
    SAFE_RELEASE(pBuffer);
    if((Event[1])){ CloseHandle(Event[1]); Event[1] = 0;}
    if((Event[0])){ CloseHandle(Event[0]); Event[0] = 0;}

//====<WAVEFileを開く>==========================
    if( !Wave.OpenFile(pFileName) )
        return false;

//====<セカンダリバッファの作成>===========
    BlockSize = Wave.GetFormat()->nAvgBytesPerSec;  //一回の更新でのブロックサイズは1秒分
    BufferNum = BlockSize * 2;                      //サンドバッファはブロック2個分

    //!DSBUFFERDESC構造体設定
    DSBUFFERDESC dsDesk;
    SecureZeroMemory( &dsDesk, sizeof(DSBUFFERDESC) );
    dsDesk.dwSize = sizeof(DSBUFFERDESC);
    dsDesk.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_GLOBALFOCUS |DSBCAPS_CTRLVOLUME |
                            DSBCAPS_LOCDEFER | DSBCAPS_CTRLPOSITIONNOTIFY |DSBCAPS_CTRLFREQUENCY;
    dsDesk.lpwfxFormat = Wave.GetFormat();
    dsDesk.dwBufferBytes = BufferNum;

    //!バッファの作成
    LPDIRECTSOUNDBUFFER SoundBuf;
    if( FAILED( DirectSound::Get().GetPrimSoundDevice()->CreateSoundBuffer( &dsDesk, &SoundBuf, NULL ) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("バッファが作成できませんでした"), pFileName , MB_OK );
    #endif
        return false;
    }

    //!IDirectSoundBuffer8インターフェイス取得
    if( FAILED( SoundBuf->QueryInterface(IID_IDirectSoundBuffer8, (LPVOID*)&pBuffer) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("IDirectSoundBuffer8インターフェイス取得失敗"), pFileName , MB_OK );
    #endif
        SAFE_RELEASE(SoundBuf);
        return false;
    }
    SAFE_RELEASE(SoundBuf);

//====<イベントの作成>====================

    //!イベント通知インターフェイス取得
    if( FAILED( pBuffer->QueryInterface(IID_IDirectSoundNotify8, (LPVOID*)&pNotify) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("イベント通知作成失敗"), pFileName , MB_OK );
    #endif
        return false;
    }
//====<イベント通知位置の設定>====================

    //!イベントオブジェクトの作成
    Event[0] = CreateEvent( NULL, FALSE, FALSE, NULL ); //サウンドブロック更新用
    Event[1] = CreateEvent( NULL, FALSE, FALSE, NULL ); //終了通知用

    DSBPOSITIONNOTIFY PosNotify[3];
    PosNotify[0].dwOffset = 1 * BlockSize - 1;  //1秒目直前
    PosNotify[0].hEventNotify = Event[0];
    PosNotify[1].dwOffset = 2 * BlockSize - 1;  //2秒目直前
    PosNotify[1].hEventNotify = Event[0];
    PosNotify[2].dwOffset = DSBPN_OFFSETSTOP;   //再生停止
    PosNotify[2].hEventNotify = Event[1];
    if( FAILED( pNotify->SetNotificationPositions( 3, PosNotify ) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("イベント位置作成失敗"), pFileName, MB_OK );
    #endif
        return false;
    }

    StreamState = true;
    return true;
}

// ------------------------------------------------
// 再生
// ------------------------------------------------
// (out)    bool    : フラグ
// ------------------------------------------------
bool SoundStream::Play()
{
    if(!StreamState)
        return false;
    ThreadHandle = (HANDLE) _beginthreadex( NULL, 0, SoundStream::ThreadFunction, this, 0, 0 );
    if(ThreadHandle = NULL)
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("スレッド作成失敗"), _T("FileName") , MB_OK );
    #endif
        return false;
    }
    return true;
}

// ------------------------------------------------
// 停止
// ------------------------------------------------
void SoundStream::Stop()
{
    if(!StreamState)
        return;
    if(pBuffer)
        pBuffer->Stop();
    WaitForSingleObject( ThreadHandle, INFINITE );
    CloseHandle(ThreadHandle);
}

// -----------------------------------------------
// ボリューム設定(バッファ別)
// -----------------------------------------------
// 0~100指定・Max100でミュート
// -----------------------------------------------
// (in)     Volume  :ボリューム
// (out)    bool    :フラグ
// -----------------------------------------------
bool SoundStream::SetVolume( LONG Volume )
{
#ifdef _DEBUG
    if( Volume < 0 || Volume > 100 )
        MessageBox( Dx::Get()->GetMainHwnd(), _T("ボリューム値不正"), _T("ストリーム") , MB_OK );
#endif
    if(pBuffer == 0)
        return false;
    Volume *= -100;
    pBuffer->SetVolume(Volume);

    return true;
}

}