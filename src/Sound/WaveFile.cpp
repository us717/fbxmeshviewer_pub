//+******************************************************************************
//| WaveFile
//+******************************************************************************

//====<インクルード>========================
#include "Lib.h"
#include "WaveFile.h"

namespace Sound{

// ----------------------------------------------------
// コンストラクタ
// ----------------------------------------------------
WaveFile::WaveFile():pFileName(0),hMmio(NULL),CurOffset(0),FileState(false){}

// ----------------------------------------------------
// デストラクタ
// ----------------------------------------------------
WaveFile::~WaveFile(){CloseFile();}

// ----------------------------------------------------
// ファイルを開く
// ----------------------------------------------------
// RIFFチャンク
// RIFFチャンク内    -Fmt(waveformatex)  :データ情報
//                  -Data(waveData)     :データ
// ----------------------------------------------------
// (in)     pFileName   :ファイル名
// (out)    bool        :フラグ
// ----------------------------------------------------
bool WaveFile::OpenFile( LPTSTR pFileName )
{
    //!既にファイル情報がある場合は初期化
    if(FileState == true)
    {
        FileState = false;
        DataSize = 0;
        CurOffset = 0;
    }

    //!Waveファイルオープン
    this->pFileName = pFileName;
    hMmio = NULL;
    memset( &mmioInfo, 0, sizeof(MMIOINFO) );
    hMmio = mmioOpen( this->pFileName, &mmioInfo, MMIO_READ );
    if( !hMmio )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("ファイルを開けませんでした"), pFileName , MB_OK );
    #endif
        return false;
    }
    
    //!RIFF情報(ファイルタイプ)取得
    //以下情報取得のための親情報になる
    MMRESULT mmRes;
    MMCKINFO RiffChunk;
    RiffChunk.fccType = mmioFOURCC('W', 'A', 'V', 'E');
    mmRes = mmioDescend( hMmio, &RiffChunk, NULL, MMIO_FINDRIFF );
    if( mmRes != MMSYSERR_NOERROR )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("RIFFが見つかりませんでした"), this->pFileName , MB_OK );
    #endif
        mmioClose( hMmio, 0 );
        return false;
    }

    //!フォーマット取得
    //チャンク情報を取得するために必要
    MMCKINFO formatChunk;
    formatChunk.ckid = mmioFOURCC('f', 'm', 't', ' ');
    mmRes = mmioDescend( hMmio, &formatChunk, &RiffChunk, MMIO_FINDCHUNK );
    if( mmRes != MMSYSERR_NOERROR )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("フォーマットが見つかりませんでした"), this->pFileName , MB_OK );
    #endif
        mmioClose( hMmio, 0 );
        return false;
    }
    
    //!WAVEFORMATEX構造体格納
    //取得できたフォーマットからデータの基本情報を読み取る
    DWORD fmsize = formatChunk.cksize;
    DWORD size = mmioRead( hMmio, (HPSTR)&WaveFormat, fmsize );
    if( size != fmsize )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("フォーマットが格納できませんでした"), this->pFileName , MB_OK );
    #endif
        mmioClose( hMmio, 0 );
        return false;
    }
    mmioAscend( hMmio, &formatChunk, 0 );//親階層(RIFF)に移動する

    //!データチャンク取得
    //取得できたフォーマットから、音データを読み取る
    DataChunk.ckid = mmioFOURCC('d', 'a', 't', 'a');
    mmRes = mmioDescend( hMmio, &DataChunk, &RiffChunk, MMIO_FINDCHUNK );
    if( mmRes != MMSYSERR_NOERROR )
    {
        mmioClose( hMmio, 0 );
        return false;
    }
    DataSize = DataChunk.cksize;
    FileState = true;
    return true;
}


// ----------------------------------------------------
// ファイルのサイズを返す
// ----------------------------------------------------
// (out)    DWORD   :書き込み先(Lock済みであること)
// ----------------------------------------------------
DWORD WaveFile::GetFileSize()
{
    return DataSize;
}


// ----------------------------------------------------
// データ読み取り
// ----------------------------------------------------
// (in)     pBuffer     :書き込み先(Lock済みであること)
// (in)     ReadSize    :読み取りサイズ
// (i/o)    pDoneSize   :実際に読み取ったサイズを返すポインタ
// (out)    bool        :フラグ
// ----------------------------------------------------
bool WaveFile::Read( BYTE *pBuffer, DWORD ReadSize, DWORD *pDoneSize )
{
    *pDoneSize = mmioRead( hMmio, (HPSTR)pBuffer, ReadSize );
    CurOffset += *pDoneSize;
    return true;
}

// ----------------------------------------------------
// 読み取り位置を先頭にする
// ----------------------------------------------------
// (out)    bool    :フラグ
// ----------------------------------------------------
bool WaveFile::Reset()
{
    mmioSeek( hMmio, -CurOffset, SEEK_CUR );
    CurOffset = 0;
    return true;
}

// ----------------------------------------------------
// Waveフォーマットを返す
// ---------------------------------------------------
// (out)    WAVEFORMATEX*   :Waveフォーマットへのポインタ
// ----------------------------------------------------
WAVEFORMATEX* WaveFile::GetFormat()
{
    return &WaveFormat;
}

// ----------------------------------------------------
// ファイルを閉じる
// ----------------------------------------------------
void WaveFile::CloseFile()
{
    if(FileState)
    mmioClose( hMmio, 0 );
}

}