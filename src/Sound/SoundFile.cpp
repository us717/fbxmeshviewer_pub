//+******************************************************************************
//| SoundFile
//+******************************************************************************

//====<インクルード>========================
#include "Lib.h"
#include "DirectSound.h"
#include "CopySoundFile.h"
#include "SoundFile.h"

namespace Sound{

// ----------------------------------------------------
// コンストラクタ
// ----------------------------------------------------
// (in) pFileName   ：読み込むファイル名
// ----------------------------------------------------
SoundFile::SoundFile( LPTSTR pFileName ):pFileName(0),pBuffer(0){
    pFileName = pFileName;
}

// ----------------------------------------------------
// デストラクタ
// ----------------------------------------------------
SoundFile::~SoundFile()
{
    WaveData.CloseFile();
    SAFE_RELEASE(pBuffer);
}

// ----------------------------------------------------
// 再生
// ----------------------------------------------------
// (in)     Loop    :ループ再生のフラグ(trueでループ)
// (out)    bool    :フラグ
// ----------------------------------------------------
bool SoundFile::Play( bool Loop )
{
    if(!pBuffer)
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("バッファが作られていません"), pFileName, MB_OK );
    #endif
        return false;
    }
    if( Loop == true )
    {
        pBuffer->Play( 0, 0, DSBPLAY_LOOPING );
    }
    else
    {
        pBuffer->Play( 0, 0, 0 );
    }
    return true;
}

// ----------------------------------------------------
// 停止
// ----------------------------------------------------
bool SoundFile::Stop()
{
    if(!pBuffer)
        return false;
    pBuffer->Stop();
    pBuffer->SetCurrentPosition(0);	//バッファの位置を先頭にする
    return true;
}

// -----------------------------------------------
// ボリューム設定(バッファ別)
// -----------------------------------------------
// 0~100指定・Max100でミュート
// -----------------------------------------------
// (in)     Volume  :ボリューム
// (out)    bool    :フラグ
// -----------------------------------------------
bool SoundFile::SetVolume( LONG Volume )
{
#ifdef _DEBUG
    if( Volume < 0 || Volume > 100 )
        MessageBox( Dx::Get()->GetMainHwnd(), _T("ボリューム値不正"), _T("セカンダリ") , MB_OK );
#endif
    if(pBuffer == 0)
        return false;
    Volume *= -100;
    pBuffer->SetVolume(Volume);

    return true;
}

// ----------------------------------------------------
// バッファをコピーする
// ----------------------------------------------------
// (in)     Buf     :コピー先
// (bool)   bool    :フラグ
// ----------------------------------------------------
bool SoundFile::CopyBuffer( CopySoundFile &Buf )
{
    if(!&Buf)
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("コピー先のメモリが確保されていません"), pFileName, MB_OK );
    #endif
    }

    //!プライムがない場合は作らない
    if( !DirectSound::Get().GetPrimSoundDevice() )
        return false;

    //!コピー元(自分)のバッファがあるか確認する
    if(!this->pBuffer)
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("バッファが作られていません"), pFileName, MB_OK );
    #endif
        return false;
    }

    //!コピー先にバッファが既にある場合は解放する
    if(!Buf.pBuffer)
        SAFE_RELEASE(Buf.pBuffer);

    LPDIRECTSOUNDBUFFER SoundBuf;
    if( FAILED ( DirectSound::Get().GetPrimSoundDevice()->DuplicateSoundBuffer( this->pBuffer, &SoundBuf ) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("バッファコピー失敗"), this->pFileName , MB_OK );
    #endif
        return false;
    }

    if( FAILED( SoundBuf->QueryInterface(IID_IDirectSoundBuffer8, (LPVOID*)&Buf.pBuffer ) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("IDirectSoundBuffer8インターフェイス取得失敗"), this->pFileName , MB_OK );
    #endif
        SAFE_RELEASE(SoundBuf);
        return false;
    }
    SAFE_RELEASE(SoundBuf);
    return true;
}

// ----------------------------------------------------
// private:バッファを作成する
// ----------------------------------------------------
// (bool)   bool    :フラグ
// ----------------------------------------------------
bool SoundFile::CreateBuffer()
{
    //!プライムがない場合は作らない
    if( !DirectSound::Get().GetPrimSoundDevice() )
        return false;

//====<WAVEFileを開く>===================
    if( !WaveData.OpenFile(pFileName) )
        return false;

//====<セカンダリバッファの作成>=========

    //!セカンダリバッファ初期化
    DSBUFFERDESC DSDesc;
    ZeroMemory( &DSDesc, sizeof(DSBUFFERDESC));
    DSDesc.dwSize = sizeof(DSBUFFERDESC);

    //!	セカンダリバッファ設定
    DSDesc.dwFlags          = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_GLOBALFOCUS|DSBCAPS_LOCDEFER | DSBCAPS_CTRLVOLUME |
                              DSBCAPS_CTRLFREQUENCY;
    DSDesc.lpwfxFormat      = WaveData.GetFormat();
    DSDesc.dwBufferBytes    = WaveData.GetFileSize();

    //! バッファ作成
    LPDIRECTSOUNDBUFFER SoundBuf;
    if( !DirectSound::Get().GetPrimSoundDevice() )
    {
        WaveData.CloseFile();
        return false;
    }

    if( FAILED( DirectSound::Get().GetPrimSoundDevice()->CreateSoundBuffer( &DSDesc, &SoundBuf, NULL) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), pFileName, _T("バッファ作成失敗"), MB_OK );
    #endif
        WaveData.CloseFile();
        return false;
    }

    //!IDirectSoundBuffer8インターフェイス取得
    if( FAILED( SoundBuf->QueryInterface(IID_IDirectSoundBuffer8, (LPVOID*)&pBuffer) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("IDirectSoundBuffer8インターフェイス取得失敗"), pFileName , MB_OK );
    #endif
        SAFE_RELEASE(SoundBuf);
        WaveData.CloseFile();
        return false;
    }
    SAFE_RELEASE(SoundBuf);

    LPBYTE	pBlock;
    DWORD	BlockSize;
    if( FAILED( pBuffer->Lock( 0, WaveData.GetFileSize(), (LPVOID*)&pBlock, &BlockSize ,0, 0, 0 ) ) )
    {
    #ifdef _DEBUG
        MessageBox( Dx::Get()->GetMainHwnd(), _T("バッファロック失敗"), pFileName , MB_OK );
    #endif
        WaveData.CloseFile();
        return false;
    }

    //!バッファに書き込み	
    DWORD DoneSize;
    WaveData.Read( pBlock, WaveData.GetFileSize(), &DoneSize );
    pBuffer->Unlock( pBlock, BlockSize, 0, 0 );
    WaveData.CloseFile();
    return true;
}

}