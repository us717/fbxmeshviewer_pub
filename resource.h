//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MeshViewer.rc
//
#define ViewerController                101
#define IDI_ICON1                       107
#define IDC_RADIO1                      1001
#define IDC_RADIO2                      1002
#define IDC_CHECK2                      1010
#define IDC_LIST1                       1011
#define IDC_RADIO4                      1014
#define IDC_RADIO5                      1015
#define IDC_BUTTON2                     1016
#define IDC_CHECK1                      1020

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
